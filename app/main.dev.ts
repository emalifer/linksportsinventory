/* eslint global-require: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import path from 'path';
import { app, BrowserWindow, ipcMain, shell } from 'electron';
import log from 'electron-log';
import MenuBuilder from './menu';

const os = require("os");
const fs = require("fs");


// export default class AppUpdater {
  //   constructor() {
    //     log.transports.file.level = 'info';
    //     autoUpdater.logger = log;
    //     autoUpdater.checkForUpdatesAndNotify();
    //   }
    // }

let mainWindow: BrowserWindow | null = null;
let splashWindow: BrowserWindow | null = null;
let printWindow: BrowserWindow | null = null;

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

if (
  process.env.NODE_ENV === 'development' ||
  process.env.DEBUG_PROD === 'true'
) {
  require('electron-debug')();
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

  return Promise.all(
    extensions.map(name => installer.default(installer[name], forceDownload))
  ).catch(console.log);
};

const createWindow = async () => {
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.DEBUG_PROD === 'true'
  ) {
    await installExtensions();
  }

  splashWindow = new BrowserWindow({
    show: true,
    width: 300,
    height: 240,
    center: true,
    transparent: true,
    frame: false,
    webPreferences: {
      nodeIntegration: false,
      preload: `${__dirname}/preload.js`
    }
  });
  splashWindow.loadURL(`file://${__dirname}/splash.html`);

  ipcMain.on('get-name', (event, arg) => { event.returnValue = app.getName() });
  ipcMain.on('get-version', (event, arg) => { event.returnValue = app.getVersion() });

  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    // frame: false,
    webPreferences:
      process.env.NODE_ENV === 'development' || process.env.E2E_BUILD === 'true'
        ? {
            nodeIntegration: true
          }
        : {
            preload: path.join(__dirname, 'dist/renderer.prod.js')
          }
  });

  mainWindow.webContents.session.clearStorageData([], (data) => {});
  mainWindow.loadURL(`file://${__dirname}/app.html#/login`);
  splashWindow.show();
  splashWindow.webContents.on('did-finish-load', () => {
    log.info('LOADING MAINWINDOW');
  });

  printWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    center: true,
    webPreferences:
      process.env.NODE_ENV === 'development' || process.env.E2E_BUILD === 'true'
        ? {
            nodeIntegration: true
          }
        : {
            preload: path.join(__dirname, 'dist/renderer.prod.js')
          }
  });
  printWindow.setMenu(null);
  printWindow.loadURL(`file://${__dirname}/printwindow.html`);

  ipcMain.on("printPDF", (event, title, content) => {
    // printWindow.show();
    printWindow.webContents.send("printPDF", title, content);
  });

  // when worker window is ready
  ipcMain.on("readyToPrintPDF", (event, title) => {
    const pdfPath = path.join(os.tmpdir(), `${title}.pdf`);
    // Use default printing options
    printWindow.webContents
      .printToPDF({pageSize: 'A4'})
      .then(data => {
      fs.writeFile(pdfPath, data, (error) => {
        if (error) throw error

        shell.openItem(pdfPath)
        event.sender.send('wrote-pdf', pdfPath)
      })
    }).catch(error => {
      console.log(error)
    })
  });

  mainWindow.webContents.on('did-finish-load', () => {
    log.info('MAINWINDOW DID FINISH LOAD');
    try {
      log.info('CLOSING SPLASH WINDOW');
      setTimeout(() => {
        splashWindow.close();
      }, 2000);
    } catch(e) {
      log.error(e);
    }
  });

  // @TODO: Use 'ready-to-show' event
  //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
  mainWindow.webContents.on('ready-to-show', () => {
    log.info('MAINWINDOW READY TO SHOW');
    try {
      log.info('CLOSING SPLASH WINDOW');
      setTimeout(() => {
        splashWindow.close();
      }, 2000);
    } catch(e) {
      log.error(e);
    }

    mainWindow.show();
    mainWindow.focus();

    if (
      process.env.NODE_ENV === 'development' ||
      process.env.DEBUG_PROD === 'true'
    ) {
      mainWindow.webContents.openDevTools({mode: 'right'})
    }

  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();

  mainWindow.maximize();

  // Remove this if your app does not use auto updates
  // eslint-disable-next-line
  // new AppUpdater();
};

/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('ready', createWindow);

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow();
});
