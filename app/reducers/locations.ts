// @flow
import { SETLOCATIONS, ISGETTINGLOCATIONS } from '../actions/locations';

const initialState = {
  isGettingLocations: false,
  locations: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETLOCATIONS:
      return {
        ...state,
        locations: action.locations,
      }
    case ISGETTINGLOCATIONS:
      return {
        ...state,
        isGettingLocations: action.isGettingLocations,
      }
    default:
      return state;
  }
}
