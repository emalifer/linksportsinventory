// @flow
import { SETITEMS, ISGETTINGITEMS, ISGETTINGITEM, ISSAVINGITEM, ISDELETINGITEM } from '../actions/items';

const initialState = {
  isGettingItems: false,
  items: [],
  isGettingItem: false,
  isSavingItem: false,
  deletingItemID: null,
  isDeletingItem: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETITEMS:
      return {
        ...state,
        items: action.items,
      }
    case ISGETTINGITEMS:
      return {
        ...state,
        isGettingItems: action.isGettingItems,
      }
    case ISGETTINGITEM:
      return {
        ...state,
        isGettingItem: action.isGettingItem
      }
    case ISSAVINGITEM:
      return {
        ...state,
        isSavingItem: action.isSavingItem
      }
    case ISDELETINGITEM:
      return {
        ...state,
        deletingItemID: action.deletingItemID,
        isDeletingItem: action.isDeletingItem
      }
    default:
      return state;
  }
}
