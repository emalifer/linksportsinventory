// @flow
import { SETPURCHASEORDERS, ISGETTINGPURCHASEORDERS, ISGETTINGPURCHASEORDER, ISSAVINGPURCHASEORDER, ISDELETINGPURCHASEORDER } from '../actions/purchaseorders';

const initialState = {
  isGettingPurchaseOrders: false,
  purchaseOrders: [],
  isGettingPurchaseOrder: false,
  isSavingPurchaseOrder: false,
  deletingPurchaseOrderID: null,
  isDeletingPurchaseOrder: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETPURCHASEORDERS:
      return {
        ...state,
        purchaseOrders: action.purchaseOrders,
      }
    case ISGETTINGPURCHASEORDERS:
      return {
        ...state,
        isGettingPurchaseOrders: action.isGettingPurchaseOrders,
      }
    case ISGETTINGPURCHASEORDER:
      return {
        ...state,
        isGettingPurchaseOrder: action.isGettingPurchaseOrder,
      }
    case ISSAVINGPURCHASEORDER:
      return {
        ...state,
        isSavingPurchaseOrder: action.isSavingPurchaseOrder,
      }
    case ISDELETINGPURCHASEORDER:
      return {
        ...state,
        deletingPurchaseOrderID: action.deletingPurchaseOrderID,
        isDeletingPurchaseOrder: action.isDeletingPurchaseOrder,
      }
    default:
      return state;
  }
}
