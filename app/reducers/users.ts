// @flow
import { SETUSERS, ISGETTINGUSERS } from '../actions/users';

const initialState = {
  isGettingUsers: false,
  users: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETUSERS:
      return {
        ...state,
        users: action.users,
      }
    case ISGETTINGUSERS:
      return {
        ...state,
        isGettingUsers: action.isGettingUsers,
      }
    default:
      return state;
  }
}
