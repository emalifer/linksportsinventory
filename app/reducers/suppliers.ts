// @flow
import { SETSUPPLIERS, ISGETTINGSUPPLIERS, ISGETTINGSUPPLIER, ISSAVINGSUPPLIER, ISDELETINGSUPPLIER } from '../actions/suppliers';

const initialState = {
  isGettingSuppliers: false,
  suppliers: [],
  isGettingSupplier: false,
  isSavingSupplier: false,
  deletingSupplierID: null,
  isDeletingSupplier: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETSUPPLIERS:
      return {
        ...state,
        suppliers: action.suppliers,
      }
    case ISGETTINGSUPPLIERS:
      return {
        ...state,
        isGettingSuppliers: action.isGettingSuppliers,
      }
    case ISGETTINGSUPPLIER:
      return {
        ...state,
        isGettingSupplier: action.isGettingSupplier
      }
    case ISSAVINGSUPPLIER:
      return {
        ...state,
        isSavingSupplier: action.isSavingSupplier
      }
    case ISDELETINGSUPPLIER:
      return {
        ...state,
        isDeletingSupplier: action.isDeletingSupplier
      }
    default:
      return state;
  }
}
