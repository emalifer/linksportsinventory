// @flow
import { SETDELIVERYRECEIPTS, ISGETTINGDELIVERYRECEIPTS, ISGETTINGDELIVERYRECEIPT, ISSAVINGDELIVERYRECEIPT, ISDELETINGDELIVERYRECEIPT } from '../actions/deliveryreceipts';

const initialState = {
  isGettingDeliveryReceipts: false,
  deliveryReceipts: [],
  isGettingDeliveryReceipt: false,
  isSavingDeliveryReceipt: false,
  deletingDeliveryReceiptID: null,
  isDeletingDeliveryReceipt: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETDELIVERYRECEIPTS:
      return {
        ...state,
        deliveryReceipts: action.deliveryReceipts,
      }
    case ISGETTINGDELIVERYRECEIPTS:
      return {
        ...state,
        isGettingDeliveryReceipts: action.isGettingDeliveryReceipts,
      }
    case ISGETTINGDELIVERYRECEIPT:
      return {
        ...state,
        isGettingDeliveryReceipt: action.isGettingDeliveryReceipt,
      }
    case ISSAVINGDELIVERYRECEIPT:
      return {
        ...state,
        isSavingDeliveryReceipt: action.isSavingDeliveryReceipt,
      }
    case ISDELETINGDELIVERYRECEIPT:
      return {
        ...state,
        deletingDeliveryReceiptID: action.deletingDeliveryReceiptID,
        isDeletingDeliveryReceipt: action.isDeletingDeliveryReceipt,
      }
    default:
      return state;
  }
}
