// @flow
import { SETCUSTOMERS, ISGETTINGCUSTOMERS, ISGETTINGCUSTOMER, ISSAVINGCUSTOMER, ISDELETINGCUSTOMER } from '../actions/customers';

const initialState = {
  isGettingCustomers: false,
  customers: [],
  isGettingCustomer: false,
  isSavingCustomer: false,
  deletingCustomerID: null,
  isDeletingCustomer: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETCUSTOMERS:
      return {
        ...state,
        customers: action.customers,
      }
    case ISGETTINGCUSTOMERS:
      return {
        ...state,
        isGettingCustomers: action.isGettingCustomers,
      }
    case ISGETTINGCUSTOMER:
      return {
        ...state,
        isGettingCustomer: action.isGettingCustomer
      }
    case ISSAVINGCUSTOMER:
      return {
        ...state,
        isSavingCustomer: action.isSavingCustomer
      }
    case ISDELETINGCUSTOMER:
      return {
        ...state,
        isDeletingCustomer: action.isDeletingCustomer
      }
    default:
      return state;
  }
}
