// @flow
import { LOGIN, LOGOUT } from '../actions/window';
import { ISLOGGINGIN } from '../actions/auth';

// DEBUG BY SETTING TRUE
const initialState = {
  isLoggingIn: false,
  isLoggedIn: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ISLOGGINGIN:
      return {
        ...state,
        isLoggingIn: action.isLoggingIn
      }
    case LOGIN:
      return {
        ...state,
        isLoggedIn: true,
      }
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
      }
    default:
      return state;
  }
}
