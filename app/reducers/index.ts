// @flow
import * as EStore from 'electron-store';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import window from './window';
import users from './users';
import contacts from './contacts';
import customers from './customers';
import items from './items';
import locations from './locations';
import suppliers from './suppliers';
import purchaseorders from './purchaseorders';
import deliveryreceipts from './deliveryreceipts';
import files from './files';

export const estore = new EStore();

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    window,
    users,
    contacts,
    customers,
    items,
    locations,
    suppliers,
    purchaseorders,
    deliveryreceipts,
    files
  });
}
