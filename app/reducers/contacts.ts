// @flow
import { SETCONTACTS, ISGETTINGCONTACTS } from '../actions/contacts';

const initialState = {
  isGettingContacts: false,
  contacts: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETCONTACTS:
      return {
        ...state,
        contacts: action.contacts,
      }
    case ISGETTINGCONTACTS:
      return {
        ...state,
        isGettingContacts: action.isGettingContacts,
      }
    default:
      return state;
  }
}
