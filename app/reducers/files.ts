// @flow
import { SETFILES, ISGETTINGFILES } from '../actions/files';

const initialState = {
  isGettingFiles: false,
  files: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SETFILES:
      return {
        ...state,
        files: action.files,
      }
    case ISGETTINGFILES:
      return {
        ...state,
        isGettingFiles: action.isGettingFiles,
      }
    default:
      return state;
  }
}
