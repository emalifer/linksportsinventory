
import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';

export const SETSUPPLIERS = 'suppliers/SETSUPPLIERS';
export const ISGETTINGSUPPLIERS = 'suppliers/ISGETTINGSUPPLIERS';

export const ISGETTINGSUPPLIER = 'suppliers/ISGETTINGSUPPLIER';

export const ISSAVINGSUPPLIER = 'suppliers/ISSAVINGSUPPLIER';

export const ISDELETINGSUPPLIER = 'suppliers/ISDELETINGSUPPLIER';

export const getSuppliers = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGSUPPLIERS,
      isGettingSuppliers: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_SUPPLIERS}?_format=json`)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETSUPPLIERS,
          suppliers: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGSUPPLIERS,
          isGettingSuppliers: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getSupplierByID = (id, cb) => {
  return (dispatch, getState) => {
    const suppliers = getState().suppliers.suppliers;
    const supplier = suppliers.find((supplier) => {
      return supplier.nid[0].value === id;
    });

    return supplier;
  }
}

export const getSupplier = (id,cb) => {

  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGSUPPLIER,
      isGettingSupplier: true
    });

    window.fetch(`${config.INVENTORY_URL}/node/${id}?_format=json`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        cb(respJson)
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGSUPPLIER,
          isGettingSupplier: false
        });
      })

  }
}

export const createOrUpdateSupplier = (method, supplier, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISSAVINGSUPPLIER,
      isSavingSupplier: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    if(method === 'PATCH') {
      const id = supplier.nid[0].value;
      url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    }

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method,
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          },
          body: JSON.stringify(supplier)
        })
        .then((resp) => {
          return resp.json()
        })
        .then((respJson) => {
          cb(respJson)
        })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          dispatch({
            type: ISSAVINGSUPPLIER,
            isSavingSupplier: false
          });
        })
    });
  }
}

export const deleteSupplier = (id, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISDELETINGSUPPLIER,
      deletingSupplierID: id,
      isDeletingSupplier: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    console.log('deleting supplier')

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          }
        })
        .then(() => {
          cb();
        })
        .finally(() => {
          dispatch({
            type: ISDELETINGSUPPLIER,
            deletingSupplierID: null,
            isDeletingSupplier: false
          });
        })
    })
  }
}
