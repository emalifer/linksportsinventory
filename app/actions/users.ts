
import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';

export const SETUSERS = 'users/SETUSERS';
export const ISGETTINGUSERS = 'users/ISGETTINGUSERS';

export const getUsers = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGUSERS,
      isGettingUsers: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    // console.log(name, pass);

    window.fetch(`${config.INVENTORY_URL}${routes.API_USERS}?_format=json`,
        {
          headers: {
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`
          }
        }
      )
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETUSERS,
          users: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGUSERS,
          isGettingUsers: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getUserByID = (id, cb) => {
  return (dispatch, getState) => {
    const users = getState().users.users;
    const user = users.find((user) => {
      return user.uid[0].value === id;
    });

    return user;
  }
}
