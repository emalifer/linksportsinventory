

import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';
import { TRUE } from 'node-sass';

export const SETFILES = 'files/SETFILES';
export const ISGETTINGFILES = 'files/ISGETTINGFILES';

export const getFiles = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGFILES,
      isGettingFiles: true
    });

    const myHeaders = new Headers();
    myHeaders.append('pragma', 'no-cache');
    myHeaders.append('cache-control', 'no-cache');

    const myInit = {
      method: 'GET',
      headers: myHeaders,
    };

    window.fetch(`${config.INVENTORY_URL}${routes.API_FILES}?_format=json`, myInit)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETFILES,
          files: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGFILES,
          isGettingFiles: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}
