
import config from '../../configs/config.json';
import routes from '../constants/routes.json';

export const SETCONTACTS = 'contacts/SETCONTACTS';
export const ISGETTINGCONTACTS = 'contacts/ISGETTINGCONTACTS';

export const getContacts = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGCONTACTS,
      isGettingContacts: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_CONTACTS}?_format=json`)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETCONTACTS,
          contacts: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGCONTACTS,
          isGettingContacts: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getContactByID = (id, cb) => {
  return (dispatch, getState) => {
    const contacts = getState().contacts.contacts;
    const contact = contacts.find((contact) => {
      return contact.nid[0].value === id;
    });

    return contact;
  }
}
