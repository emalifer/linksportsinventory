
import numeral from 'numeraljs';

import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';
import moment from 'moment';

export const SETDELIVERYRECEIPTS = 'deliveryreceipts/SETDELIVERYRECEIPTS';
export const ISGETTINGDELIVERYRECEIPTS = 'deliveryreceipts/ISGETTINGDELIVERYRECEIPTS';

export const ISGETTINGDELIVERYRECEIPT = 'deliveryreceipts/ISGETTINGDELIVERYRECEIPT';

export const ISSAVINGDELIVERYRECEIPT = 'deliveryreceipts/ISSAVINGDELIVERYRECEIPT';

export const ISDELETINGDELIVERYRECEIPT = 'deliveryreceipts/ISDELETINGDELIVERYRECEIPT';

export const STATUS = {
  PENDING: 'pending',
  APPROVED: 'approved',
  CANCELLED: 'cancelled'
}

export const getDeliveryReceipts = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGDELIVERYRECEIPTS,
      isGettingDeliveryReceipts: true
    });

    const myHeaders = new Headers();
    myHeaders.append('pragma', 'no-cache');
    myHeaders.append('cache-control', 'no-cache');

    const myInit = {
      method: 'GET',
      headers: myHeaders,
    };

    window.fetch(`${config.INVENTORY_URL}${routes.API_DELIVERYRECEIPTS}?_format=json`, myInit)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETDELIVERYRECEIPTS,
          deliveryReceipts: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGDELIVERYRECEIPTS,
          isGettingDeliveryReceipts: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getDeliveryReceipt = (id,cb) => {

  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGDELIVERYRECEIPT,
      isGettingDeliveryReceipt: true
    });

    window.fetch(`${config.INVENTORY_URL}/node/${id}?_format=json`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        cb(respJson)
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGDELIVERYRECEIPT,
          isGettingDeliveryReceipt: false
        });
      })

  }
}

export const createOrUpdateDeliveryReceipt = (method, deliveryReceipt, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISSAVINGDELIVERYRECEIPT,
      isSavingDeliveryReceipt: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    if(method === 'PATCH') {
      const id = deliveryReceipt.nid[0].value;
      url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    }

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method,
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          },
          body: JSON.stringify(deliveryReceipt)
        })
        .then((resp) => {
          return resp.json()
        })
        .then((respJson) => {
          cb(respJson)
        })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          dispatch({
            type: ISSAVINGDELIVERYRECEIPT,
            isSavingDeliveryReceipt: false
          });
        })
    })
  }
}

export const deleteDeliveryReceipt = (id, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISDELETINGDELIVERYRECEIPT,
      deletingDeliveryReceiptID: id,
      isDeletingDeliveryReceipt: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          }
        })
        .then(() => {
          cb();
        })
        .finally(() => {
          dispatch({
            type: ISDELETINGDELIVERYRECEIPT,
            deletingDeliveryReceiptID: null,
            isDeletingDeliveryReceipt: false
          });
        })
    });
  }
}

export const getNextDeliveryReceiptNumber = (cb) => {
  return (dispatch, getState) => {

    // console.log(getState().deliveryreceipts);
    const deliveryReceipts = getState().deliveryreceipts.deliveryReceipts;

    if(deliveryReceipts.length > 0) {
      const deliveryReceiptNumbersSorted = deliveryReceipts.map((deliveryReceipt) => {
        return deliveryReceipt.title[0].value;
      }).sort((a, b) => {
        return a - b;
      });

      const highestDeliveryReceiptNumber = deliveryReceiptNumbersSorted.pop();
      cb(padZeroes(parseInt(highestDeliveryReceiptNumber, 10) + 1, 6));
    } else {
      cb(padZeroes(1,6));
    }
  }
}

const padZeroes = (num, size) => { return ('000000000' + num).substr(-size); }

export const getDeliveryReceiptBadgeStatus = (status) => {
  switch(status) {
    case STATUS.PENDING:
      return 'badge-warning';
    case STATUS.APPROVED:
      return 'badge-success';
    case STATUS.CANCELLED:
      return 'badge-danger';
    default:
      break;
  }
}

export const getDeliveryReceiptRowStatus = (status) => {
  switch(status) {
    case STATUS.PENDING:
      return 'table-warning';
    case STATUS.APPROVED:
      return 'table-success';
    case STATUS.CANCELLED:
      return 'table-dark';
    default:
      break;
  }
}

export const getDeliveryReceiptsWithItemID = (id) => {
  return (dispatch, getState) => {
    const deliveryReceipts = getState().deliveryreceipts.deliveryReceipts;

    const deliveryReceiptsWithItemID = deliveryReceipts.filter((deliveryReceipt) => {
      let flag = false;
      deliveryReceipt.field_purchase_order_items.forEach((deliveryReceiptItem) => {
        if(deliveryReceiptItem.target_id === id && deliveryReceipt.field_purchase_order_status[0].value === STATUS.APPROVED) {
          flag = true;
        }
      });

      return flag;
    });

    return deliveryReceiptsWithItemID;
  }
}
