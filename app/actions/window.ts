

export const LOGIN = 'window/LOGIN';
export const LOGOUT = 'window/LOGOUT';

export const login = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: LOGIN
    });

    cb();
  }
}

export const logout = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: LOGOUT
    });

    cb();
  }
}
