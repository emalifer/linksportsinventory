
import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';

export const SETCUSTOMERS = 'customers/SETCUSTOMERS';
export const ISGETTINGCUSTOMERS = 'customers/ISGETTINGCUSTOMERS';

export const ISGETTINGCUSTOMER = 'customers/ISGETTINGCUSTOMER';

export const ISSAVINGCUSTOMER = 'customers/ISSAVINGCUSTOMER';

export const ISDELETINGCUSTOMER = 'customers/ISDELETINGCUSTOMER';

export const getCustomers = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGCUSTOMERS,
      isGettingCustomers: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_CUSTOMERS}?_format=json`)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETCUSTOMERS,
          customers: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGCUSTOMERS,
          isGettingCustomers: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getCustomerByID = (id, cb) => {
  return (dispatch, getState) => {
    const customers = getState().customers.customers;
    const customer = customers.find((customer) => {
      return customer.nid[0].value === id;
    });

    return customer;
  }
}

export const getCustomer = (id,cb) => {

  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGCUSTOMER,
      isGettingCustomer: true
    });

    window.fetch(`${config.INVENTORY_URL}/node/${id}?_format=json`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        cb(respJson)
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGCUSTOMER,
          isGettingCustomer: false
        });
      })

  }
}

export const createOrUpdateCustomer = (method, customer, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISSAVINGCUSTOMER,
      isSavingCustomer: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    if(method === 'PATCH') {
      const id = customer.nid[0].value;
      url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    }

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method,
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          },
          body: JSON.stringify(customer)
        })
        .then((resp) => {
          return resp.json()
        })
        .then((respJson) => {
          cb(respJson)
        })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          dispatch({
            type: ISSAVINGCUSTOMER,
            isSavingCustomer: false
          });
        })
    });
  }
}

export const deleteCustomer = (id, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISDELETINGCUSTOMER,
      deletingCustomerID: id,
      isDeletingCustomer: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    console.log('deleting customer')

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          }
        })
        .then(() => {
          cb();
        })
        .finally(() => {
          dispatch({
            type: ISDELETINGCUSTOMER,
            deletingCustomerID: null,
            isDeletingCustomer: false
          });
        })
    })
  }
}
