
import config from '../../configs/config.json';
import routes from '../constants/routes.json';

export const SETLOCATIONS = 'customers/SETLOCATIONS';
export const ISGETTINGLOCATIONS = 'customers/ISGETTINGLOCATIONS';

export const getLocations = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGLOCATIONS,
      isGettingLocations: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_LOCATIONS}?_format=json`)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETLOCATIONS,
          locations: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGLOCATIONS,
          isGettingLocations: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getLocationByID = (id, cb) => {
  return (dispatch, getState) => {
    const locations = getState().locations.locations;
    const location = locations.find((location) => {
      return location.nid[0].value === id;
    });

    return location;
  }
}
