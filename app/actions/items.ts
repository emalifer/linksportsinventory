
import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';

export const SETITEMS = 'items/SETITEMS';
export const ISGETTINGITEMS = 'items/ISGETTINGITEMS';

export const ISGETTINGITEM = 'items/ISGETTINGITEM';

export const ISSAVINGITEM = 'items/ISSAVINGITEM';

export const ISDELETINGITEM = 'items/ISDELETINGITEM';

export const getItems = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGITEMS,
      isGettingItems: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_ITEMS}?_format=json`)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETITEMS,
          items: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGITEMS,
          isGettingItems: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getItemByID = (id, cb) => {
  return (dispatch, getState) => {
    const items = getState().items.items;
    const item = items.find((item) => {
      return item.nid[0].value === id;
    });

    return item;
  }
}

export const getItem = (id,cb) => {

  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGITEM,
      isGettingItem: true
    });

    window.fetch(`${config.INVENTORY_URL}/node/${id}?_format=json`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        cb(respJson)
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGITEM,
          isGettingItem: false
        });
      })

  }
}

export const createOrUpdateItem = (method, item, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISSAVINGITEM,
      isSavingItem: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    if(method === 'PATCH') {
      const id = item.nid[0].value;
      url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    }

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method,
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          },
          body: JSON.stringify(item)
        })
        .then((resp) => {
          return resp.json()
        })
        .then((respJson) => {
          cb(respJson)
        })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          dispatch({
            type: ISSAVINGITEM,
            isSavingItem: false
          });
        })
    });
  }
}

export const deleteItem = (id, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISDELETINGITEM,
      deletingItemID: id,
      isDeletingItem: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          }
        })
        .then(() => {
          cb();
        })
        .finally(() => {
          dispatch({
            type: ISDELETINGITEM,
            deletingItemID: null,
            isDeletingItem: false
          });
        })
    });
  }
}
