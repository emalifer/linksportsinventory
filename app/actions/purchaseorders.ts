
import numeral from 'numeraljs';

import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import { estore } from '../reducers/index';
import moment from 'moment';

export const SETPURCHASEORDERS = 'purchaseorders/SETPURCHASEORDERS';
export const ISGETTINGPURCHASEORDERS = 'purchaseorders/ISGETTINGPURCHASEORDERS';

export const ISGETTINGPURCHASEORDER = 'purchaseorders/ISGETTINGPURCHASEORDER';

export const ISSAVINGPURCHASEORDER = 'purchaseorders/ISSAVINGPURCHASEORDER';

export const ISDELETINGPURCHASEORDER = 'purchaseorders/ISDELETINGPURCHASEORDER';

export const STATUS = {
  PENDING: 'pending',
  APPROVED: 'approved',
  CANCELLED: 'cancelled'
}

export const CURRENCY = {
  USD: 'usd',
  SGD: 'sgd',
  EUR: 'eur',
  PHP: 'php'
}

export const getCurrencyDisplay = (curr) => {
  switch(curr) {
    case CURRENCY.USD:
      return 'USD $';
    case CURRENCY.SGD:
      return 'S$';
    case CURRENCY.EUR:
      return 'EURO €';
    case CURRENCY.PHP:
      return 'Php';
  }
}

export const getPurchaseOrders = (cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGPURCHASEORDERS,
      isGettingPurchaseOrders: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_PURCHASEORDERS}?_format=json`)
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        dispatch({
          type: SETPURCHASEORDERS,
          purchaseOrders: respJson
        });
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGPURCHASEORDERS,
          isGettingPurchaseOrders: false
        });
      })

    if(typeof cb === "function") {
      cb();
    }
  }
}

export const getPurchaseOrder = (id,cb) => {

  return (dispatch, getState) => {
    dispatch({
      type: ISGETTINGPURCHASEORDER,
      isGettingPurchaseOrder: true
    });

    window.fetch(`${config.INVENTORY_URL}/node/${id}?_format=json`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((resp) => {
        return resp.json()
      })
      .then((respJson) => {
        cb(respJson)
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        dispatch({
          type: ISGETTINGPURCHASEORDER,
          isGettingPurchaseOrder: false
        });
      })

  }
}

export const createOrUpdatePurchaseOrder = (method, purchaseOrder, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISSAVINGPURCHASEORDER,
      isSavingPurchaseOrder: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    if(method === 'PATCH') {
      const id = purchaseOrder.nid[0].value;
      url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    }

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method,
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          },
          body: JSON.stringify(purchaseOrder)
        })
        .then((resp) => {
          return resp.json()
        })
        .then((respJson) => {
          cb(respJson)
        })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {
          dispatch({
            type: ISSAVINGPURCHASEORDER,
            isSavingPurchaseOrder: false
          });
        })
    })

  }
}

export const deletePurchaseOrder = (id, cb) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISDELETINGPURCHASEORDER,
      deletingPurchaseOrderID: id,
      isDeletingPurchaseOrder: true
    });

    const name = estore.get('name');
    const pass = estore.get('pass');

    let url = `${config.INVENTORY_URL}/node/${id}?_format=json`;
    let tokenurl = `${config.INVENTORY_URL}/rest/session/token`;

    console.log('deleting purchase order')

    window.fetch(tokenurl).then((resp) => {
      return resp.text();
    }).then((token) => {
      window.fetch(url,
        {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': token,
            'Authorization': `Basic ${btoa(`${name}:${pass}`)}`,
            'Accept': 'application/json'
          }
        })
        .then(() => {
          cb();
        })
        .finally(() => {
          dispatch({
            type: ISDELETINGPURCHASEORDER,
            deletingPurchaseOrderID: null,
            isDeletingPurchaseOrder: false
          });
        })
    })
  }
}

export const getNextPurchaseOrderNumber = (cb) => {
  return (dispatch, getState) => {
    const purchaseOrders = getState().purchaseorders.purchaseOrders;

    if(purchaseOrders.length > 0) {
      const purchaseOrderNumbersSorted = purchaseOrders.map((purchaseOrder) => {
        return purchaseOrder.title[0].value;
      }).sort((a, b) => {
        return a - b;
      });

      const highestPurchaseOrderNumber = purchaseOrderNumbersSorted.pop();
      cb(padZeroes(parseInt(highestPurchaseOrderNumber, 10) + 1, 6));
    } else {
      cb(padZeroes(1,6));
    }
  }
}

const padZeroes = (num, size) => { return ('000000000' + num).substr(-size); }

export const getPurchaseOrderBadgeStatus = (status) => {
  switch(status) {
    case STATUS.PENDING:
      return 'badge-warning';
    case STATUS.APPROVED:
      return 'badge-success';
    case STATUS.CANCELLED:
      return 'badge-danger';
    default:
      break;
  }
}

export const getPurchaseOrderRowStatus = (status) => {
  switch(status) {
    case STATUS.PENDING:
      return 'table-warning';
    case STATUS.APPROVED:
      return 'table-success';
    case STATUS.CANCELLED:
      return 'table-dark';
    default:
      break;
  }
}

export const getPurchaseOrdersWithItemID = (id) => {
  return (dispatch, getState) => {
    const purchaseOrders = getState().purchaseorders.purchaseOrders;

    const purchaseOrdersWithItemID = purchaseOrders.filter((purchaseOrder) => {
      let flag = false;
      purchaseOrder.field_purchase_order_items.forEach((purchaseOrderItem) => {
        if(purchaseOrderItem.target_id === id && purchaseOrder.field_purchase_order_status[0].value === STATUS.APPROVED) {
          flag = true;
        }
      });

      return flag;
    });

    return purchaseOrdersWithItemID;
  }
}
