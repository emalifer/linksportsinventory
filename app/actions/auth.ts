
import config from '../../configs/config.json';
import routes from '../constants/routes.json';
import * as WINDOWACTIONS from './window';
import { estore } from '../reducers/index';

export const ISLOGGINGIN = 'auth/ISLOGGINGIN';

export const login = (name, pass, cb, err) => {
  return (dispatch, getState) => {
    dispatch({
      type: ISLOGGINGIN,
      isLoggingIn: true
    });

    window.fetch(`${config.INVENTORY_URL}${routes.API_AUTH}/login?_format=hal_json`,
      {
        method: 'POST',
        body: JSON.stringify({name, pass}),
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then((resp) => {
        // console.log(resp)
        if(!resp.ok) {
          throw Error(resp.json());
        }

        return resp.json()
      })
      .then((respJson) => {
        console.log(respJson)
        estore.set('name', name);
        estore.set('pass', pass);
        estore.set('user', respJson);
        dispatch({
          type: WINDOWACTIONS.LOGIN
        });
        cb(respJson);
      })
      .catch((error) => {
        err(error);
      })
      .finally(() => {
        dispatch({
          type: ISLOGGINGIN,
          isLoggingIn: false
        });
      })
  }
}

export const isAdmin = () => {
  const user = estore.get('user');

  if(user.current_user.roles) {
    if(user.current_user.roles.indexOf('administrator') !== -1) {
      return true;
    }
  }

  return false;
}

export const isOwner = (id) => {
  const user = estore.get('user');

  if(user.current_user.uid === String(id)) {
    return true;
  }

  return false;
}
