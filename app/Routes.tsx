import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router';
import { withRouter } from 'react-router-dom';
import App from './containers/App';
import routes from './constants/routes.json';
import AdminLayout from './components/AdminLayout/AdminLayout';
import Login from './components/Login/Login';
import NotFound from './components/NotFound/NotFound';
import Logout from './components/Logout/Logout';

type Props = {
  location: string
};


class Routes extends Component<Props> {

  componentDidMount = () => {
  }

  render() {

    const { location, match } = this.props;

    return (
      <App>
        <Switch location={location}>
          <Route path={routes.ADMIN} component={AdminLayout} />
          <Route path={routes.LOGIN} component={Login} exact />
          <Route path={routes.LOGOUT} component={Logout} exact />
          <Route exact path={routes.NotFound} component={NotFound} />
        </Switch>
      </App>
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
    },
    dispatch
  );

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Routes));
