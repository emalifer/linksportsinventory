import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { push } from 'connected-react-router';
import * as AuthActions from '../../actions/auth';
import routes from '../../constants/routes.json';

const { session } = require('electron').remote;

class Login extends Component {
  componentDidMount = () => {
    const { isLoggedIn } = this.props;
    session.defaultSession.clearStorageData();
    if (isLoggedIn) {
      this.goToAdmin();
    }
  }

  signIn = () => {
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    this.props.login(username, password, (resp) => {
      this.props.push(routes.ADMIN);
    }, (err) => {
      console.log(err);
    });
  }

  goToAdmin = () => {
    this.props.push(routes.ADMIN);
  }

  render() {

    const { isLoggingIn } = this.props;

    return (
      <div id="login">
        <form className="form-signin" onSubmit={(e) => { e.preventDefault(); this.signIn(e); }}>
          <h1 className="h3 mb-3 font-weight-normal text-white">Please sign in</h1>
          <label htmlFor="username" className="sr-only">Username</label>
          <input type="text" name="username" id="username" className="form-control" defaultValue="admin" placeholder="Username" required autoFocus disabled={isLoggingIn} readOnly />
          <label htmlFor="password" className="sr-only">Password</label>
          <input type="password" name="password" id="password" className="form-control" defaultValue="=hYQ9-2?dBII" placeholder="Password" disabled={isLoggingIn} required readOnly />
          <button className="btn btn-lg btn-light btn-block" type="submit" disabled={isLoggingIn}>Sign in</button>
          <p className="mt-3 mb-3 text-white">&copy; 2019-2020 Linksports Inventory</p>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  isLoggedIn: PropTypes.bool
};

Login.defaultProps = {
  isLoggedIn: false
}

const mapStateToProps = (state) => {
  return {
    isLoggingIn: state.window.isLoggingIn,
    isLoggedIn: state.window.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    login: AuthActions.login,
    push
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login));
