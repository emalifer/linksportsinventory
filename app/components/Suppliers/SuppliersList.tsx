import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import { Typeahead } from 'react-bootstrap-typeahead';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import routes from '../../constants/routes.json';
import { getSuppliers, deleteSupplier } from '../../actions/suppliers';
import { isAdmin, isOwner } from '../../actions/auth';
import { getUserByID } from '../../actions/users';

const { dialog } = require('electron').remote;

class SuppliersList extends Component {

  constructor(props) {
    super(props)
  }

  state = {
    suppliers: [],
    filter: {
      enabled: false,
      title: [{value: ''}]
    },
    suppliers: [],
    users: []
  }

  componentDidMount = () => {

    this.setState({suppliers: this.props.suppliers}, () => {
      if(this.state.filter.enabled) {
        this.filterSuppliers();
      }
    });

    this.mapUsers();
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(prevProps.suppliers !== this.props.suppliers) {
      this.setState({suppliers: this.props.suppliers}, () => {
        if(this.state.filter.enabled) {
          this.filterSuppliers();
        }
      });
    }
    if(prevProps.users !== this.props.users) {
      this.mapUsers()
    }

    if(prevState.filter.enabled !== this.state.filter.enabled && !this.state.filter.enabled) {
      this.clearFilter()
    }
  }

  showHideFilter = () => {
    this.setState(produce(this.state, draft => {
      draft.filter.enabled = !this.state.filter.enabled;
    }));
  }

  mapUsers = () => {
    const users = this.props.users.map((user) => {
      return {
        id: user.uid[0].value,
        label: user.name[0].value
      }
    });
    this.setState({users});
  }

  changeTitleFilter = (e) => {
    this.setState(produce(this.state, draft => {
      draft.filter.title[0].value = e.target.value;
    }));
  }


  clearFilter = () => {

    this.setState(produce(this.state, draft => {
      draft.filter.title = [{value: ''}];
    }), () => { this.filterSuppliers(); });
  }

  filterSuppliers = (e) => {

    if(e) {
      e.preventDefault();
    }

    const { filter } = this.state;
    let suppliers = this.props.suppliers;

    if(filter.title[0].value.length > 0) {
      suppliers = suppliers.filter((supplier) => {
        return supplier.title[0].value.match(filter.title[0].value)
      });
    }

    this.setState({suppliers});
  }

  confirmDeleteSupplier = (supplier) => {

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete supplier ${supplier.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteSupplier(supplier.nid[0].value, () => {
        this.props.getSuppliers(() => {
        })
      })
    }
  }

  render() {

    const { isGettingSuppliers, deletingSupplierID, isDeletingSupplier } = this.props;
    const {suppliers, filter } = this.state;

    return (
      <>
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase active">Suppliers</li>
          </ol>
        </nav>

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-parachute-box mr-2"></i>Suppliers</h1>
          <div className="float-right">
            <NavLink className="btn btn-outline-success btn-lg" to={`${routes.SUPPLIERS}/create`}><i className="fas fa-pen mr-3"></i>Create</NavLink>
          </div>
        </div>

        <h4 className="float-left">Suppliers List</h4>
        <ul className="nav nav-tabs float-right" style={{borderBottom: '0px'}}>
          <li className="nav-item">
            <a className={`nav-link cursor-pointer ${filter.enabled ? `active` : ``}`} onClick={() => { this.showHideFilter(); }}>
              <i className="fas fa-filter mr-2"></i>
              Filter
            </a>
          </li>
        </ul>
        <div className="clearfix"></div>

        <div className={`filter-box border-top border-left border-right p-3 ${filter.enabled ? `` : `d-none`}`}>
          <form onSubmit={(e) => { this.filterSuppliers(e); }}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-4 pl-0">
                  <div className="form-group">
                    <label>Filter by Supplier Name</label>
                    <input className="form-control" placeholder="Supplier Name" value={filter.title[0].value} onChange={(e) => { this.changeTitleFilter(e) }} />
                  </div>
                </div>
                <div className="col-4 pr-0">
                  <div className="float-right">
                    <button onClick={() => { this.clearFilter(); }} className="btn btn-link mr-2 text-dark">Clear Filter</button>
                    <button type="submit" className="btn btn-outline-success"><i className="fas fa-filter mr-3"></i>Filter</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div className="table-responsive">
          <table className="table table-striped table-sm table-hover">
            <thead>
              <tr>
                <th>Supplier</th>
                <th width="115">Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingSuppliers &&
                <tr>
                  <td colSpan="2">
                    <em>Loading Suppliers</em>
                  </td>
                </tr>
              }
              {!isGettingSuppliers &&
                <>
                {suppliers.length > 0 &&
                  <>
                    {suppliers.map((supplier, idx) => {
                      return (
                        <tr key={supplier.nid[0].value} className={`${deletingSupplierID === supplier.nid[0].value ? `table-danger` : ``}`}>
                          <td>
                            <p style={{fontSize: '1.2rem'}} className="mb-0">
                              {supplier.title[0].value}
                            </p>
                          </td>
                          <td>
                            <NavLink className={`btn btn-outline-success btn-sm mr-1 ${deletingSupplierID === supplier.nid[0].value ? `disabled` : ``}`} to={`${routes.SUPPLIERS}/${supplier.nid[0].value}`}>
                              <i className="fas fa-search"></i>
                            </NavLink>
                            {(() => {
                              let editable = false;

                              if(isAdmin() || isOwner(supplier.uid[0].target_id)) {
                                editable = true;
                              }

                              if(editable) {
                                return (
                                  <NavLink className={`btn btn-outline-dark btn-sm mr-1 ${deletingSupplierID === supplier.nid[0].value ? `disabled` : ``}`} to={`${routes.SUPPLIERS}/${supplier.nid[0].value}/edit`}>
                                    <i className="fas fa-pen"></i>
                                  </NavLink>
                                )
                              }
                            })()}
                            {(() => {
                              let deletable = false;

                              if(isAdmin() || isOwner(supplier.uid[0].target_id)) {
                                deletable = true;
                              }

                              if(deletable) {
                                return (
                                  <button className={`btn btn-outline-danger btn-sm ${deletingSupplierID === supplier.nid[0].value ? `disabled` : ``}`} onClick={() => { this.confirmDeleteSupplier(supplier) }}>
                                    <i className="fas fa-trash"></i>
                                  </button>
                                )
                              }
                            })()}
                          </td>
                        </tr>
                      )
                    })}
                  </>
                }
                {suppliers.length === 0 &&
                  <tr>
                    <td colSpan="2">
                      <em>No Suppliers</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {suppliers.length} Supplier{suppliers.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    suppliers: state.suppliers.suppliers,
    users: state.users.users,
    isGettingSuppliers: state.suppliers.isGettingSuppliers,
    deletingSupplierID: state.suppliers.deletingSupplierID,
    isDeletingSupplier: state.suppliers.isDeletingSupplier
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getSuppliers,
    deleteSupplier,
    getUserByID,
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuppliersList);
