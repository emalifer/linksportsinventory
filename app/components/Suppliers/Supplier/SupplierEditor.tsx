import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import { ToastContainer, toast } from 'react-toastify';
import Barcode from 'react-barcode';
import CKEditor from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Typeahead } from 'react-bootstrap-typeahead';

import config from '../../../../configs/config';
import routes from '../../../constants/routes.json';
import {
  getSuppliers,
  getSupplier,
  createOrUpdateSupplier,
  getSupplierByID
} from '../../../actions/suppliers';
import sample from './sample.png';

const MODE = {
  CREATE: 'create',
  UPDATE: 'update'
}

class SupplierEditor extends Component {

  constructor(props) {
    super(props);
    this.titleInput = React.createRef();
  }

  state = {
    mode: '',
    supplier: {
      type: [{target_id: "supplier"}],
      title: [{value: '', error: false}],
      body: [{value: ''}],
      field_supplier_code: [{value: '', error: false}],
      field_supplier_address_street: [{value: ''}],
      field_supplier_address_city: [{value: ''}],
      field_supplier_address_state: [{value: ''}],
      field_supplier_address_zip: [{value: ''}],
      field_supplier_address_country: [{value: ''}],
      field_supplier_telephone: [{value: ''}],
      field_supplier_email_address: [{value: ''}],
      supplier_contact_name: [{value: ''}]
    }
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getSupplier(this.props.match.params.id, (supplier) => {
        console.log(supplier);
        this.setState(
          produce(this.state, draft => {
            draft.supplier = supplier;

            if(supplier.body.length === 0) {
              draft.supplier.body = [{value: ''}];
            }

            if(supplier.field_supplier_code.length === 0) {
              draft.supplier.field_supplier_code = [{value: ''}];
            }

            if(supplier.field_supplier_address_street.length === 0) {
              draft.supplier.field_supplier_address_street = [{value: ''}];
            }

            if(supplier.field_supplier_address_city.length === 0) {
              draft.supplier.field_supplier_address_city = [{value: ''}];
            }

            if(supplier.field_supplier_address_state.length === 0) {
              draft.supplier.field_supplier_address_state = [{value: ''}];
            }

            if(supplier.field_supplier_address_zip.length === 0) {
              draft.supplier.field_supplier_address_zip = [{value: ''}];
            }

            if(supplier.field_supplier_address_country.length === 0) {
              draft.supplier.field_supplier_address_country = [{value: ''}];
            }

            if(supplier.field_supplier_telephone.length === 0) {
              draft.supplier.field_supplier_telephone = [{value: ''}];
            }

            if(supplier.field_supplier_email_address.length === 0) {
              draft.supplier.field_supplier_email_address = [{value: ''}];
            }

            if(supplier.supplier_contact_name?.length === 0) {
              draft.supplier.supplier_contact_name = [{value: ''}];
            }

            draft.mode = MODE.UPDATE;
          })
        );
      })
    } else {
      this.setState(
        produce(this.state, draft => {
          draft.mode = MODE.CREATE;
        })
      );
    }

  }

  componentDidUpdate = (prevProps) => {
  }

  changeTitle = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.title[0].value = e.target.value;
      draft.supplier.title[0].error = false;
    }));
  }

  changeSupplierCode = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_code[0].value = e.target.value;
      draft.supplier.field_supplier_code[0].error = false;
    }));
  }

  changeBody = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.body[0].value = e.target.value;
      draft.supplier.body[0].error = false;
    }));
  }

  changeStreet = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_address_street[0].value = e.target.value;
    }));
  }

  changeCity = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_address_city[0].value = e.target.value;
    }));
  }

  changeState = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_address_state[0].value = e.target.value;
    }));
  }

  changeZip = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_address_zip[0].value = e.target.value;
    }));
  }

  changeCountry = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_address_country[0].value = e.target.value;
    }));
  }

  changePhone = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_telephone[0].value = e.target.value;
    }));
  }

  changeEmail = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.field_supplier_email_address[0].value = e.target.value;
    }));
  }

  changeContactName = (e) => {
    this.setState(produce(this.state, draft => {
      draft.supplier.supplier_contact_name[0].value = e.target.value;
    }));
  }

  validateSupplier = () => {
    let flag = true;
    const { supplier } = this.state;

    this.setState(produce(this.state, draft => {
      if(supplier.title[0].value.length === 0) {
        flag = false;
        draft.supplier.title[0].error = true;
        toast.error("Please enter a Supplier Name");
      }
      if(supplier.field_supplier_code[0].value.length === 0) {
        flag = false;
        draft.supplier.field_supplier_code[0].error = true;
        toast.error("Please enter a Supplier Code");
      }
    }));

    return flag;
  }

  saveSupplier = (e) => {

    e.preventDefault();

    if(this.validateSupplier()) {

      let method = 'POST';

      switch(this.state.mode) {
        case MODE.CREATE:
          method = 'POST';
          break;
        case MODE.UPDATE:
          method = 'PATCH';
          break;
        default:
          break;
      }

      this.props.createOrUpdateSupplier(method, this.state.supplier, (resp) => {
        this.props.getSuppliers(() => {
          this.props.history.push(`${routes.SUPPLIERS}/${resp.nid[0].value}`)
        });
      })
    }

  }

  render() {

    const { isSavingSupplier } = this.props;
    const { mode, supplier }  = this.state;

    return (
      <>
        <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop={false} closeOnClick rtl={false} pauseOnVisibilityChange draggable pauseOnHover />

        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.SUPPLIERS}>Suppliers</NavLink></li>
            {mode === MODE.CREATE &&
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{supplier.title[0].value ? supplier.title[0].value : `Create`}</li>
            }
            {mode === MODE.UPDATE &&
              <>
                <li className="breadcrumb-item font-weight-bold text-uppercase">
                  <NavLink className="text-success" to={`${routes.SUPPLIERS}/${supplier.nid[0].value ? supplier.nid[0].value : ``}`}>{supplier.title[0].value ? supplier.title[0].value : ``}</NavLink>
                </li>
                <li className="breadcrumb-item font-weight-bold text-uppercase active">Edit</li>
              </>
            }
          </ol>
        </nav>

        <form onSubmit={(e) => { this.saveSupplier(e); }}>
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="col-8 pl-0">
              <h1 className="h2 text-success font-weight-bold text-uppercase">
                <i className="fas fa-parachute-box mr-2"></i>
                {supplier.title[0].value ? supplier.title[0].value : `New Supplier`}
              </h1>
            </div>
            {/* <div className="text-right col-4 pr-0">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingItem}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingItem ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingItem ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div> */}
          </div>

          <div className="card p-4 mb-4">
            <div className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="supplierName">Supplier Name</label></h5>
                      <input type="text" id="supplierName" className={`form-control ${supplier.title[0].error ? `is-invalid` : ``}`} disabled={isSavingSupplier} placeholder="Supplier Name" value={supplier.title[0].value} onChange={(e) => { this.changeTitle(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="supplierCode">Supplier Code</label></h5>
                      <input type="text" id="supplierCode" className={`form-control ${supplier.field_supplier_code[0].error ? `is-invalid` : ``}`} disabled={isSavingSupplier} placeholder="Supplier Code" value={supplier.field_supplier_code[0].value} onChange={(e) => { this.changeSupplierCode(e) }} />
                    </div>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <h5 className="card-title">Supplier Description</h5>
                    <textarea className="form-control" rows="3" value={supplier.body[0].value} onChange={(e) => { this.changeBody(e)} } />
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="">Supplier Address </label></h5>
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingSupplier} placeholder="Street" value={supplier.field_supplier_address_street[0]?.value} onChange={(e) => { this.changeStreet(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingSupplier} placeholder="City" value={supplier.field_supplier_address_city[0]?.value} onChange={(e) => { this.changeCity(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingSupplier} placeholder="State" value={supplier.field_supplier_address_state[0]?.value} onChange={(e) => { this.changeState(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingSupplier} placeholder="Zip" value={supplier.field_supplier_address_zip[0]?.value} onChange={(e) => { this.changeZip(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingSupplier} placeholder="Country" value={supplier.field_supplier_address_country[0]?.value} onChange={(e) => { this.changeCountry(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="supplierPhone">Supplier Telephone</label></h5>
                      <input type="text" id="supplierPhone" className={`form-control`} disabled={isSavingSupplier} placeholder="Supplier Telephone" value={supplier.field_supplier_telephone[0]?.value} onChange={(e) => { this.changePhone(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="supplierEmail">Supplier Email</label></h5>
                      <input type="text" id="supplierEmail" className={`form-control`} disabled={isSavingSupplier} placeholder="Supplier Email" value={supplier.field_supplier_email_address[0]?.value} onChange={(e) => { this.changeEmail(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="supplierContact">Supplier Contact Name</label></h5>
                      <input type="text" id="supplierContact" className={`form-control`} disabled={isSavingSupplier} placeholder="Supplier Contact Name" value={supplier.supplier_contact_name?.[0]?.value} onChange={(e) => { this.changeContactName(e) }} />
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="mb-3">
            <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingSupplier}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingSupplier ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingSupplier ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div>
            <div className="clearfix"></div>
          </div>

        </form>

      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSavingSupplier: state.suppliers.isSavingSupplier,
    suppliers: state.suppliers.suppliers
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getSuppliers,
    getSupplier,
    createOrUpdateSupplier,
    getSupplierByID
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(SupplierEditor));
