import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import { ipcRenderer } from 'electron';

import config from '../../../../configs/config';
import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import { getSuppliers, getSupplier, getSupplierByID, deleteSupplier } from '../../../actions/suppliers';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getUserByID } from '../../../actions/users';

const { dialog } = require('electron').remote;

class Supplier extends Component {

  state = {
    supplier: {}
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getSupplier(this.props.match.params.id, (supplier) => {
        console.log(supplier);
        this.setState(
          produce(this.state, draft => {
            draft.supplier = supplier;
          })
        );
      })
    }
  }

  componentDidUpdate = (prevProps) => {
  }

  generateDescription = (html) => {
    return {__html: html};
  }

  confirmDeleteSupplier = () => {
    const { supplier } = this.state;

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete Supplier ${supplier.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteSupplier(supplier.nid[0].value, () => {
        this.props.getSuppliers(() => {
          this.props.history.push(`${routes.SUPPLIERS}`);
        })
      })
    }
  }

  render() {

    const { supplier }  = this.state;
    const { isDeletingSupplier }  = this.props;
    let total = 0;
    let userName = '';
    if(supplier.uid) {
      let user = this.props.getUserByID(supplier.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    }

    if(supplier.nid){
      return (
        <>
          <nav className="mt-4">
            <ol className="breadcrumb">
              <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.SUPPLIERS}>Suppliers</NavLink></li>
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{supplier.title ? supplier.title[0].value : ``}</li>
            </ol>
          </nav>

          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="col-8 pl-0">
              <h1 className="h2 text-success font-weight-bold text-uppercase">
                <i className="fas fa-parachute-box mr-2"></i>
                {supplier.title ? supplier.title[0].value : ``}
              </h1>
            </div>
            <div className="text-right col-4 pr-0">
              {(() => {
                let editable = false;

                if(isAdmin() || isOwner(supplier.uid[0].target_id)) {
                  editable = true;
                }

                if(editable) {
                  return (
                    <NavLink className={`btn btn-outline-secondary btn-lg mr-2 ${isDeletingSupplier ? `disabled` : ``}`} to={`${routes.SUPPLIERS}/${supplier.nid[0].value}/edit`}>
                      <i className="fas fa-pen mr-3"></i>Edit
                    </NavLink>
                  )
                }
              })()}
              {(() => {
                let deletable = false;

                if(isAdmin() || isOwner(supplier.uid[0].target_id)) {
                  deletable = true;
                }

                if(deletable) {
                  return (
                    <button className="btn btn-outline-danger btn-lg" disabled={isDeletingSupplier} onClick={() => { this.confirmDeleteSupplier(); }}><i className="fas fa-trash mr-3"></i>
                      {isDeletingSupplier ? 'Deleting' : 'Delete'}
                    </button>
                  )
                }
              })()}
            </div>
          </div>

          <div className="card p-4 mb-4">
            <div id="item" className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(supplier.field_supplier_code.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Supplier Code</h5>
                            <div>{supplier.field_supplier_code[0].value} </div>
                          </>
                        )
                      }
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(supplier.body.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Supplier Description</h5>
                            <div dangerouslySetInnerHTML={this.generateDescription(supplier.body[0].value)} />
                          </>
                        )
                      }
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Supplier Address</h5>
                          <div>
                            {supplier.field_supplier_address_street[0]?.value}<br/>
                            {supplier.field_supplier_address_city[0]?.value}<br/>
                            {supplier.field_supplier_address_state[0]?.value}<br/>
                            {supplier.field_supplier_address_zip[0]?.value}<br/>
                            {supplier.field_supplier_address_country[0]?.value}<br/>
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Supplier Telephone</h5>
                          <div>
                            {supplier.field_supplier_telephone[0]?.value}
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Supplier Email</h5>
                          <div>
                            {supplier.field_supplier_email_address[0]?.value}
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Supplier Contact Name</h5>
                          <div>
                            {supplier.supplier_contact_name?.[0]?.value}
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

              </div>

            </div>
          </div>

        </>
      );
    }

    else {
      return (
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.SUPPLIERS}>Suppliers</NavLink></li>
            <li className="breadcrumb-item font-weight-bold text-uppercase active">{supplier.title ? supplier.title[0].value : ``}</li>
          </ol>
        </nav>
      )
    }

  }
}

const mapStateToProps = (state) => {
  return {
    isDeletingSupplier: state.suppliers.isDeletingSupplier,
    suppliers: state.suppliers.suppliers
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getSuppliers,
    getSupplier,
    getSupplierByID,
    getUserByID,
    deleteSupplier
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Supplier));
