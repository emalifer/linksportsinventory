import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router';
import routes from '../../constants/routes.json';

import SuppliersList from './SuppliersList';
import Supplier from './Supplier/Supplier';
import SupplierEditor from './Supplier/SupplierEditor';

class Suppliers extends Component {
  render() {

    const { location, match } = this.props;

    return (
      <Switch location={location}>
        <Route path={routes.SUPPLIERS} component={SuppliersList} exact />
        <Route path={`${routes.SUPPLIERS}/create`} component={SupplierEditor} exact />
        <Route path={`${routes.SUPPLIERS}/:id`} component={Supplier} exact />
        <Route path={`${routes.SUPPLIERS}/:id/edit`} component={SupplierEditor} exact />
      </Switch>
    );
  }
}

export default withRouter(Suppliers);
