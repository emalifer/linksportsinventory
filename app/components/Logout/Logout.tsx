import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as WindowActions from '../../actions/window';
import { withRouter } from 'react-router';
import { push } from 'connected-react-router';
import routes from '../../constants/routes.json';

class Logout extends Component {

  componentDidMount = () => {
    setTimeout(() => {
      this.props.logout((err) => {
        if(!err){
          this.goToLogin();
        }
      })
    }, 3000);
  }

  goToLogin = () => {
    this.props.push(routes.LOGIN);
  }

  render() {

    return (
      <div id="logout">
        <div className="logout-msg">
          <h1 className="h3 mb-3 font-weight-normal text-white">Logging out</h1>
          <p className="mt-3 mb-3 text-white">&copy; 2019-2020 Linksports Inventory</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.window.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    logout: WindowActions.logout,
    push
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Logout));
