import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Users extends Component {
  render() {

    const { users, isGettingUsers } = this.props;

    return (
      <>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-users mr-2"></i>Users</h1>
        </div>

        <h4>User List</h4>
        <div className="table-responsive">
          <table className="table table-striped table-sm">
            <thead>
              <tr>
                <th>User Name</th>
                <th>Role</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingUsers &&
                <tr>
                  <td colSpan="3">
                    <em>Loading Users</em>
                  </td>
                </tr>
              }
              {!isGettingUsers &&
                <>
                {users.length > 0 &&
                  <>
                    {users.map((user, idx) => {
                      return (
                        <tr key={user.uid[0].value}>
                          <td>{user.name[0].value}</td>
                          <td>
                            {user.roles.length > 0 &&
                              <>
                                {user.roles[0].target_id}
                              </>
                            }
                          </td>
                          <td></td>
                        </tr>
                      )
                    })}
                  </>
                }
                {users.length === 0 &&
                  <tr>
                    <td colSpan="3">
                      <em>No Users</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {users.length} User{users.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isGettingUsers: state.users.isGettingUsers,
    users: state.users.users
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users);
