import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Files extends Component {
  render() {

    const { files, isGettingFiles } = this.props;

    return (
      <>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-files mr-2"></i>Files</h1>
        </div>

        <h4>Files List</h4>
        <div className="table-responsive">
          <table className="table table-striped table-sm">
            <thead>
              <tr>
                <th>File Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingFiles &&
                <tr>
                  <td colSpan="3">
                    <em>Loading Files</em>
                  </td>
                </tr>
              }
              {!isGettingFiles &&
                <>
                {files.length > 0 &&
                  <>
                    {files.map((file, idx) => {
                      return (
                        <tr key={file.uid[0].value}>
                          <td>{file.filename[0].value}</td>
                          <td></td>
                        </tr>
                      )
                    })}
                  </>
                }
                {files.length === 0 &&
                  <tr>
                    <td colSpan="3">
                      <em>No Files</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {files.length} File{files.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isGettingFiles: state.files.isGettingFiles,
    files: state.files.files
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Files);
