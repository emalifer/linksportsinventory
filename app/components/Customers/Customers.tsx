import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router';
import routes from '../../constants/routes.json';

import CustomersList from './CustomersList';
import Customer from './Customer/Customer';
import CustomerEditor from './Customer/CustomerEditor';

class Customers extends Component {
  render() {

    const { location, match } = this.props;

    return (
      <Switch location={location}>
        <Route path={routes.CUSTOMERS} component={CustomersList} exact />
        <Route path={`${routes.CUSTOMERS}/create`} component={CustomerEditor} exact />
        <Route path={`${routes.CUSTOMERS}/:id`} component={Customer} exact />
        <Route path={`${routes.CUSTOMERS}/:id/edit`} component={CustomerEditor} exact />
      </Switch>
    );
  }
}

export default withRouter(Customers);
