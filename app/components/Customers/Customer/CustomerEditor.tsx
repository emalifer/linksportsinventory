import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import { ToastContainer, toast } from 'react-toastify';
import Barcode from 'react-barcode';
import CKEditor from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Typeahead } from 'react-bootstrap-typeahead';

import config from '../../../../configs/config';
import routes from '../../../constants/routes.json';
import {
  getCustomers,
  getCustomer,
  createOrUpdateCustomer,
  getCustomerByID
} from '../../../actions/customers';
import sample from './sample.png';

const MODE = {
  CREATE: 'create',
  UPDATE: 'update'
}

class CustomerEditor extends Component {

  constructor(props) {
    super(props);
    this.titleInput = React.createRef();
  }

  state = {
    mode: '',
    customer: {
      type: [{target_id: "customer"}],
      title: [{value: '', error: false}],
      body: [{value: ''}],
      field_customer_code: [{value: '', error: false}],
      field_customer_address_street: [{value: ''}],
      field_customer_address_cit: [{value: ''}],
      field_customer_address_state: [{value: ''}],
      field_customer_address_zip: [{value: ''}],
      field_customer_address_country: [{value: ''}],
      field_customer_telephone: [{value: ''}],
      field_customer_email: [{value: ''}],
      field_customer_contact_name: [{value: ''}]
    }
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getCustomer(this.props.match.params.id, (customer) => {
        console.log(customer);
        this.setState(
          produce(this.state, draft => {
            draft.customer = customer;

            if(customer.body.length === 0) {
              draft.customer.body = [{value: ''}];
            }

            if(customer.field_customer_code.length === 0) {
              draft.customer.field_customer_code = [{value: ''}];
            }

            if(customer.field_customer_address_street.length === 0) {
              draft.customer.field_customer_address_street = [{value: ''}];
            }

            if(customer.field_customer_address_cit.length === 0) {
              draft.customer.field_customer_address_cit = [{value: ''}];
            }

            if(customer.field_customer_address_state.length === 0) {
              draft.customer.field_customer_address_state = [{value: ''}];
            }

            if(customer.field_customer_address_zip.length === 0) {
              draft.customer.field_customer_address_zip = [{value: ''}];
            }

            if(customer.field_customer_address_country.length === 0) {
              draft.customer.field_customer_address_country = [{value: ''}];
            }

            if(customer.field_customer_telephone.length === 0) {
              draft.customer.field_customer_telephone = [{value: ''}];
            }

            if(customer.field_customer_email.length === 0) {
              draft.customer.field_customer_email = [{value: ''}];
            }

            if(customer.field_customer_contact_name?.length === 0) {
              draft.customer.field_customer_contact_name = [{value: ''}];
            }

            draft.mode = MODE.UPDATE;
          })
        );
      })
    } else {
      this.setState(
        produce(this.state, draft => {
          draft.mode = MODE.CREATE;
        })
      );
    }

  }

  componentDidUpdate = (prevProps) => {
  }

  changeTitle = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.title[0].value = e.target.value;
      draft.customer.title[0].error = false;
    }));
  }

  changeCustomerCode = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_code[0].value = e.target.value;
      draft.customer.field_customer_code[0].error = false;
    }));
  }

  changeBody = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.body[0].value = e.target.value;
      draft.customer.body[0].error = false;
    }));
  }

  changeStreet = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_address_street[0].value = e.target.value;
    }));
  }

  changeCity = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_address_cit[0].value = e.target.value;
    }));
  }

  changeState = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_address_state[0].value = e.target.value;
    }));
  }

  changeZip = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_address_zip[0].value = e.target.value;
    }));
  }

  changeCountry = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_address_country[0].value = e.target.value;
    }));
  }

  changePhone = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_telephone[0].value = e.target.value;
    }));
  }

  changeEmail = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_email[0].value = e.target.value;
    }));
  }

  changeContactName = (e) => {
    this.setState(produce(this.state, draft => {
      draft.customer.field_customer_contact_name[0].value = e.target.value;
    }));
  }

  validateCustomer = () => {
    let flag = true;
    const { customer } = this.state;

    this.setState(produce(this.state, draft => {
      if(customer.title[0].value.length === 0) {
        flag = false;
        draft.customer.title[0].error = true;
        toast.error("Please enter a Customer Name");
      }
      if(customer.field_customer_code[0].value.length === 0) {
        flag = false;
        draft.customer.field_customer_code[0].error = true;
        toast.error("Please enter a Customer Code");
      }
    }));

    return flag;
  }

  saveCustomer = (e) => {

    e.preventDefault();

    if(this.validateCustomer()) {

      let method = 'POST';

      switch(this.state.mode) {
        case MODE.CREATE:
          method = 'POST';
          break;
        case MODE.UPDATE:
          method = 'PATCH';
          break;
        default:
          break;
      }

      this.props.createOrUpdateCustomer(method, this.state.customer, (resp) => {
        this.props.getCustomers(() => {
          this.props.history.push(`${routes.CUSTOMERS}/${resp.nid[0].value}`)
        });
      })
    }

  }

  render() {

    const { isSavingCustomer } = this.props;
    const { mode, customer }  = this.state;

    return (
      <>
        <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop={false} closeOnClick rtl={false} pauseOnVisibilityChange draggable pauseOnHover />

        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.CUSTOMERS}>Customers</NavLink></li>
            {mode === MODE.CREATE &&
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{customer.title[0].value ? customer.title[0].value : `Create`}</li>
            }
            {mode === MODE.UPDATE &&
              <>
                <li className="breadcrumb-item font-weight-bold text-uppercase">
                  <NavLink className="text-success" to={`${routes.CUSTOMERS}/${customer.nid[0].value ? customer.nid[0].value : ``}`}>{customer.title[0].value ? customer.title[0].value : ``}</NavLink>
                </li>
                <li className="breadcrumb-item font-weight-bold text-uppercase active">Edit</li>
              </>
            }
          </ol>
        </nav>

        <form onSubmit={(e) => { this.saveCustomer(e); }}>
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="col-8 pl-0">
              <h1 className="h2 text-success font-weight-bold text-uppercase">
                <i className="fas fa-handshake mr-2"></i>
                {customer.title[0].value ? customer.title[0].value : `New Customer`}
              </h1>
            </div>
            {/* <div className="text-right col-4 pr-0">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingItem}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingItem ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingItem ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div> */}
          </div>

          <div className="card p-4 mb-4">
            <div className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="customerName">Customer Name</label></h5>
                      <input type="text" id="customerName" className={`form-control ${customer.title[0].error ? `is-invalid` : ``}`} disabled={isSavingCustomer} placeholder="Customer Name" value={customer.title[0].value} onChange={(e) => { this.changeTitle(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="customerCode">Customer Code</label></h5>
                      <input type="text" id="customerCode" className={`form-control ${customer.field_customer_code[0].error ? `is-invalid` : ``}`} disabled={isSavingCustomer} placeholder="Customer Code" value={customer.field_customer_code[0].value} onChange={(e) => { this.changeCustomerCode(e) }} />
                    </div>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <h5 className="card-title">Customer Description</h5>
                    <textarea className="form-control" rows="3" value={customer.body[0].value} onChange={(e) => { this.changeBody(e)} } />
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="">Customer Address </label></h5>
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingCustomer} placeholder="Street" value={customer.field_customer_address_street[0]?.value} onChange={(e) => { this.changeStreet(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingCustomer} placeholder="City" value={customer.field_customer_address_cit[0]?.value} onChange={(e) => { this.changeCity(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingCustomer} placeholder="State" value={customer.field_customer_address_state[0]?.value} onChange={(e) => { this.changeState(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingCustomer} placeholder="Zip" value={customer.field_customer_address_zip[0]?.value} onChange={(e) => { this.changeZip(e) }} />
                      <input type="text" id="" className={`form-control mb-1`} disabled={isSavingCustomer} placeholder="Country" value={customer.field_customer_address_country[0]?.value} onChange={(e) => { this.changeCountry(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="customerPhone">Customer Telephone</label></h5>
                      <input type="text" id="customerPhone" className={`form-control`} disabled={isSavingCustomer} placeholder="Customer Telephone" value={customer.field_customer_telephone[0]?.value} onChange={(e) => { this.changePhone(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="customerEmail">Customer Email</label></h5>
                      <input type="text" id="customerEmail" className={`form-control`} disabled={isSavingCustomer} placeholder="Customer Email" value={customer.field_customer_email[0]?.value} onChange={(e) => { this.changeEmail(e) }} />
                    </div>

                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="customerContact">Customer Contact Name</label></h5>
                      <input type="text" id="customerContact" className={`form-control`} disabled={isSavingCustomer} placeholder="Customer Contact Name" value={customer.field_customer_contact_name?.[0]?.value} onChange={(e) => { this.changeContactName(e) }} />
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="mb-3">
            <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingCustomer}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingCustomer ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingCustomer ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div>
            <div className="clearfix"></div>
          </div>

        </form>

      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSavingCustomer: state.customers.isSavingCustomer,
    customers: state.customers.customers
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getCustomers,
    getCustomer,
    createOrUpdateCustomer,
    getCustomerByID
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerEditor));
