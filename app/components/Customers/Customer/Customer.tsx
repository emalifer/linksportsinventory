import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import { ipcRenderer } from 'electron';

import config from '../../../../configs/config';
import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import { getCustomers, getCustomer, getCustomerByID, deleteCustomer } from '../../../actions/customers';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getUserByID } from '../../../actions/users';

const { dialog } = require('electron').remote;

class Customer extends Component {

  state = {
    customer: {}
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getCustomer(this.props.match.params.id, (customer) => {
        console.log(customer);
        this.setState(
          produce(this.state, draft => {
            draft.customer = customer;
          })
        );
      })
    }
  }

  componentDidUpdate = (prevProps) => {
  }

  generateDescription = (html) => {
    return {__html: html};
  }

  confirmDeleteCustomer = () => {
    const { customer } = this.state;

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete Customer ${customer.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteCustomer(customer.nid[0].value, () => {
        this.props.getCustomers(() => {
          this.props.history.push(`${routes.CUSTOMERS}`);
        })
      })
    }
  }

  render() {

    const { customer }  = this.state;
    const { isDeletingCustomer }  = this.props;
    let total = 0;
    let userName = '';
    if(customer.uid) {
      let user = this.props.getUserByID(customer.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    }

    if(customer.nid){
      return (
        <>
          <nav className="mt-4">
            <ol className="breadcrumb">
              <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.CUSTOMERS}>Customers</NavLink></li>
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{customer.title ? customer.title[0].value : ``}</li>
            </ol>
          </nav>

          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="col-8 pl-0">
              <h1 className="h2 text-success font-weight-bold text-uppercase">
                <i className="fas fa-handshake mr-2"></i>
                {customer.title ? customer.title[0].value : ``}
              </h1>
            </div>
            <div className="text-right col-4 pr-0">
              {(() => {
                let editable = false;

                if(isAdmin() || isOwner(customer.uid[0].target_id)) {
                  editable = true;
                }

                if(editable) {
                  return (
                    <NavLink className={`btn btn-outline-secondary btn-lg mr-2 ${isDeletingCustomer ? `disabled` : ``}`} to={`${routes.CUSTOMERS}/${customer.nid[0].value}/edit`}>
                      <i className="fas fa-pen mr-3"></i>Edit
                    </NavLink>
                  )
                }
              })()}
              {(() => {
                let deletable = false;

                if(isAdmin() || isOwner(customer.uid[0].target_id)) {
                  deletable = true;
                }

                if(deletable) {
                  return (
                    <button className="btn btn-outline-danger btn-lg" disabled={isDeletingCustomer} onClick={() => { this.confirmDeleteCustomer(); }}><i className="fas fa-trash mr-3"></i>
                      {isDeletingCustomer ? 'Deleting' : 'Delete'}
                    </button>
                  )
                }
              })()}
            </div>
          </div>

          <div className="card p-4 mb-4">
            <div id="item" className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(customer.field_customer_code.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Customer Code</h5>
                            <div>{customer.field_customer_code[0].value} </div>
                          </>
                        )
                      }
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(customer.body.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Customer Description</h5>
                            <div dangerouslySetInnerHTML={this.generateDescription(customer.body[0].value)} />
                          </>
                        )
                      }
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Customer Address</h5>
                          <div>
                            {customer.field_customer_address_street[0]?.value}<br/>
                            {customer.field_customer_address_cit[0]?.value}<br/>
                            {customer.field_customer_address_state[0]?.value}<br/>
                            {customer.field_customer_address_zip[0]?.value}<br/>
                            {customer.field_customer_address_country[0]?.value}<br/>
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Customer Telephone</h5>
                          <div>
                            {customer.field_customer_telephone[0]?.value}
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Customer Email</h5>
                          <div>
                            {customer.field_customer_email[0]?.value}
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      return (
                        <>
                          <h5 className="card-title">Customer Contact Name</h5>
                          <div>
                            {customer.field_customer_contact_name?.[0]?.value}
                          </div>
                        </>
                      )
                    })()}
                  </div>
                </div>

              </div>

            </div>
          </div>

        </>
      );
    }

    else {
      return (
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.CUSTOMERS}>Customers</NavLink></li>
            <li className="breadcrumb-item font-weight-bold text-uppercase active">{customer.title ? customer.title[0].value : ``}</li>
          </ol>
        </nav>
      )
    }

  }
}

const mapStateToProps = (state) => {
  return {
    isDeletingCustomer: state.customers.isDeletingCustomer,
    customers: state.customers.customers
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getCustomers,
    getCustomer,
    getCustomerByID,
    getUserByID,
    deleteCustomer
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Customer));
