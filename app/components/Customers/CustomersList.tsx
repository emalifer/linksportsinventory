import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import { Typeahead } from 'react-bootstrap-typeahead';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import routes from '../../constants/routes.json';
import { getCustomers, deleteCustomer } from '../../actions/customers';
import { isAdmin, isOwner } from '../../actions/auth';
import { getUserByID } from '../../actions/users';

const { dialog } = require('electron').remote;

class CustomersList extends Component {

  constructor(props) {
    super(props)
  }

  state = {
    customers: [],
    filter: {
      enabled: false,
      title: [{value: ''}]
    },
    customers: [],
    users: []
  }

  componentDidMount = () => {

    this.setState({customers: this.props.customers}, () => {
      if(this.state.filter.enabled) {
        this.filterCustomers();
      }
    });

    this.mapUsers();
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(prevProps.customers !== this.props.customers) {
      this.setState({customers: this.props.customers}, () => {
        if(this.state.filter.enabled) {
          this.filterCustomers();
        }
      });
    }
    if(prevProps.users !== this.props.users) {
      this.mapUsers()
    }

    if(prevState.filter.enabled !== this.state.filter.enabled && !this.state.filter.enabled) {
      this.clearFilter()
    }
  }

  showHideFilter = () => {
    this.setState(produce(this.state, draft => {
      draft.filter.enabled = !this.state.filter.enabled;
    }));
  }

  mapUsers = () => {
    const users = this.props.users.map((user) => {
      return {
        id: user.uid[0].value,
        label: user.name[0].value
      }
    });
    this.setState({users});
  }

  changeTitleFilter = (e) => {
    this.setState(produce(this.state, draft => {
      draft.filter.title[0].value = e.target.value;
    }));
  }


  clearFilter = () => {

    this.setState(produce(this.state, draft => {
      draft.filter.title = [{value: ''}];
    }), () => { this.filterCustomers(); });
  }

  filterCustomers = (e) => {

    if(e) {
      e.preventDefault();
    }

    const { filter } = this.state;
    let customers = this.props.customers;

    if(filter.title[0].value.length > 0) {
      customers = customers.filter((customer) => {
        return customer.title[0].value.match(filter.title[0].value)
      });
    }

    this.setState({customers});
  }

  confirmDeleteCustomer = (customer) => {

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete customer ${customer.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteCustomer(customer.nid[0].value, () => {
        this.props.getCustomers(() => {
        })
      })
    }
  }

  render() {

    const { isGettingCustomers, deletingCustomerID, isDeletingCustomer } = this.props;
    const {customers, filter } = this.state;

    return (
      <>
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase active">Customers</li>
          </ol>
        </nav>

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-handshake mr-2"></i>Customers</h1>
          <div className="float-right">
            <NavLink className="btn btn-outline-success btn-lg" to={`${routes.CUSTOMERS}/create`}><i className="fas fa-pen mr-3"></i>Create</NavLink>
          </div>
        </div>

        <h4 className="float-left">Customers List</h4>
        <ul className="nav nav-tabs float-right" style={{borderBottom: '0px'}}>
          <li className="nav-item">
            <a className={`nav-link cursor-pointer ${filter.enabled ? `active` : ``}`} onClick={() => { this.showHideFilter(); }}>
              <i className="fas fa-filter mr-2"></i>
              Filter
            </a>
          </li>
        </ul>
        <div className="clearfix"></div>

        <div className={`filter-box border-top border-left border-right p-3 ${filter.enabled ? `` : `d-none`}`}>
          <form onSubmit={(e) => { this.filterCustomers(e); }}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-4 pl-0">
                  <div className="form-group">
                    <label>Filter by Customer Name</label>
                    <input className="form-control" placeholder="Customer Name" value={filter.title[0].value} onChange={(e) => { this.changeTitleFilter(e) }} />
                  </div>
                </div>
                <div className="col-4 pr-0">
                  <div className="float-right">
                    <button onClick={() => { this.clearFilter(); }} className="btn btn-link mr-2 text-dark">Clear Filter</button>
                    <button type="submit" className="btn btn-outline-success"><i className="fas fa-filter mr-3"></i>Filter</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div className="table-responsive">
          <table className="table table-striped table-sm table-hover">
            <thead>
              <tr>
                <th>Customer</th>
                <th width="115">Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingCustomers &&
                <tr>
                  <td colSpan="2">
                    <em>Loading Customers</em>
                  </td>
                </tr>
              }
              {!isGettingCustomers &&
                <>
                {customers.length > 0 &&
                  <>
                    {customers.map((customer, idx) => {
                      return (
                        <tr key={customer.nid[0].value} className={`${deletingCustomerID === customer.nid[0].value ? `table-danger` : ``}`}>
                          <td>
                            <p style={{fontSize: '1.2rem'}} className="mb-0">
                              {customer.title[0].value}
                            </p>
                          </td>
                          <td>
                            <NavLink className={`btn btn-outline-success btn-sm mr-1 ${deletingCustomerID === customer.nid[0].value ? `disabled` : ``}`} to={`${routes.CUSTOMERS}/${customer.nid[0].value}`}>
                              <i className="fas fa-search"></i>
                            </NavLink>
                            {(() => {
                              let editable = false;

                              if(isAdmin() || isOwner(customer.uid[0].target_id)) {
                                editable = true;
                              }

                              if(editable) {
                                return (
                                  <NavLink className={`btn btn-outline-dark btn-sm mr-1 ${deletingCustomerID === customer.nid[0].value ? `disabled` : ``}`} to={`${routes.CUSTOMERS}/${customer.nid[0].value}/edit`}>
                                    <i className="fas fa-pen"></i>
                                  </NavLink>
                                )
                              }
                            })()}
                            {(() => {
                              let deletable = false;

                              if(isAdmin() || isOwner(customer.uid[0].target_id)) {
                                deletable = true;
                              }

                              if(deletable) {
                                return (
                                  <button className={`btn btn-outline-danger btn-sm ${deletingCustomerID === customer.nid[0].value ? `disabled` : ``}`} onClick={() => { this.confirmDeleteCustomer(customer) }}>
                                    <i className="fas fa-trash"></i>
                                  </button>
                                )
                              }
                            })()}
                          </td>
                        </tr>
                      )
                    })}
                  </>
                }
                {customers.length === 0 &&
                  <tr>
                    <td colSpan="2">
                      <em>No Customers</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {customers.length} Customer{customers.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customers: state.customers.customers,
    users: state.users.users,
    isGettingCustomers: state.customers.isGettingCustomers,
    deletingCustomerID: state.customers.deletingCustomerID,
    isDeletingCustomer: state.customers.isDeletingCustomer
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getCustomers,
    deleteCustomer,
    getUserByID,
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomersList);
