import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class NotFound extends Component {
  
  static contextTypes = {
    router: PropTypes.object
  }

  render() {
    return (
      <div data-tid="container">
        Not Found

        <button onClick={this.context.router.history.goBack}>Back</button>
      </div>
    );
  }
}