import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import { withRouter } from 'react-router';

import routes from '../../constants/routes.json';
import { isAdmin } from '../../actions/auth';

const { dialog, session } = require('electron').remote;

class Navigation extends Component {

  onSelect = (key) => {
    if(key === 'logout') {
      this.warningLogout();
    } else {
      this.props.history.push(key);
    }
  }

  warningLogout = (e) => {
    const resp = dialog.showMessageBoxSync(
      {
        message: `Are you sure you want to logout?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      session.defaultSession.clearStorageData();
      this.props.history.push(`${routes.LOGOUT}`);
    }
  }

  render() {
    return (
      <SideNav
        onSelect={this.onSelect}
        onToggle={this.props.onToggle}
        expanded={this.props.expanded}
        className="position-fixed bg-success"
      >
        <SideNav.Toggle />
        <SideNav.Nav defaultSelected={this.props.location.pathname}>
          <NavItem
            eventKey={routes.ADMIN}
            className="text-uppercase font-weight-bold"
          >
            <NavIcon>
              <i className="fas fa-cogs mr-2" />
            </NavIcon>
            <NavText>Dashboard</NavText>
          </NavItem>
          <NavItem eventKey={routes.PURCHASEORDERS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-inbox mr-2" />
            </NavIcon>
            <NavText>Purchase Orders</NavText>
          </NavItem>
          <NavItem eventKey={routes.DELIVERYRECEIPTS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-truck-loading mr-2" />
            </NavIcon>
            <NavText>Delivery Receipts</NavText>
          </NavItem>
          <NavItem eventKey={routes.ITEMS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fab fa-buffer mr-2" />
            </NavIcon>
            <NavText>Items</NavText>
          </NavItem>
          <NavItem eventKey={routes.SUPPLIERS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-parachute-box mr-2" />
            </NavIcon>
            <NavText>Suppliers</NavText>
          </NavItem>
          {/* <NavItem eventKey={routes.LOCATIONS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-map-marker-alt mr-2" />
            </NavIcon>
            <NavText>Locations</NavText>
          </NavItem> */}
          <NavItem eventKey={routes.CUSTOMERS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-handshake mr-2" />
            </NavIcon>
            <NavText>Customers</NavText>
          </NavItem>
          {/* <NavItem eventKey={routes.CONTACTS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-address-book mr-2" />
            </NavIcon>
            <NavText>Contacts</NavText>
          </NavItem> */}
          {/* <NavItem eventKey={routes.FILES} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-photo-video mr-2" />
            </NavIcon>
            <NavText>Files</NavText>
          </NavItem> */}
          {/* <NavItem eventKey={routes.USERS} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-users mr-2" />
            </NavIcon>
            <NavText>Users</NavText>
          </NavItem> */}
          <NavItem eventKey={'logout'} className="text-uppercase font-weight-bold">
            <NavIcon>
              <i className="fas fa-sign-out-alt mr-2" />
            </NavIcon>
            <NavText>Logout</NavText>
          </NavItem>
        </SideNav.Nav>
      </SideNav>
    );
  }
}

export default withRouter(Navigation);
