import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Contacts extends Component {
  render() {

    const { contacts, isGettingContacts } = this.props;

    return (
      <>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-address-book mr-2"></i>Contacts</h1>
        </div>

        <h4>Contact List</h4>
        <div className="table-responsive">
          <table className="table table-striped table-sm">
            <thead>
              <tr>
                <th>Contact Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingContacts &&
                <tr>
                  <td colSpan="3">
                    <em>Loading Contacts</em>
                  </td>
                </tr>
              }
              {!isGettingContacts &&
                <>
                {contacts.length > 0 &&
                  <>
                    {contacts.map((contact, idx) => {
                      return (
                        <tr key={contact.nid[0].value}>
                          <td>{contact.title[0].value}</td>
                          <td></td>
                        </tr>
                      )
                    })}
                  </>
                }
                {contacts.length === 0 &&
                  <tr>
                    <td colSpan="3">
                      <em>No Contacts</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {contacts.length} Contact{contacts.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isGettingContacts: state.contacts.isGettingContacts,
    contacts: state.contacts.contacts
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contacts);
