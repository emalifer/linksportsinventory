import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Locations extends Component {
  render() {

    const { locations, isGettingLocations } = this.props;

    return (
      <>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-map-marker-alt mr-2"></i>Locations</h1>
        </div>

        <h4>Locations List</h4>
        <div className="table-responsive">
          <table className="table table-striped table-sm">
            <thead>
              <tr>
                <th>Location Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingLocations &&
                <tr>
                  <td colSpan="3">
                    <em>Loading Locations</em>
                  </td>
                </tr>
              }
              {!isGettingLocations &&
                <>
                {locations.length > 0 &&
                  <>
                    {locations.map((location, idx) => {
                      return (
                        <tr key={location.nid[0].value}>
                          <td>{location.title[0].value}</td>
                          <td></td>
                        </tr>
                      )
                    })}
                  </>
                }
                {locations.length === 0 &&
                  <tr>
                    <td colSpan="3">
                      <em>No Locations</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {locations.length} Location{locations.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isGettingLocations: state.locations.isGettingLocations,
    locations: state.locations.locations
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Locations);
