// @flow
import React, { Component } from 'react';

import Navigation from '../Navigation/Navigation';

import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router';

import routes from '../../constants/routes.json';

import Dashboard from '../Dashboard/Dashboard';
import Items from '../Items/Items';
import PurchaseOrders from '../PurchaseOrders/PurchaseOrders';
import DeliveryReceipts from '../DeliveryReceipts/DeliveryReceipts';
import Suppliers from '../Suppliers/Suppliers';
import Locations from '../Locations/Locations';
import Customers from '../Customers/Customers';
import Contacts from '../Contacts/Contacts';
import Users from '../Users/Users';
import Files from '../Files/Files';

import * as UsersActions from '../../actions/users';
import * as CustomersActions from '../../actions/customers';
import * as ContactActions from '../../actions/contacts';
import * as ItemsActions from '../../actions/items';
import * as LocationsActions from '../../actions/locations';
import * as SuppliersActions from '../../actions/suppliers';
import * as PurchaseOrderActions from '../../actions/purchaseorders';
import * as DeliveryReceiptActions from '../../actions/deliveryreceipts';
import * as FilesActions from '../../actions/files';

import { estore } from '../../reducers/index';

import { NavLink } from 'react-router-dom';

type Props = {};

class AdminLayout extends Component<Props> {
  props: Props;

  state = {
    isFirstRun: true,
    hydrated: false,
    expanded: true
  }

  componentDidMount = () => {
    this.props.getUsers();
    this.props.getContacts();
    this.props.getCustomers();
    this.props.getItems();
    this.props.getLocations();
    this.props.getSuppliers();
    this.props.getPurchaseOrders();
    this.props.getDeliveryReceipts();
    this.props.getFiles();
  }

  componentDidUpdate = (prevProps) => {

    const { isGettingUsers, isGettingContacts, isGettingCustomers, isGettingItems, isGettingLocations, isGettingsSuppliers, isGettingPurchaseOrders, isGettingDeliveryReceipts, isGettingFiles } = this.props;

    if(prevProps.isGettingUsers !== isGettingUsers) {
      this.isHydrated();
    }
    if(prevProps.isGettingContacts !== isGettingContacts) {
      this.isHydrated();
    }
    if(prevProps.isGettingCustomers !== isGettingCustomers) {
      this.isHydrated();
    }
    if(prevProps.isGettingItems !== isGettingItems) {
      this.isHydrated();
    }
    if(prevProps.isGettingLocations !== isGettingLocations) {
      this.isHydrated();
    }
    if(prevProps.isGettingsSuppliers !== isGettingsSuppliers) {
      this.isHydrated();
    }
    if(prevProps.isGettingPurchaseOrders !== isGettingPurchaseOrders) {
      this.isHydrated();
    }
    if(prevProps.isGettingDeliveryReceipts !== isGettingDeliveryReceipts) {
      this.isHydrated();
    }
    if(prevProps.isGettingFiles !== isGettingFiles) {
      this.isHydrated();
    }

  }

  isHydrated = () => {

    const { isGettingUsers, isGettingContacts, isGettingCustomers, isGettingItems, isGettingLocations, isGettingsSuppliers, isGettingPurchaseOrders, isGettingDeliveryReceipts, isGettingFiles } = this.props;
    const { hydrated, isFirstRun } = this.state;

    if( !isGettingUsers && !isGettingContacts && !isGettingCustomers && !isGettingItems && !isGettingLocations && !isGettingsSuppliers && !isGettingPurchaseOrders && !isGettingDeliveryReceipts && !isGettingFiles && isFirstRun) {
      this.setState({isFirstRun: false, hydrated: true});
    }
  }

  sideBarOnToggle = () => {
    this.setState({expanded: !this.state.expanded})
  }

  render() {

    if(!this.props.isLoggedIn) {
      return <Redirect to={routes.LOGIN} />
    }

    const { location, match } = this.props;
    const { hydrated, expanded } = this.state;

    const userName = estore.get('name');

    return (
      <div>

        <div className="">
          <div className="">

            <Navigation onToggle={this.sideBarOnToggle} expanded={expanded} />

            {hydrated &&
              <main role="main" className={`${expanded ? `expanded` : ``} pl-4 pr-4`}>
                <Switch location={location}>
                  <Route path={routes.ADMIN} exact component={Dashboard} />
                  <Route path={routes.ITEMS} component={Items} />
                  <Route path={routes.PURCHASEORDERS} component={PurchaseOrders} />
                  <Route path={routes.DELIVERYRECEIPTS} component={DeliveryReceipts} />
                  <Route path={routes.SUPPLIERS} component={Suppliers} />
                  <Route path={routes.LOCATIONS} component={Locations} />
                  <Route path={routes.CUSTOMERS} component={Customers} />
                  <Route path={routes.CONTACTS} component={Contacts} />
                  <Route path={routes.USERS} component={Users} />
                  <Route path={routes.FILES} component={Files} />
                </Switch>
              </main>
            }

          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.window.isLoggedIn,
    isGettingUsers: state.users.isGettingUsers,
    isGettingCustomers: state.customers.isGettingCustomers,
    isGettingContacts: state.contacts.isGettingContacts,
    isGettingItems: state.items.isGettingItems,
    isGettingLocations: state.locations.isGettingLocations,
    isGettingsSuppliers: state.suppliers.isGettingsSuppliers,
    isGettingPurchaseOrders: state.purchaseorders.isGettingPurchaseOrders,
    isGettingDeliveryReceipts: state.deliveryreceipts.isGettingDeliveryReceipts,
    isGettingFiles: state.files.isGettingFiles
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getUsers: UsersActions.getUsers,
    getCustomers: CustomersActions.getCustomers,
    getContacts: ContactActions.getContacts,
    getItems: ItemsActions.getItems,
    getLocations: LocationsActions.getLocations,
    getSuppliers: SuppliersActions.getSuppliers,
    getPurchaseOrders: PurchaseOrderActions.getPurchaseOrders,
    getDeliveryReceipts: DeliveryReceiptActions.getDeliveryReceipts,
    getFiles: FilesActions.getFiles
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminLayout));
