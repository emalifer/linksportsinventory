import React, { Component } from 'react';

class Dashboard extends Component {
  render() {
    return (
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-cogs mr-2"></i>Dashboard</h1>
      </div>
    );
  }
}

export default Dashboard;
