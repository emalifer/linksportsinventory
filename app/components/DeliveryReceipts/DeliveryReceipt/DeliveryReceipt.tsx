import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import { ipcRenderer } from 'electron';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import { getDeliveryReceipts, getDeliveryReceipt, deleteDeliveryReceipt, getDeliveryReceiptBadgeStatus, STATUS as DELIVERYRECEIPTSTATUS } from '../../../actions/deliveryreceipts';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getSupplierByID } from '../../../actions/suppliers';
import { getContactByID } from '../../../actions/contacts';
import { getLocationByID } from '../../../actions/locations';
import { getCustomerByID } from '../../../actions/customers';
import { getItemByID } from '../../../actions/items';
import { getUserByID } from '../../../actions/users';

const { dialog } = require('electron').remote;

class DeliveryReceipt extends Component {

  state = {
    deliveryReceipt: {},
    printModal: false,
    verticalSpace: '0',
    pages: [
      { text: 'Page 1 of 1', left: '2', top: '92' },
      { text: 'www.linksportsinc.com | jeugenio@linksportsinc.com', left: '42', top: '92' }
    ]
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getDeliveryReceipt(this.props.match.params.id, (deliveryReceipt) => {
        this.setState(
          produce(this.state, draft => {
            draft.deliveryReceipt = deliveryReceipt;
            if(deliveryReceipt.field_purchase_order_terms.length === 0) {
              draft.deliveryReceipt.field_purchase_order_terms = [{value: ''}]
            }
            if(deliveryReceipt.field_purchase_order_shipping.length === 0) {
              draft.deliveryReceipt.field_purchase_order_shipping = [{value: ''}]
            }
            if(deliveryReceipt.field_purchase_order_delivery.length === 0) {
              draft.deliveryReceipt.field_purchase_order_delivery = [{value: ''}]
            }
          })
        );
      })
    }
  }

  addPage = (e) => {
    e.preventDefault();
    this.setState(produce(this.state, draftState => {
      draftState.pages.push({
        text: e.target.elements.text.value,
        left: e.target.elements.left.value,
        top: e.target.elements.top.value
      })
    }))
  }

  removePage = (i) => {
    this.setState(produce(this.state, draftState => {
      draftState.pages.splice(parseInt(i, 10), 1);
    }))
  }

  sendToPrinter = () => {
    ipcRenderer.send("printPDF", `DR-${this.state.deliveryReceipt.title[0].value}`, document.getElementById('deliveryreceipt').innerHTML);
  }

  confirmDeleteDeliveryReceipt = () => {
    const { deliveryReceipt } = this.state;

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete Delivery Receipt Number ${deliveryReceipt.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteDeliveryReceipt(deliveryReceipt.nid[0].value, () => {
        this.props.getDeliveryReceipts(() => {
          this.props.history.push(`${routes.DELIVERYRECEIPTS}`);
        })
      })
    }

    // dialog.showMessageBox(
    //   {
    //     message: `Do you want to delete Delivery Receipt Number ${deliveryReceipt.title[0].value}?`,
    //     buttons: ['Yes','No']
    //   },
    //   (response) => {
    //     if(response === 0) {
    //       this.props.deleteDeliveryReceipt(deliveryReceipt.nid[0].value, () => {
    //         this.props.getDeliveryReceipts(() => {
    //           this.props.history.push(`${routes.DELIVERYRECEIPTS}`);
    //         })
    //       })
    //     }
    //   }
    // );
  }

  render() {

    const { deliveryReceipt, printModal, verticalSpace, pages }  = this.state;
    const { isDeletingDeliveryReceipt }  = this.props;
    let total = 0;
    let userName = '';
    if(deliveryReceipt.uid) {
      let user = this.props.getUserByID(deliveryReceipt.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    }

    console.log(deliveryReceipt)

    if(deliveryReceipt.nid){
      return (
        <>
          <nav className="mt-4">
            <ol className="breadcrumb">
              <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.DELIVERYRECEIPTS}>Delivery Receipts</NavLink></li>
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{deliveryReceipt.title ? deliveryReceipt.title[0].value : ``}</li>
            </ol>
          </nav>

          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 className="h2 text-success font-weight-bold text-uppercase">
              <i className="fas fa-truck-loading mr-2"></i>
              {deliveryReceipt.title ? deliveryReceipt.title[0].value : ``}
              <span className={`badge ${getDeliveryReceiptBadgeStatus(deliveryReceipt.field_purchase_order_status[0].value)} ml-2`}>{deliveryReceipt.field_purchase_order_status[0].value}</span>
            </h1>
            <div className="float-right">
              <button className="btn btn-outline-success btn-lg mr-2" onClick={() => { this.setState({printModal: !printModal})}}>Print PDF Settings</button>
              <button className="btn btn-outline-success btn-lg mr-2" onClick={() => { this.sendToPrinter(); }}><i className="fas fa-print mr-3"></i>Print</button>
              {(() => {
                let editable = false;

                if(isAdmin() || isOwner(deliveryReceipt.uid[0].target_id)) {
                  editable = true;
                }
                if(deliveryReceipt.field_purchase_order_status[0].value === DELIVERYRECEIPTSTATUS.APPROVED || deliveryReceipt.field_purchase_order_status[0].value === DELIVERYRECEIPTSTATUS.CANCELLED) {
                  editable = false;
                }

                if(editable) {
                  return (
                    <NavLink className={`btn btn-outline-secondary btn-lg mr-2 ${isDeletingDeliveryReceipt ? `disabled` : ``}`} to={`${routes.DELIVERYRECEIPTS}/${deliveryReceipt.nid[0].value}/edit`}>
                      <i className="fas fa-pen mr-3"></i>Edit
                    </NavLink>
                  )
                }
              })()}
              {(() => {
                let deletable = false;

                if(isAdmin() || isOwner(deliveryReceipt.uid[0].target_id)) {
                  deletable = true;
                }
                if(deliveryReceipt.field_purchase_order_status[0].value === DELIVERYRECEIPTSTATUS.APPROVED || deliveryReceipt.field_purchase_order_status[0].value === DELIVERYRECEIPTSTATUS.CANCELLED) {
                  deletable = false;
                }

                if(deletable) {
                  return (
                    <button className="btn btn-outline-danger btn-lg" disabled={isDeletingDeliveryReceipt} onClick={() => { this.confirmDeleteDeliveryReceipt(); }}><i className="fas fa-trash mr-3"></i>
                      {isDeletingDeliveryReceipt ? 'Deleting' : 'Delete'}
                    </button>
                  )
                }
              })()}
            </div>
          </div>

          <div className="card mb-4">
            <div id="deliveryreceipt" className="card-body">

              {pages.map((page, i) => (
                <div className="position-absolute d-none d-print-block" style={{ left: `${page.left}rem`, top: `${page.top}rem`, zIndex: 1000 }} key={`pagenumber-${i}`}>
                  {page.text}
                </div>
              ))}

              <div className="container-fluid mb-4">

                <div className="row mb-3">
                  <div className="col"><img src={logo} width="400" /></div>
                  <div className="col">
                    <p className="text-right font-italic font-weight-bold">"Your sports solutions partner"</p>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <p>
                      105 New Society St., Rosario Subd.-4<br/>
                      Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                      Philippines<br/><br/>
                      Tel # +63 2 75007559<br/>
                      Fax # +63 2 77486641<br/><br/>
                      Email: info@linksportsinc.com<br/>
                    </p>
                  </div>
                  <div className="col">
                    <table className="table table-bordered table-sm mb-0">
                      <thead>
                        <tr><th colSpan="2"><h3 className="mb-0 text-uppercase font-weight-bold">Delivery Receipt</h3></th></tr>
                      </thead>
                      <tbody>
                        <tr><th>Delivery Receipt Number</th><td>{deliveryReceipt.title[0].value}</td></tr>
                        <tr><th>Date</th><td>{deliveryReceipt.field_purchase_order_date ? moment(deliveryReceipt.field_purchase_order_date[0].value).format(`MMMM DD, YYYY`) : ``}</td></tr>
                        <tr><th>PO</th><td>{deliveryReceipt.field_purchase_order[0]?.value}</td></tr>
                        {/* <tr><th>Terms</th><td>{deliveryReceipt.field_purchase_order_terms[0].value}</td></tr> */}
                        <tr><th>Shipment</th><td>{deliveryReceipt.field_purchase_order_shipping[0].value}</td></tr>
                        <tr><th  width="50%">Delivery</th><td>{deliveryReceipt.field_purchase_order_delivery[0].value}</td></tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div className="row mb-3">

                  <div className="col">
                    {/* Location */}
                    {/* {deliveryReceipt.field_purchase_order_ship_to &&
                      <>
                      {deliveryReceipt.field_purchase_order_ship_to.length > 0 &&
                        <>
                          {deliveryReceipt.field_purchase_order_ship_to.map((locationItem) => {
                            const location = this.props.getLocationByID(locationItem.target_id);
                            return (
                              <React.Fragment key={locationItem.target_id}>
                                {location &&
                                  <div className="card" style={{minHeight: '180px'}}>
                                    <div className="card-body">
                                      <p  className="mb-0">Ship To:</p>
                                      <h5>{location.title[0].value}</h5>
                                      <p dangerouslySetInnerHTML={{__html: location.body[0].processed}}></p>
                                      {location.field_location_contact.length > 0 &&
                                          <>
                                            {location.field_location_contact.map((contactItem) => {
                                              const contact = this.props.getContactByID(contactItem.target_id);
                                              return (
                                                <React.Fragment key={contactItem.target_id}>
                                                  <p  className="mb-0">Contact Name:</p>
                                                  <h5 className="mb-0">{contact.title[0].value}</h5>
                                                  <p className="mb-0">{contact.field_contact_mobile[0].value}</p>
                                                  <p className="mb-0">{contact.field_contact_telephone[0].value}</p>
                                                </React.Fragment>
                                              )
                                            })}
                                          </>
                                        }
                                      <p  className="mt-3">Prepared By: {userName} {isOwner(deliveryReceipt.uid[0].target_id) ? `(You)` : ``}</p>
                                    </div>
                                  </div>
                                }
                              </React.Fragment>
                            )
                          })}
                        </>
                      }
                      </>
                    } */}
                    {/* <div className="card" style={{minHeight: '180px'}}>
                      <div className="card-body">
                        <p className="mb-0 font-weight-bold">Ship To:</p>
                        <h5 className="text-uppercase"><strong>Linksports, Inc.</strong></h5>
                        <p>
                          105 New Society St., Rosario Subd.-4<br/>
                          Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                          Philippines<br/><br/>

                          Contact Name:	<strong>Jojo Eugenio</strong><br/>
                          +63917 5207606<br/>
                          jojo@linksportsinc.com
                        </p>
                      </div>
                    </div> */}
                    {/* Customer */}
                    <div className="card" style={{minHeight: '180px'}}>
                      <div className="card-body">
                        {deliveryReceipt.field_purchase_order_ship_to &&
                          <>
                          {deliveryReceipt.field_purchase_order_ship_to.length > 0 &&
                            <>
                              {deliveryReceipt.field_purchase_order_ship_to.map((locationItem) => {
                                const customer = this.props.getCustomerByID(locationItem.target_id);
                                return (
                                  <React.Fragment key={locationItem.target_id}>
                                    {customer &&
                                      <>
                                        <p  className="mb-0">Ship To:</p>
                                        <h5 className="text-uppercase"><strong>{customer.title[0].value}</strong></h5>
                                        <p>
                                          {customer.field_customer_address_street[0]?.value},&nbsp;
                                          {customer.field_customer_address_cit[0]?.value}<br/>
                                          {customer.field_customer_address_state[0]?.value},&nbsp;
                                          {customer.field_customer_address_zip[0]?.value},&nbsp;
                                          {customer.field_customer_address_country[0]?.value},&nbsp;
                                        </p>
                                        <p>
                                          {customer.field_customer_contact_name[0]?.value}<br/>
                                          {customer.field_customer_email[0]?.value}<br/>
                                          {customer.field_customer_telephone[0]?.value}<br/>
                                        </p>
                                        {/* {location.field_location_contact.length > 0 &&
                                          <>
                                            {location.field_location_contact.map((contactItem) => {
                                              const contact = this.props.getContactByID(contactItem.target_id);
                                              return (
                                                <React.Fragment key={contactItem.target_id}>
                                                  <p className="mb-0">Contact Name:</p>
                                                  <h5 className="mb-0">{contact.title[0].value}</h5>
                                                  <p className="mb-0">{contact.field_contact_mobile[0].value}</p>
                                                  <p className="mb-0">{contact.field_contact_telephone[0].value}</p>
                                                </React.Fragment>
                                              )
                                            })}
                                          </>
                                        } */}
                                      </>
                                    }
                                  </React.Fragment>
                                )
                              })}
                            </>
                          }
                          {deliveryReceipt.field_purchase_order_ship_to.length === 0 &&
                            <>
                              <p  className="mb-0">Ship To:</p>
                              <h5 className="text-uppercase"><strong>No Customer</strong></h5>
                            </>
                          }
                          </>
                        }
                      </div>
                    </div>
                  </div>
                </div>

                {/* ITEMS */}
                <div className="row">
                  <div className="col">
                    <table className="table table-smd table-striped table-hover border-1">
                      <thead className="thead-dark">
                        <tr>
                          <th className="text-center text-uppercase font-weight-bold">Item</th>
                          <th className="text-uppercase font-weight-bold">Qty</th>
                          <th width="65%" className="text-uppercase font-weight-bold">Description</th>
                          {/* <th className="text-right">Unit Price</th>
                          <th className="text-right">Amount</th> */}
                        </tr>
                      </thead>
                      {deliveryReceipt.field_purchase_order_items &&
                        <>
                        {deliveryReceipt.field_purchase_order_items.length > 0 &&
                          <tbody>
                            {deliveryReceipt.field_purchase_order_items.map((itemItem, id) => {
                              const item = this.props.getItemByID(itemItem.target_id);
                              const supplier = this.props.getSupplierByID(item?.field_item_supplier[0].target_id);
                              if(item){

                                const itemTotal = parseFloat(parseFloat(item.field_unit_price[0].value) * parseFloat(itemItem.quantity)).toFixed(2);
                                total += parseFloat(itemTotal);

                                return (
                                  <React.Fragment key={itemItem.target_id}>
                                    <tr>
                                      <td className="text-center">{id + 1}</td>
                                      <td>{itemItem.quantity}</td>
                                      <td>
                                        <p className="mb-0"><strong>{item.title[0].value}</strong></p>
                                        {item.field_i[0].value &&
                                          <>
                                            {item.field_item.length > 0 &&
                                              <>
                                                {item.field_item.map((subitemItem, subitemid) => {
                                                  const subItem = this.props.getItemByID(subitemItem.target_id);
                                                  if(subItem) {
                                                    return <p key={`subItem-${subItem.target_id}`} className="mb-0" style={{fontSize: '.8rem'}}>{subitemItem.quantity}x {subItem.title[0].value}</p>
                                                  }
                                                })}
                                              </>
                                            }
                                          </>
                                        }
                                      </td>
                                      {/* <td className="text-right">{numeral(item.field_unit_price[0].value).format('0,0.00')}</td>
                                      <td className="text-right">{numeral(itemTotal).format('0,0.00')}</td> */}
                                    </tr>
                                  </React.Fragment>
                                )
                              }
                            })}
                            <tr className="border-bottom">
                              <td colSpan={6} className="text-center">
                                <strong>---------- NOTHING FOLLOWS ----------</strong>
                              </td>
                            </tr>
                          </tbody>
                        }
                        </>
                      }
                    </table>
                  </div>
                </div>

                {/* <div style={{ pageBreakAfter: 'always' }} /> */}

                <div className="d-none d-print-block" style={{ height: `${verticalSpace}rem` }}> {/* Hack for bringin down */}
                </div>

                <div className="row">
                  <div className="col">
                    <table className="table table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="border-top-0">
                            {deliveryReceipt.field_purchase_order_status[0].value === DELIVERYRECEIPTSTATUS.APPROVED &&
                              <>
                                <p>
                                  <strong>Remarks</strong><br/><br/><br/>
                                  <em>Electronically approved, signature not required</em><br/><br/>
                                  <strong>Approved By:</strong><br/>
                                  JLE
                                </p>
                                <p>
                                  The undersigned individual hereby acknowledges the receipt and the delivered goods detailed on the included list/invoice. Furthermore, this letter acknowledges that delivered goods underwent proper inspection and without any defect.
                                </p>
                                <div className="row">
                                  <div className="col">
                                    Received by<br/><br/>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col">
                                    <hr />
                                    <strong>Name</strong>
                                  </div>
                                  <div className="col">
                                    <hr />
                                    <strong>Signature</strong>
                                  </div>
                                  <div className="col">
                                    <hr />
                                    <strong>Date</strong>
                                  </div>
                                </div>
                              </>
                            }

                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <div className="row">
                  {/* <div className="col">
                    <table className="table table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="text-right bg-dark text-white">Subtotal</td>
                          <td className="text-right"  width="50%">{numeral(parseFloat(total)).format('0,0.00')}</td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">VAT</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_order_vat[0].value)).format('0,0.00')}</td>
                        </tr> */}
                        {/* <tr>
                          <td className="text-right bg-dark text-white">Ex-Works Value - Sub-Total</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_ex_works[0].value)).format('0,0.00')}</td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Certificate of Conformity</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_coc[0].value)).format('0,0.00')}</td>
                          {}
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Handling Pack SOLAS</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_hps[0].value)).format('0,0.00')}</td>
                          {}
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Photo/HT Certificate</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_phc[0].value)).format('0,0.00')}</td>
                          {}
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Freight</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_freight[0].value)).format('0,0.00')}</td>
                          {}
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Bank and or Courier Cost</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_bcc[0].value)).format('0,0.00')}</td>
                          {}
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Documentation Fee</td>
                          <td className="text-right">{numeral(parseFloat(deliveryReceipt.field_purchase_doc[0].value)).format('0,0.00')}</td>
                          {}
                        </tr> */}
                        {(() => {
                          // total = total + parseFloat(deliveryReceipt.field_purchase_order_vat[0].value);

                          // total = total + parseFloat(deliveryReceipt.field_purchase_ex_works[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_coc[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_hps[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_phc[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_freight[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_bcc[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_doc[0].value);
                        })()}
                        {/* <tr>
                          <td className="text-right bg-dark text-white pt-3 pb-3"><h5 className="font-weight-bold">TOTAL</h5></td>
                          <td className="text-right pt-3 pb-3"><h5>Php {numeral(parseFloat(total)).format('0,0.00')}</h5></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div> */}
                </div>

                <div className="row mt-4">
                  <div className="col text-right">
                    {/* <span className="d-inline-block mr-3 text-success">www.linksportsinc.com</span> | <span className="d-inline-block ml-3">jeugenio@linksportsinc.com</span> */}
                  </div>
                </div>

              </div>

            </div>

            <Modal isOpen={printModal} size={'lg'} toggle={() => { this.setState({printModal: !printModal})}}>
              <ModalHeader toggle={() => { this.setState({printModal: !printModal})}}>Print Settings</ModalHeader>
              <ModalBody>
                <h5 className="mb-3">
                  These settings are only viewed on printing PDF
                </h5>
                <div className="form-group">
                  <label htmlFor="verticalSpace">Vertical Space</label>
                  <input id="verticalSpace" type="number" className="form-control form-control-lg" value={verticalSpace} placeholder="Vertical Space" min="0" onChange={(e) => { this.setState({verticalSpace: e.target.value}) }}></input>
                </div>
                <div className="form-group">
                  <label>Manual Pages</label>
                  <div className="row">
                    <div className="col col-4 font-weight-bold">Text</div>
                    <div className="col col-3 font-weight-bold">Left</div>
                    <div className="col col-3 font-weight-bold">Top</div>
                    <div className="col col-2 font-weight-bold">Remove</div>
                  </div>
                  {pages.map((page, i) => (
                    <div className="row mb-1" key={`page-${i}`}>
                      <div className="col col-4">{page.text}</div>
                      <div className="col col-3">{page.left}</div>
                      <div className="col col-3">{page.top}</div>
                      <div className="col col-2"><button type="button" className="btn btn-sm btn-danger text-center" style={{ width: '30px' }} onClick={(e) => { this.removePage(i) }}><i className="fa fa-times" /></button></div>
                    </div>
                  ))}
                  <hr/>
                  <form onSubmit={(e) => { this.addPage(e); }}>
                    <div className="row">
                      <div className="col col-4">
                        <input id="text" type="text" className="form-control form-control-sm" placeholder="Please enter paging text" defaultValue="Page 1 of 1"></input>
                      </div>
                      <div className="col col-3">
                        <input id="left" type="number" min="0" className="form-control form-control-sm" placeholder="Please enter left position" defaultValue="0"></input>
                      </div>
                      <div className="col col-3">
                        <input id="top" type="number" min="0" className="form-control form-control-sm" placeholder="Please enter right position" defaultValue="0"></input>
                      </div>
                      <div className="col col-2">
                        <button type="submit" className="btn btn-sm btn-outline-success text-center" style={{ width: '30px' }}><i className="fa fa-plus" /></button>
                      </div>
                    </div>
                  </form>
                </div>
              </ModalBody>
              <ModalFooter>
                <Button color="success" size={'lg'} onClick={() => { this.setState({printModal: !printModal})}}>Okay</Button>
              </ModalFooter>
            </Modal>

          </div>

        </>
      );
    }

    else {
      return (
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.DELIVERYRECEIPTS}>Delivery Receipts</NavLink></li>
            <li className="breadcrumb-item font-weight-bold text-uppercase active">{deliveryReceipt.title ? deliveryReceipt.title[0].value : ``}</li>
          </ol>
        </nav>
      )
    }

  }
}

const mapStateToProps = (state) => {
  return {
    isDeletingDeliveryReceipt: state.deliveryreceipts.isDeletingDeliveryReceipt
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getDeliveryReceipts,
    getDeliveryReceipt,
    getSupplierByID,
    getContactByID,
    getLocationByID,
    getCustomerByID,
    getItemByID,
    getUserByID,
    deleteDeliveryReceipt
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(DeliveryReceipt));
