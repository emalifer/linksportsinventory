import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import { Typeahead } from 'react-bootstrap-typeahead';
import DatePicker from 'react-datepicker';
import { ToastContainer, toast } from 'react-toastify';

import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import terms from '../../../constants/terms.json';
import shipments from '../../../constants/shipment.json';
import deliveries from '../../../constants/deliveryDR.json';
import { getDeliveryReceipts, getDeliveryReceipt, createOrUpdateDeliveryReceipt, getNextDeliveryReceiptNumber, getDeliveryReceiptBadgeStatus, STATUS as DELIVERYRECEIPTSTATUS } from '../../../actions/deliveryreceipts';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getSupplierByID } from '../../../actions/suppliers';
import { getContactByID } from '../../../actions/contacts';
import { getLocationByID } from '../../../actions/locations';
import { getCustomerByID } from '../../../actions/customers';
import { getItemByID } from '../../../actions/items';
import { getUserByID } from '../../../actions/users';
import { estore } from '../../../reducers/index';

const MODE = {
  CREATE: 'create',
  UPDATE: 'update'
}

class DeliveryReceiptEditor extends Component {

  constructor (props) {
    super(props);
    this.titleInput = React.createRef();
    this.supplierInput = React.createRef();
    this.locationInput = React.createRef();
    this.itemsTypeahead = React.createRef();
  }

  state = {
    mode: '',
    deliveryReceipt: {
      type: [{target_id: "delivery_receipt"}],
      title: [{value: '', error: false}],
      field_purchase_order_date: [{value: new Date(), error: false}],
      field_purchase_order_terms: [{value: terms["30DAYS"]}],
      field_purchase_order: [{value: 'PO '}],
      field_purchase_order_shipping: [{value: shipments["30DAYS"]}],
      field_purchase_order_delivery: [{value: deliveries.INLAND}],
      field_purchase_order_ship_to: [{target_id: 0, error: false}],
      field_purchase_order_items: [],
      field_purchase_order_vat: [{value: 0}],
      field_purchase_order_status: [{value: DELIVERYRECEIPTSTATUS.PENDING}],
      field_purchase_ex_works: [{value: 0}],
      field_purchase_coc: [{value: 0}],
      field_purchase_hps: [{value: 0}],
      field_purchase_phc: [{value: 0}],
      field_purchase_freight: [{value: 0}],
      field_purchase_bcc: [{value: 0}],
      field_purchase_doc: [{value: 0}],
    },
    tempTarget: [],
    tempQuantity: 0,
    tempItemError: false,
    suppliers: [],
    customers: [],
    locations: [],
    items: []
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getDeliveryReceipt(this.props.match.params.id, (deliveryReceipt) => {
        this.setState(
          produce(this.state, draft => {
            console.log(deliveryReceipt)
            draft.deliveryReceipt = deliveryReceipt;
            if(deliveryReceipt.field_purchase_order_terms.length === 0) {
              draft.deliveryReceipt.field_purchase_order_terms = [{value: ''}]
            }
            if(deliveryReceipt.field_purchase_order_shipping.length === 0) {
              draft.deliveryReceipt.field_purchase_order_shipping = [{value: ''}]
            }
            if(deliveryReceipt.field_purchase_order_delivery.length === 0) {
              draft.deliveryReceipt.field_purchase_order_delivery = [{value: ''}]
            }
            draft.deliveryReceipt.field_purchase_order_date[0].value = moment(deliveryReceipt.field_purchase_order_date[0].value).toDate()
            draft.mode = MODE.UPDATE;
          })
        );
      })
    } else {
      this.props.getNextDeliveryReceiptNumber((nextDeliveryReceiptNumber) => {
        this.setState(produce(this.state, draft => {
          draft.deliveryReceipt.title[0].value = nextDeliveryReceiptNumber;
          draft.mode = MODE.CREATE;
        }))
      });
    }

    this.mapSuppliers();
    this.mapCustomers();
    this.mapLocations();
    this.mapItems();

  }

  componentDidUpdate = (prevProps) => {
    if(prevProps.suppliers !== this.props.suppliers) {
      this.mapSuppliers()
    }
    if(prevProps.locations !== this.props.locations) {
      this.mapLocations()
    }
    if(prevProps.items !== this.props.items) {
      this.mapItems()
    }
  }

  mapSuppliers = () => {
    const suppliers = this.props.suppliers.map((supplier) => {
      return {
        id: supplier.nid[0].value,
        label: supplier.title[0].value
      }
    });
    this.setState({suppliers});
  }

  mapLocations = () => {
    const locations = this.props.locations.map((location) => {
      return {
        id: location.nid[0].value,
        label: location.title[0].value
      }
    });
    this.setState({locations});
  }

  mapCustomers = () => {
    const customers = this.props.customers.map((customer) => {
      return {
        id: customer.nid[0].value,
        label: customer.title[0].value
      }
    });
    this.setState({customers});
  }

  mapItems = () => {
    const items = this.props.items.map((item) => {
      return {
        id: item.nid[0].value,
        label: item.title[0].value,
        field_unit_price: item.field_unit_price[0].value
      }
    });
    this.setState({items});
  }

  changeTitle = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.title[0].value = e.target.value;
      draft.deliveryReceipt.title[0].error = false;
    }));
  }

  changeDate = (date) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_date[0].value = date,
      draft.deliveryReceipt.field_purchase_order_date[0].error = false;
    }));
  }

  changeTerms = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_terms[0].value = e.target.value
    }));
  }

  changePO = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order[0].value = e.target.value;
    }));
  }

  changeShipping = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_shipping[0].value = e.target.value
    }));
  }

  changeDelivery = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_delivery[0].value = e.target.value
    }));
  }

  changeSupplier = (suppliers) => {
    if(suppliers.length > 0){
      this.setState(produce(this.state, draft => {
        draft.deliveryReceipt.field_purchase_order_supplier[0] = {};
        draft.deliveryReceipt.field_purchase_order_supplier[0].target_id = suppliers[0].id;
        draft.deliveryReceipt.field_purchase_order_supplier[0].error = false;
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.deliveryReceipt.field_purchase_order_supplier[0] = {};
        draft.deliveryReceipt.field_purchase_order_supplier[0].target_id = 0
      }));
    }
  }

  // changeLocation = (locations) => {
  //   if(locations.length > 0){
  //     this.setState(produce(this.state, draft => {
  //       draft.deliveryReceipt.field_purchase_order_ship_to[0] = {};
  //       draft.deliveryReceipt.field_purchase_order_ship_to[0].target_id = locations[0].id;
  //       draft.deliveryReceipt.field_purchase_order_ship_to[0].error = false;
  //     }));
  //   } else {
  //     this.setState(produce(this.state, draft => {
  //       draft.deliveryReceipt.field_purchase_order_ship_to[0] = {};
  //       draft.deliveryReceipt.field_purchase_order_ship_to[0].target_id = 0
  //     }));
  //   }
  // }

  changeCustomer = (customers) => {
    if(customers.length > 0){
      this.setState(produce(this.state, draft => {
        draft.deliveryReceipt.field_purchase_order_ship_to[0] = {};
        draft.deliveryReceipt.field_purchase_order_ship_to[0].target_id = customers[0].id;
        draft.deliveryReceipt.field_purchase_order_ship_to[0].error = false;
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.deliveryReceipt.field_purchase_order_ship_to[0] = {};
        draft.deliveryReceipt.field_purchase_order_ship_to[0].target_id = 0
      }));
    }
  }

  changeTempQuantity = (e) => {
    this.setState(produce(this.state, draft => {
      draft.tempQuantity = e.target.value
    }));
  }

  changeTemp = (items) => {
    if(items.length > 0){
      this.setState(produce(this.state, draft => {
        draft.tempTarget = items
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.tempTarget = []
      }));
    }
  }

  addItem = () => {
    if(this.state.tempQuantity > 0 && this.state.tempTarget.length > 0) {
      this.setState(produce(this.state, draft => {
        draft.deliveryReceipt.field_purchase_order_items.push(
          {
            target_id: draft.tempTarget[0].id,
            quantity: numeral(draft.tempQuantity).format('0')
          }
        );
        draft.tempQuantity = 0;
        draft.tempTarget = [];
        draft.tempItemError = false;
      }));
      this.itemsTypeahead.getInstance().clear();
    }
  }

  removeItem = (id) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_items.splice(id, 1);
    }));
  }

  changeVAT = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_vat[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeShippingCost = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_shipping_co[0].value = e.target.value ? e.target.value : 0
    }));
  }

  changeInsurance = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_insurance[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeEW = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_ex_works[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeCOC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_coc[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeHPS = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_hps[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changePHC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_phc[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeFreight = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_freight[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeBCC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_bcc[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeDOC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_doc[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeStatus = (e) => {
    this.setState(produce(this.state, draft => {
      draft.deliveryReceipt.field_purchase_order_status[0].value = e.target.value;
    }));
  }

  validateDeliveryReceipt = () => {
    let flag = true;
    const { deliveryReceipt } = this.state;

    console.log(deliveryReceipt.field_purchase_order_date[0].value);

    this.setState(produce(this.state, draft => {
      if(deliveryReceipt.title[0].value.length === 0) {
        flag = false;
        draft.deliveryReceipt.title[0].error = true;
        toast.error("Please enter a Delivery Receipt Number");
      }
      if(!deliveryReceipt.field_purchase_order_date[0].value) {
        flag = false;
        draft.deliveryReceipt.field_purchase_order_date[0].error = true;
        toast.error("Please select a Delivery Receipt date");
      }
      // if(deliveryReceipt.field_purchase_order_ship_to[0].target_id === 0) {
      //   flag = false;
      //   draft.deliveryReceipt.field_purchase_order_ship_to[0].error = true;
      //   toast.error("Please select a location/ship-to");
      // }
      if(deliveryReceipt.field_purchase_order_items.length === 0) {
        flag = false;
        draft.tempItemError = true;
        toast.error("Please add at least one item");
      }
    }))

    return flag;
  }

  saveDeliveryReceipt = (e) => {

    e.preventDefault();

    if(this.validateDeliveryReceipt()) {
      let method = 'POST';

      switch(this.state.mode) {
        case MODE.CREATE:
          method = 'POST';
          break;
        case MODE.UPDATE:
          method = 'PATCH';
          break;
        default:
          break;
      }

      this.props.createOrUpdateDeliveryReceipt(method, this.state.deliveryReceipt, (resp) => {
        this.props.getDeliveryReceipts(() => {
          this.props.history.push(`${routes.DELIVERYRECEIPTS}/${resp.nid[0].value}`)
        });
      })
    }

  }

  render() {

    const { isSavingDeliveryReceipt } = this.props;
    const { mode, deliveryReceipt, suppliers, locations, customers, items, tempQuantity, tempTarget, tempItemError }  = this.state;

    let total = 0;
    let userName = '';
    if(deliveryReceipt.uid) {
      let user = this.props.getUserByID(deliveryReceipt.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    } else {
      userName = estore.get('name');
    }

    return (
      <>
        <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop={false} closeOnClick rtl={false} pauseOnVisibilityChange draggable pauseOnHover />

        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.DELIVERYRECEIPTS}>Delivery Receipts</NavLink></li>
            {mode === MODE.CREATE &&
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{deliveryReceipt.title[0].value ? deliveryReceipt.title[0].value : `Create`}</li>
            }
            {mode === MODE.UPDATE &&
              <>
                <li className="breadcrumb-item font-weight-bold text-uppercase">
                  <NavLink className="text-success" to={`${routes.DELIVERYRECEIPTS}/${deliveryReceipt.nid[0].value ? deliveryReceipt.nid[0].value : ``}`}>{deliveryReceipt.title[0].value ? deliveryReceipt.title[0].value : ``}</NavLink>
                </li>
                <li className="breadcrumb-item font-weight-bold text-uppercase active">Edit</li>
              </>
            }
          </ol>
        </nav>

        <form onSubmit={(e) => { this.saveDeliveryReceipt(e); }}>
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 className="h2 text-success font-weight-bold text-uppercase">
              <i className="fas fa-truck-loading mr-2"></i>
              {deliveryReceipt.title[0].value ? deliveryReceipt.title[0].value : `New Purchase Order`}
              <span className={`badge ${getDeliveryReceiptBadgeStatus(deliveryReceipt.field_purchase_order_status[0].value)} ml-2`}>{deliveryReceipt.field_purchase_order_status[0].value}</span>
            </h1>
            {/* <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingDeliveryReceipt}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingDeliveryReceipt ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingDeliveryReceipt ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div> */}
          </div>

          <div className="card mb-4">
            <div className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-3 mt-4">
                  <div className="col"><img src={logo} width="400" /></div>
                  <div className="col">
                    <p className="text-right font-italic font-weight-bold">"Your sports solutions partner"</p>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <p>
                      105 New Society St., Rosario Subd.-4<br/>
                      Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                      Philippines<br/><br/>
                      Tel # +63 2 75007559<br/>
                      Fax # +63 2 77486641<br/><br/>
                      Email: info@linksportsinc.com<br/>
                    </p>
                  </div>
                  <div className="col">
                    <table className="table table-bordered table-sm mb-0">
                      <thead>
                        <tr><th colSpan="2"><h3 className="mb-0 text-uppercase font-weight-bold">Delivery Receipt</h3></th></tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th>Delivery Receipt Number</th>
                          <td  width="50%"><input type="number" className={`form-control form-control-sm ${deliveryReceipt.title[0].error ? `is-invalid` : ``}`} disabled={isSavingDeliveryReceipt} placeholder="Delivery Receipt Number" value={deliveryReceipt.title[0].value} onChange={(e) => { this.changeTitle(e) }} /></td>
                        </tr>
                        <tr>
                          <th>Date</th>
                          <td>
                            <DatePicker className={`form-control form-control-sm ${deliveryReceipt.field_purchase_order_date[0].error ? `is-invalid` : ``}`} disabled={isSavingDeliveryReceipt} placeholderText="Delivery Receipt Date" selected={deliveryReceipt.field_purchase_order_date[0].value} onChange={(date) => {this.changeDate(date)}} dateFormat="MMMM dd, yyyy" isClearable />
                            {/* <input type="date" className="form-control form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Delivery Receipt Date" value={moment(deliveryReceipt.field_purchase_order_date[0].value).format('YYYY-MM-DD')} onChange={(e) => { this.changeDate(e) }}/> */}
                          </td>
                        </tr>
                        {/* <tr>
                          <th>Terms</th>
                          <td>
                            <select className="form-control form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Terms" value={deliveryReceipt.field_purchase_order_terms[0].value} onChange={(e) => { this.changeTerms(e) }}>
                              {Object.keys(terms).map((term, index) => {
                                return (
                                  <option value={term} key={`terms-${index}`}>
                                    {terms[term]}
                                  </option>
                                )
                              })}
                            </select>
                          </td>
                        </tr> */}
                        <tr>
                          <th>PO</th>
                          <td width="50%"><input type="text" className={`form-control form-control-sm`} disabled={isSavingDeliveryReceipt} placeholder="Delivery Receipt PO" value={deliveryReceipt.field_purchase_order[0]?.value} onChange={(e) => { this.changePO(e) }} /></td>
                        </tr>
                        <tr>
                          <th>Shipment</th>
                          <td>
                            {/* <input className="form-control form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Shipping" value={deliveryReceipt.field_purchase_order_shipping[0].value} onChange={(e) => { this.changeShipping(e) }} /> */}
                            <select className="form-control form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Shipment" value={deliveryReceipt.field_purchase_order_shipping[0].value} onChange={(e) => { this.changeShipping(e) }}>
                              {Object.keys(shipments).map((shipment, index) => {
                                return (
                                  <option value={shipment} key={`shipment-${index}`}>
                                    {shipments[shipment]}
                                  </option>
                                )
                              })}
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Delivery</th>
                          <td>
                            {/* <input className="form-control form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Delivery" value={deliveryReceipt.field_purchase_order_delivery[0].value} onChange={(e) => { this.changeDelivery(e) }} /> */}
                            <select className="form-control form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Delivery" value={deliveryReceipt.field_purchase_order_delivery[0].value} onChange={(e) => { this.changeDelivery(e) }}>
                              {Object.keys(deliveries).map((delivery, index) => {
                                return (
                                  <option value={delivery} key={`delivery-${index}`}>
                                    {deliveries[delivery]}
                                  </option>
                                )
                              })}
                            </select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div className="row mb-3">

                  <div className="col">
                    <div className="card" style={{minHeight: '230px'}}>
                      <div className="card-body">
                        <div className="mb-3">
                          <Typeahead id="locations-typeahead" isInvalid={deliveryReceipt.field_purchase_order_ship_to[0].error} placeholder="Search customer" onChange={(selected) => { this.changeCustomer(selected); }} options={customers} clearButton={true} disabled={isSavingDeliveryReceipt} />
                        </div>
                        {/* Customer */}
                        {deliveryReceipt.field_purchase_order_ship_to &&
                          <>
                          {deliveryReceipt.field_purchase_order_ship_to.length > 0 &&
                            <>
                              {deliveryReceipt.field_purchase_order_ship_to.map((locationItem) => {
                                const customer = this.props.getCustomerByID(locationItem.target_id);
                                return (
                                  <React.Fragment key={locationItem.target_id}>
                                    {customer &&
                                      <>
                                        <p  className="mb-0">Ship To:</p>
                                        <h5 className="text-uppercase"><strong>{customer.title[0].value}</strong></h5>
                                        <p>
                                          {customer.field_customer_address_street[0]?.value},&nbsp;
                                          {customer.field_customer_address_cit[0]?.value}<br/>
                                          {customer.field_customer_address_state[0]?.value},&nbsp;
                                          {customer.field_customer_address_zip[0]?.value},&nbsp;
                                          {customer.field_customer_address_country[0]?.value},&nbsp;
                                        </p>
                                        <p>
                                          {customer.field_customer_contact_name[0]?.value}<br/>
                                          {customer.field_customer_email[0]?.value}<br/>
                                          {customer.field_customer_telephone[0]?.value}<br/>
                                        </p>
                                        {/* {location.field_location_contact.length > 0 &&
                                          <>
                                            {location.field_location_contact.map((contactItem) => {
                                              const contact = this.props.getContactByID(contactItem.target_id);
                                              return (
                                                <React.Fragment key={contactItem.target_id}>
                                                  <p className="mb-0">Contact Name:</p>
                                                  <h5 className="mb-0">{contact.title[0].value}</h5>
                                                  <p className="mb-0">{contact.field_contact_mobile[0].value}</p>
                                                  <p className="mb-0">{contact.field_contact_telephone[0].value}</p>
                                                </React.Fragment>
                                              )
                                            })}
                                          </>
                                        } */}
                                      </>
                                    }
                                  </React.Fragment>
                                )
                              })}
                            </>
                          }
                          </>
                        }
                        {/* <p className="mb-0 font-weight-bold">Ship To:</p>
                        <p>
                          <strong>Linksports, Inc.</strong><br/>
                          105 New Society St., Rosario Subd.-4<br/>
                          Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                          Philippines<br/><br/>

                          Contact Name:	<strong>Jojo Eugenio</strong><br/>
                          +63917 5207606<br/>
                          jojo@linksportsinc.com
                        </p> */}
                        {/* <p>Prepared By: {userName}</p> */}
                      </div>
                    </div>
                  </div>
                </div>

                {/* ITEMS */}
                <div className="row">
                  <div className="col">
                    <table className="table table-smd table-striped table-hover">
                      <thead className="thead-dark">
                        <tr>
                          <th className="text-center"></th>
                          <th className="text-center text-uppercase font-weight-bold">Item</th>
                          <th width="12%" className="text-uppercase font-weight-bold">Qty</th>
                          <th width="65%" className="text-uppercase font-weight-bold">Description</th>
                          {/* <th width="15%" className="text-right">Unit Price</th>
                          <th width="25%" className="text-right">Amount</th> */}
                        </tr>
                      </thead>
                      {deliveryReceipt.field_purchase_order_items &&
                        <>
                        {deliveryReceipt.field_purchase_order_items.length > 0 &&
                          <tbody>
                            {deliveryReceipt.field_purchase_order_items.map((itemItem, id) => {
                              const item = this.props.getItemByID(itemItem.target_id);
                              const supplier = this.props.getSupplierByID(item?.field_item_supplier?.[0].target_id);
                              if(item){

                                const itemTotal = parseFloat(parseFloat(item.field_unit_price[0].value) * parseFloat(itemItem.quantity)).toFixed(2);
                                total += parseFloat(itemTotal);

                                return (
                                  <React.Fragment key={`${id}-${itemItem.target_id}`}>
                                    <tr>
                                      <td className="text-center">
                                        <button className="btn btn-danger btn-sm rounded-circle text-center" onClick={() => { this.removeItem(id); }} style={{width: '30px', height: '30px'}}><i className="fas fa-times"></i></button>
                                      </td>
                                      <td className="text-center">{id + 1}</td>
                                      <td>{numeral(itemItem.quantity).format('0,0')}</td>
                                      <td>
                                        <p className="mb-0"><strong>{item.title[0].value}</strong></p>
                                        {item.field_i[0].value &&
                                          <>
                                            {item.field_item.length > 0 &&
                                              <>
                                                {item.field_item.map((subitemItem, subitemid) => {
                                                  const subItem = this.props.getItemByID(subitemItem.target_id);
                                                  if(subItem) {
                                                    return <p key={`subItem-${subItem.target_id}`} className="mb-0" style={{fontSize: '.8rem'}}>{subitemItem.quantity}x  {subItem.title[0].value}</p>
                                                  }
                                                })}
                                              </>
                                            }
                                          </>
                                        }
                                      </td>
                                      {/* <td className="text-right">{numeral(item.field_unit_price[0].value).format('0,0.00')}</td>
                                      <td className="text-right">{numeral(itemTotal).format('0,0.00')}</td> */}
                                    </tr>
                                  </React.Fragment>
                                )
                              }
                            })}
                          </tbody>
                        }
                        </>
                      }
                      <tbody>
                        <tr className={`${tempItemError ? `table-danger` :``}`}>
                          <td className="text-center">
                            <button className="btn btn-success btn-sm rounded-circle text-center" disabled={!tempQuantity || tempTarget.length === 0} onClick={() => { this.addItem(); }} style={{width: '30px', height: '30px'}}><i className="fas fa-plus"></i></button>
                          </td>
                          <td></td>
                          <td><input className="form-control" type="number" disabled={isSavingDeliveryReceipt} placeholder="Qty" value={tempQuantity} min="0" onChange={(e) => { this.changeTempQuantity(e) }} /></td>
                          <td>
                            <Typeahead id="items-typeahead" className="" placeholder="Search Item" onChange={(selected) => { this.changeTemp(selected); }} options={items} clearButton={true} ref={(typeahead) => this.itemsTypeahead = typeahead} disabled={isSavingDeliveryReceipt} />
                          </td>
                          {/* <td className="text-right">
                            {tempTarget.length > 0 &&
                              <>{numeral(tempTarget[0].field_unit_price).format('0,0.00')}</>
                            }
                            {(() => {
                              if(tempQuantity && tempTarget.length > 0) {
                                total = total + parseFloat(tempTarget[0].field_unit_price) * parseFloat(tempQuantity);
                              }
                            })()}
                          </td>
                          <td className="text-right">
                            {(() => {
                              if(tempQuantity && tempTarget.length > 0) {
                                return numeral(parseFloat(total)).format('0,0.00');
                              }
                            })()}
                          </td> */}
                        </tr>
                        <tr className="border-bottom">
                          <td colSpan={7} className="text-center">
                            <strong>---------- NOTHING FOLLOWS ----------</strong>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                {/* <div style={{ pageBreakAfter: 'always' }} /> */}

                <div className="row">
                  <div className="col">
                    <table className="table table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="border-top-0">
                            {(() => {
                              if(deliveryReceipt.nid){
                                if(isAdmin() || isOwner(deliveryReceipt.uid[0].target_id)) {
                                  return (
                                    <div className="form-group">
                                      <label>Status</label>
                                      <select className="form-control col-5" value={deliveryReceipt.field_purchase_order_status[0].value} onChange={(e) => { this.changeStatus(e) }}>
                                        {Object.keys(DELIVERYRECEIPTSTATUS).map((key, id) => {
                                          if(key === 'APPROVED') {
                                            if(isAdmin()) {
                                              return <option key={id} value={DELIVERYRECEIPTSTATUS[key]}>{key}</option>;
                                            }
                                          } else {
                                            return <option key={id} value={DELIVERYRECEIPTSTATUS[key]}>{key}</option>;
                                          }
                                        })}
                                      </select>
                                    </div>
                                  );
                                }
                              }
                            })()}
                            {deliveryReceipt.field_purchase_order_status[0].value === DELIVERYRECEIPTSTATUS.APPROVED &&
                              <>
                                <p>
                                  <strong>Remarks</strong><br/><br/><br/>
                                  <em>Electronically approved, signature not required</em><br/><br/>
                                  <strong>Approved By:</strong><br/>
                                  JLE
                                </p>
                                <p>
                                  The undersigned individual hereby acknowledges the receipt and the delivered goods detailed on the included list/invoice. Furthermore, this letter acknowledges that delivered goods underwent proper inspection and without any defect.
                                </p>
                                <div className="row">
                                  <div className="col">
                                    Received by<br/><br/>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col">
                                    <hr />
                                    <strong>Name</strong>
                                  </div>
                                  <div className="col">
                                    <hr />
                                    <strong>Signature</strong>
                                  </div>
                                  <div className="col">
                                    <hr />
                                    <strong>Date</strong>
                                  </div>
                                </div>
                              </>
                            }

                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <div className="row">
                  {/* <div className="col">
                    <table  className="table table-sm table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="text-right bg-dark text-white">Subtotal</td>
                          <td className="text-right" width="50%">{numeral(parseFloat(total)).format('0,0.00')}</td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">VAT</td>
                          <td className="text-right">
                            <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="VAT" min="0" value={deliveryReceipt.field_purchase_order_vat[0].value}  onChange={(e) => { this.changeVAT(e) }} />
                          </td>
                        </tr> */}
                        {/* <tr>
                          <td className="text-right bg-dark text-white">Ex-Works Value - Sub-Total</td>
                          <td className="text-right">
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Ex-Works Value - Sub-Total" min="0" value={deliveryReceipt.field_purchase_ex_works[0].value}  onChange={(e) => { this.changeEW(e) }} />
                          </td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Certificate of Conformity</td>
                          <td className="text-right">
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Certificate of Conformity" min="0" value={deliveryReceipt.field_purchase_coc[0].value}  onChange={(e) => { this.changeCOC(e) }} />
                          </td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Handling Pack SOLAS</td>
                          <td className="text-right">
                            {console.log(deliveryReceipt)}
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Handling Pack SOLAS" min="0" value={deliveryReceipt.field_purchase_hps[0].value}  onChange={(e) => { this.changeHPS(e) }} />
                          </td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Photo/HT Certificate</td>
                          <td className="text-right">
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Photo/HT Certificate" min="0" value={deliveryReceipt.field_purchase_phc[0].value}  onChange={(e) => { this.changePHC(e) }} />
                          </td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Freight</td>
                          <td className="text-right">
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Freight" min="0" value={deliveryReceipt.field_purchase_freight[0].value}  onChange={(e) => { this.changeFreight(e) }} />
                          </td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Bank and or Courier Cost</td>
                          <td className="text-right">
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Bank and or Courier Cost" min="0" value={deliveryReceipt.field_purchase_bcc[0].value}  onChange={(e) => { this.changeBCC(e) }} />
                          </td>
                        </tr>
                        <tr>
                          <td className="text-right bg-dark text-white">Documentation Fee</td>
                          <td className="text-right">
                            <input type="number" className="form-control text-right form-control-sm" disabled={isSavingDeliveryReceipt} placeholder="Documentation Fee" min="0" value={deliveryReceipt.field_purchase_doc[0].value}  onChange={(e) => { this.changeDOC(e) }} />
                          </td>
                        </tr> */}
                        {(() => {
                          // total = total + parseFloat(deliveryReceipt.field_purchase_order_vat[0].value);

                          // total = total + parseFloat(deliveryReceipt.field_purchase_ex_works[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_coc[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_hps[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_phc[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_freight[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_bcc[0].value);
                          // total = total + parseFloat(deliveryReceipt.field_purchase_doc[0].value);
                        })()}
                        {/* <tr>
                          <td className="text-right bg-dark text-white pt-3 pb-3"><h5 className="font-weight-bold">TOTAL</h5></td>
                          <td className="text-right pt-3 pb-3">
                            <h5 className="mb-0">Php {numeral(parseFloat(total)).format('0,0.00')}</h5>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div> */}
                </div>

                <div className="row mt-4">
                  <div className="col text-right">
                    {/* <span className="d-inline-block mr-3 text-success">www.linksportsinc.com</span> | <span className="d-inline-block ml-3">jeugenio@linksportsinc.com</span> */}
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="mb-3">
            <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingDeliveryReceipt}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingDeliveryReceipt ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingDeliveryReceipt ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div>
            <div className="clearfix"></div>
          </div>

        </form>

      </>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    suppliers: state.suppliers.suppliers,
    locations: state.locations.locations,
    customers: state.customers.customers,
    items: state.items.items,
    isSavingDeliveryReceipt: state.deliveryreceipts.isSavingDeliveryReceipt
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getUserByID,
    getDeliveryReceipts,
    getDeliveryReceipt,
    createOrUpdateDeliveryReceipt,
    getSupplierByID,
    getContactByID,
    getLocationByID,
    getCustomerByID,
    getItemByID,
    getNextDeliveryReceiptNumber
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(DeliveryReceiptEditor));
