// @flow
import React, { Component } from 'react';

import Navigation from '../Navigation/Navigation';

import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router';
import routes from '../../constants/routes.json';

import DeliveryReceiptsList from './DeliveryReceiptsList';
import DeliveryReceipt from './DeliveryReceipt/DeliveryReceipt';
import DeliveryReceiptEditor from './DeliveryReceipt/DeliveryReceiptEditor';

class DeliveryReceipts extends Component {
  render() {

    const { location, match } = this.props;

    console.log(location)

    return (
      <Switch location={location}>
        <Route path={routes.DELIVERYRECEIPTS} component={DeliveryReceiptsList} exact />
        <Route path={`${routes.DELIVERYRECEIPTS}/create`} component={DeliveryReceiptEditor} exact />
        <Route path={`${routes.DELIVERYRECEIPTS}/:id`} component={DeliveryReceipt} exact />
        <Route path={`${routes.DELIVERYRECEIPTS}/:id/edit`} component={DeliveryReceiptEditor} exact />
      </Switch>
    );
  }
}

export default withRouter(DeliveryReceipts);
