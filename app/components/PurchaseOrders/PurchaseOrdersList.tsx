import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import { Typeahead } from 'react-bootstrap-typeahead';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import routes from '../../constants/routes.json';
import { getPurchaseOrders, deletePurchaseOrder, getPurchaseOrderBadgeStatus, STATUS as PURCHASEORDERSTATUS } from '../../actions/purchaseorders';
import { isAdmin, isOwner } from '../../actions/auth';
import { getUserByID } from '../../actions/users';

const { dialog } = require('electron').remote;

class PurchaseOrdersList extends Component {

  constructor(props) {
    super(props)

    this.supplierInput = React.createRef();
    this.locationInput = React.createRef();
    this.itemsInput = React.createRef();
    this.userInput = React.createRef();
  }

  state = {
    purchaseOrders: [],
    filter: {
      enabled: false,
      title: [{value: ''}],
      field_purchase_order_date: [{value: null}],
      field_purchase_order_supplier: [{target_id: null}],
      field_purchase_order_ship_to: [{target_id: null}],
      field_purchase_order_items: [],
      uid: [{target_id: null}],
      field_purchase_order_status: [{value: 0}]
    },
    suppliers: [],
    locations: [],
    items: [],
    users: []
  }

  componentDidMount = () => {

    this.setState({purchaseOrders: this.props.purchaseOrders}, () => {
      if(this.state.filter.enabled) {
        this.filterPurchaseOrders();
      }
    });

    this.mapSuppliers();
    this.mapLocations();
    this.mapItems();
    this.mapUsers();
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(prevProps.purchaseOrders !== this.props.purchaseOrders) {
      this.setState({purchaseOrders: this.props.purchaseOrders}, () => {
        if(this.state.filter.enabled) {
          this.filterPurchaseOrders();
        }
      });
    }
    if(prevProps.suppliers !== this.props.suppliers) {
      this.mapSuppliers()
    }
    if(prevProps.locations !== this.props.locations) {
      this.mapLocations()
    }
    if(prevProps.items !== this.props.items) {
      this.mapItems()
    }
    if(prevProps.users !== this.props.users) {
      this.mapUsers()
    }

    if(prevState.filter.enabled !== this.state.filter.enabled && !this.state.filter.enabled) {
      this.clearFilter()
    }
  }

  showHideFilter = () => {
    this.setState(produce(this.state, draft => {
      draft.filter.enabled = !this.state.filter.enabled;
    }));
  }

  mapSuppliers = () => {
    const suppliers = this.props.suppliers.map((supplier) => {
      return {
        id: supplier.nid[0].value,
        label: supplier.title[0].value
      }
    });
    this.setState({suppliers});
  }

  mapLocations = () => {
    const locations = this.props.locations.map((location) => {
      return {
        id: location.nid[0].value,
        label: location.title[0].value
      }
    });
    this.setState({locations});
  }

  mapItems = () => {
    const items = this.props.items.map((item) => {
      return {
        id: item.nid[0].value,
        label: item.title[0].value,
        field_unit_price: item.field_unit_price[0].value
      }
    });
    this.setState({items});
  }

  mapUsers = () => {
    const users = this.props.users.map((user) => {
      return {
        id: user.uid[0].value,
        label: user.name[0].value
      }
    });
    this.setState({users});
  }

  changeTitleFilter = (e) => {
    this.setState(produce(this.state, draft => {
      draft.filter.title[0].value = e.target.value;
    }));
  }

  changeDateFilter = (date) => {
    this.setState(produce(this.state, draft => {
      draft.filter.field_purchase_order_date[0].value = date;
    }));
  }

  changeSupplierFilter = (suppliers) => {
    if(suppliers.length > 0){
      this.setState(produce(this.state, draft => {
        draft.filter.field_purchase_order_supplier[0] = {target_id: suppliers[0].id};
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.filter.field_purchase_order_supplier[0] = {target_id: null};
      }));
    }
  }

  changeLocationFilter = (locations) => {
    if(locations.length > 0){
      this.setState(produce(this.state, draft => {
        draft.filter.field_purchase_order_ship_to[0] = { target_id: locations[0].id };
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.filter.field_purchase_order_ship_to[0] = {target_id: null};
      }));
    }
  }

  changeUserFilter = (users) => {
    if(users.length > 0){
      this.setState(produce(this.state, draft => {
        draft.filter.uid[0] = { target_id: users[0].id };
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.filter.uid[0] = { target_id: null };
      }));
    }
  }

  changeItemsFilter = (items) => {
    if(items.length > 0){
      this.setState(produce(this.state, draft => {
        draft.filter.field_purchase_order_items = items
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.filter.field_purchase_order_items = []
      }));
    }
  }

  changeStatusFilter = (e) => {
    this.setState(produce(this.state, draft => {
      draft.filter.field_purchase_order_status[0].value = e.target.value;
    }));
  }

  clearFilter = () => {

    this.supplierInput.getInstance().clear();
    this.locationInput.getInstance().clear();
    this.itemsInput.getInstance().clear();
    this.userInput.getInstance().clear();

    this.setState(produce(this.state, draft => {
      draft.filter.title = [{value: ''}];
      draft.filter.field_purchase_order_date = [{value: null}];
      draft.filter.field_purchase_order_supplier = [{target_id: null}];
      draft.filter.field_purchase_order_ship_to = [{target_id: null}];
      draft.filter.field_purchase_order_items = [];
      draft.filter.uid = [{id: null}];
      draft.filter.field_purchase_order_status = [{value: 0}];
    }), () => { this.filterPurchaseOrders(); });
  }

  filterPurchaseOrders = (e) => {

    if(e) {
      e.preventDefault();
    }

    const { filter } = this.state;
    let purchaseOrders = this.props.purchaseOrders;

    if(filter.title[0].value.length > 0) {
      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {
        return purchaseOrder.title[0].value.match(filter.title[0].value)
      });
    }

    if(filter.field_purchase_order_date[0].value) {
      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {
        return moment(purchaseOrder.field_purchase_order_date[0].value).format('MM/DD/YYYY') === moment(filter.field_purchase_order_date[0].value).format('MM/DD/YYYY');
      });
    }

    if(filter.field_purchase_order_supplier[0].target_id) {
      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {
        return purchaseOrder.field_purchase_order_supplier[0].target_id === filter.field_purchase_order_supplier[0].target_id;
      });
    }

    if(filter.field_purchase_order_ship_to[0].target_id) {
      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {
        return purchaseOrder.field_purchase_order_ship_to[0].target_id === filter.field_purchase_order_ship_to[0].target_id;
      });
    }

    if(filter.field_purchase_order_items.length > 0) {
      const itemIDs = filter.field_purchase_order_items.map((item) => {
        return item.id;
      });

      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {

        const purchaseOrderItemIDs = purchaseOrder.field_purchase_order_items.map((item) => {
          return item.target_id;
        });

        return itemIDs.every(function(item) {
          return purchaseOrderItemIDs.indexOf(item) !== -1;
        });

      });
    }

    if(filter.uid[0].target_id) {
      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {
        return purchaseOrder.uid[0].target_id === filter.uid[0].target_id;
      });
    }

    if(filter.field_purchase_order_status[0].value !== 0) {
      purchaseOrders = purchaseOrders.filter((purchaseOrder) => {
        return purchaseOrder.field_purchase_order_status[0].value === filter.field_purchase_order_status[0].value;
      });
    }

    this.setState({purchaseOrders});
  }

  confirmDeletePurchaseOrder = (purchaseOrder) => {
    // dialog.showMessageBox(
    //   {
    //     message: `Do you want to delete PO Number ${purchaseOrder.title[0].value}?`,
    //     buttons: ['Yes','No']
    //   },
    //   (response) => {
    //     if(response === 0) {
    //       this.props.deletePurchaseOrder(purchaseOrder.nid[0].value, () => {
    //         this.props.getPurchaseOrders(() => {
    //         })
    //       })
    //     }
    //   }
    // );

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete PO Number ${purchaseOrder.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deletePurchaseOrder(purchaseOrder.nid[0].value, () => {
        this.props.getPurchaseOrders(() => {
        })
      })
    }
  }

  render() {

    const { isGettingPurchaseOrders, deletingPurchaseOrderID, isDeletingPurchaseOrder } = this.props;
    const { purchaseOrders, filter, suppliers, locations, items, users } = this.state;

    return (
      <>
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase active">Purchase Orders</li>
          </ol>
        </nav>

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fas fa-inbox mr-2"></i>Purchase Orders</h1>
          <div className="float-right">
            <NavLink className="btn btn-outline-success btn-lg" to={`${routes.PURCHASEORDERS}/create`}><i className="fas fa-pen mr-3"></i>Create</NavLink>
          </div>
        </div>

        <h4 className="float-left">Purchase Orders List</h4>
        <ul className="nav nav-tabs float-right" style={{borderBottom: '0px'}}>
          <li className="nav-item">
            <a className={`nav-link cursor-pointer ${filter.enabled ? `active` : ``}`} onClick={() => { this.showHideFilter(); }}>
              <i className="fas fa-filter mr-2"></i>
              Filter
            </a>
          </li>
        </ul>
        <div className="clearfix"></div>

        <div className={`filter-box border-top border-left border-right p-3 ${filter.enabled ? `` : `d-none`}`}>
          <form onSubmit={(e) => { this.filterPurchaseOrders(e); }}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-4 pl-0">
                  <div className="form-group">
                    <label>Filter by PO Number</label>
                    <input className="form-control" placeholder="PO Number" value={filter.title[0].value} onChange={(e) => { this.changeTitleFilter(e) }} />
                  </div>
                </div>
                <div className="col-4">
                  <div className="form-group">
                    <label>Filter by PO Date</label>
                    <DatePicker className="form-control" placeholderText="PO Date" selected={filter.field_purchase_order_date[0].value} onChange={(date) => {this.changeDateFilter(date)}} dateFormat="MMMM dd, yyyy" isClearable />
                  </div>
                </div>
                <div className="col-4 pr-0">
                  <div className="float-right">
                    <button onClick={() => { this.clearFilter(); }} className="btn btn-link mr-2 text-dark">Clear Filter</button>
                    <button type="submit" className="btn btn-outline-success"><i className="fas fa-filter mr-3"></i>Filter</button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-4 pl-0">
                  <div className="form-group">
                    <label>Filter by Supplier</label>
                    <Typeahead id="supplier-typeahead" ref={(e) => { this.supplierInput = e; }} placeholder="Search supplier" onChange={(selected) => { this.changeSupplierFilter(selected); }} options={suppliers} clearButton={true} />
                  </div>
                </div>
                <div className="col-4">
                  <div className="form-group">
                    <label>Filter by Location</label>
                    <Typeahead id="locations-typeahead" ref={(e) => { this.locationInput = e; }} placeholder="Search location" onChange={(selected) => { this.changeLocationFilter(selected); }} options={locations} clearButton={true} />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8 pl-0">
                  <div className="form-group">
                    <label>Filter by Items</label>
                    <Typeahead id="items-typeahead" ref={(e) => { this.itemsInput = e; }} placeholder="Search items" onChange={(selected) => { this.changeItemsFilter(selected); }} options={items} clearButton={true} multiple={true} />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8 pl-0">
                  <div className="form-group">
                    <label>Filter by prepared by</label>
                    <Typeahead id="users-typeahead" ref={(e) => { this.userInput = e; }} placeholder="Search users" onChange={(selected) => { this.changeUserFilter(selected); }} options={users} clearButton={true} />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-8 pl-0">
                  <div className="form-group">
                    <label>Filter by status</label>
                    <select className="form-control col-5" value={filter.field_purchase_order_status[0].value} onChange={(e) => { this.changeStatusFilter(e) }}>
                      <option value={0}>ANY</option>
                      {Object.keys(PURCHASEORDERSTATUS).map((key, id) => {
                        return <option key={id} value={PURCHASEORDERSTATUS[key]}>{key}</option>;
                      })}
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div className="table-responsive">
          <table className="table table-striped table-sm table-hover">
            <thead>
              <tr>
                <th>PO#</th>
                <th width="115">Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingPurchaseOrders &&
                <tr>
                  <td colSpan="2">
                    <em>Loading Purchase Orders</em>
                  </td>
                </tr>
              }
              {!isGettingPurchaseOrders &&
                <>
                {purchaseOrders.length > 0 &&
                  <>
                    {purchaseOrders.map((purchaseOrder, idx) => {
                      return (
                        <tr key={purchaseOrder.nid[0].value} className={`${deletingPurchaseOrderID === purchaseOrder.nid[0].value ? `table-danger` : ``}`}>
                          <td>
                            <p style={{fontSize: '1.2rem'}} className="mb-0">
                              {purchaseOrder.title[0].value}
                              <span className={`badge ${getPurchaseOrderBadgeStatus(purchaseOrder.field_purchase_order_status[0].value)} ml-2`}>{purchaseOrder.field_purchase_order_status[0].value}</span>
                            </p>
                            {(() => {
                              if(purchaseOrder.uid) {
                                let user = this.props.getUserByID(purchaseOrder.uid[0].target_id);
                                if(user) {
                                  let userName = user.name[0].value;
                                  return `Prepared by ${userName} ${isOwner(purchaseOrder.uid[0].target_id) ? `(You)` : ``}`;
                                }
                              }
                            })()}
                          </td>
                          <td>
                            <NavLink className={`btn btn-outline-success btn-sm mr-1 ${deletingPurchaseOrderID === purchaseOrder.nid[0].value ? `disabled` : ``}`} to={`${routes.PURCHASEORDERS}/${purchaseOrder.nid[0].value}`}>
                              <i className="fas fa-search"></i>
                            </NavLink>
                            {(() => {
                              let editable = false;

                              if(isAdmin() || isOwner(purchaseOrder.uid[0].target_id)) {
                                editable = true;
                              }
                              if(purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.APPROVED || purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.CANCELLED) {
                                editable = false;
                              }

                              if(editable) {
                                return (
                                  <NavLink className={`btn btn-outline-dark btn-sm mr-1 ${deletingPurchaseOrderID === purchaseOrder.nid[0].value ? `disabled` : ``}`} to={`${routes.PURCHASEORDERS}/${purchaseOrder.nid[0].value}/edit`}>
                                    <i className="fas fa-pen"></i>
                                  </NavLink>
                                )
                              }
                            })()}
                            {(() => {
                              let deletable = false;

                              if(isAdmin() || isOwner(purchaseOrder.uid[0].target_id)) {
                                deletable = true;
                              }
                              if(purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.APPROVED || purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.CANCELLED) {
                                deletable = false;
                              }

                              if(deletable) {
                                return (
                                  <button className={`btn btn-outline-danger btn-sm ${deletingPurchaseOrderID === purchaseOrder.nid[0].value ? `disabled` : ``}`} onClick={() => { this.confirmDeletePurchaseOrder(purchaseOrder) }}>
                                    <i className="fas fa-trash"></i>
                                  </button>
                                )
                              }
                            })()}
                          </td>
                        </tr>
                      )
                    })}
                  </>
                }
                {purchaseOrders.length === 0 &&
                  <tr>
                    <td colSpan="2">
                      <em>No Purchase Orders</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {purchaseOrders.length} Purchase Order{purchaseOrders.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    suppliers: state.suppliers.suppliers,
    locations: state.locations.locations,
    items: state.items.items,
    users: state.users.users,
    isGettingPurchaseOrders: state.purchaseorders.isGettingPurchaseOrders,
    purchaseOrders: state.purchaseorders.purchaseOrders,
    deletingPurchaseOrderID: state.purchaseorders.deletingPurchaseOrderID,
    isDeletingPurchaseOrder: state.purchaseorders.isDeletingPurchaseOrder
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getPurchaseOrders,
    deletePurchaseOrder,
    getUserByID,
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PurchaseOrdersList);
