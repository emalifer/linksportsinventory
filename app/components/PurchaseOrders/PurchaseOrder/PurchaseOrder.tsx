import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import { ipcRenderer } from 'electron';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import { getPurchaseOrders, getPurchaseOrder, deletePurchaseOrder, getPurchaseOrderBadgeStatus, STATUS as PURCHASEORDERSTATUS, CURRENCY, getCurrencyDisplay } from '../../../actions/purchaseorders';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getSupplierByID } from '../../../actions/suppliers';
import { getContactByID } from '../../../actions/contacts';
import { getLocationByID } from '../../../actions/locations';
import { getItemByID } from '../../../actions/items';
import { getUserByID } from '../../../actions/users';

const { dialog } = require('electron').remote;

const generateInstruction = (instruction) => {
  return { __html: instruction };
};

class PurchaseOrder extends Component {

  state = {
    purchaseOrder: {},
    printModal: false,
    verticalSpace: '0',
    pages: [
      { text: 'Page 1 of 1', left: '2', top: '92' },
      { text: 'www.linksportsinc.com | jeugenio@linksportsinc.com', left: '42', top: '92' }
    ]
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getPurchaseOrder(this.props.match.params.id, (purchaseOrder) => {
        this.setState(
          produce(this.state, draft => {
            draft.purchaseOrder = purchaseOrder;
            console.log(purchaseOrder)
            if(purchaseOrder.field_purchase_order_terms.length === 0) {
              draft.purchaseOrder.field_purchase_order_terms = [{value: ''}]
            }
            if(purchaseOrder.field_purchase_order_shipping.length === 0) {
              draft.purchaseOrder.field_purchase_order_shipping = [{value: ''}]
            }
            if(purchaseOrder.field_purchase_order_delivery.length === 0) {
              draft.purchaseOrder.field_purchase_order_delivery = [{value: ''}]
            }
            if(purchaseOrder.field_purchase_order_currency) {
              if(purchaseOrder.field_purchase_order_currency.length === 0) {
                draft.purchaseOrder.field_purchase_order_currency = [{value: CURRENCY.USD}]
              }
            }
            if(!purchaseOrder.field_purchase_order_currency) {
              draft.purchaseOrder.field_purchase_order_currency = [{value: CURRENCY.USD}]
            }

            // if(purchaseOrder.field_purchase_ewv) {
            //   draft.purchaseOrder.field_purchase_ewv = [{value: ''}]
            // }
            // if(purchaseOrder.field_purchase_coc_p) {
            //   draft.purchaseOrder.field_purchase_coc_p = [{value: ''}]
            // }
            // if(purchaseOrder.field_purchase_hps_p) {
            //   draft.purchaseOrder.field_purchase_hps_p = [{value: ''}]
            // }
            // if(purchaseOrder.photo_ht_certificate_p) {
            //   draft.purchaseOrder.photo_ht_certificate_p = [{value: ''}]
            // }
            // if(purchaseOrder.field_purchase_freight_p) {
            //   draft.purchaseOrder.field_purchase_freight_p = [{value: ''}]
            // }
            // if(purchaseOrder.bank_and_or_courier_cost_p) {
            //   draft.purchaseOrder.bank_and_or_courier_cost_p = [{value: ''}]
            // }
            // if(purchaseOrder.field_purchase_doc_p) {
            //   draft.purchaseOrder.field_purchase_doc_p = [{value: ''}]
            // }

          })
        );
      })
    }
  }

  addPage = (e) => {
    e.preventDefault();
    this.setState(produce(this.state, draftState => {
      draftState.pages.push({
        text: e.target.elements.text.value,
        left: e.target.elements.left.value,
        top: e.target.elements.top.value
      })
    }))
  }

  removePage = (i) => {
    this.setState(produce(this.state, draftState => {
      draftState.pages.splice(parseInt(i, 10), 1);
    }))
  }

  sendToPrinter = () => {
    ipcRenderer.send("printPDF", `PO-${this.state.purchaseOrder.title[0].value}`, document.getElementById('purchaseorder').innerHTML);
  }

  confirmDeletePurchaseOrder = () => {
    const { purchaseOrder } = this.state;

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete PO Number ${purchaseOrder.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deletePurchaseOrder(purchaseOrder.nid[0].value, () => {
        this.props.getPurchaseOrders(() => {
          this.props.history.push(`${routes.PURCHASEORDERS}`);
        })
      })
    }

    // dialog.showMessageBox(
    //   {
    //     message: `Do you want to delete PO Number ${purchaseOrder.title[0].value}?`,
    //     buttons: ['Yes','No']
    //   },
    //   (response) => {
    //     if(response === 0) {
    //       this.props.deletePurchaseOrder(purchaseOrder.nid[0].value, () => {
    //         this.props.getPurchaseOrders(() => {
    //           this.props.history.push(`${routes.PURCHASEORDERS}`);
    //         })
    //       })
    //     }
    //   }
    // );
  }

  render() {

    const { purchaseOrder, printModal, verticalSpace, pages }  = this.state;
    const { isDeletingPurchaseOrder }  = this.props;
    let total = 0;
    let userName = '';
    if(purchaseOrder.uid) {
      let user = this.props.getUserByID(purchaseOrder.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    }

    if(purchaseOrder.nid){
      return (
        <>
          <nav className="mt-4">
            <ol className="breadcrumb">
              <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.PURCHASEORDERS}>Purchase Orders</NavLink></li>
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{purchaseOrder.title ? purchaseOrder.title[0].value : ``}</li>
            </ol>
          </nav>

          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 className="h2 text-success font-weight-bold text-uppercase">
              <i className="fas fa-inbox mr-2"></i>
              {purchaseOrder.title ? purchaseOrder.title[0].value : ``}
              <span className={`badge ${getPurchaseOrderBadgeStatus(purchaseOrder.field_purchase_order_status[0].value)} ml-2`}>{purchaseOrder.field_purchase_order_status[0].value}</span>
            </h1>
            <div className="float-right">
              <button className="btn btn-outline-success btn-lg mr-2" onClick={() => { this.setState({printModal: !printModal})}}>Print PDF Settings</button>
              <button className="btn btn-outline-success btn-lg mr-2" onClick={() => { this.sendToPrinter(); }}><i className="fas fa-print mr-3"></i>Print</button>
              {(() => {
                let editable = false;

                if(isAdmin() || isOwner(purchaseOrder.uid[0].target_id)) {
                  editable = true;
                }
                if(purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.APPROVED || purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.CANCELLED) {
                  editable = false;
                }

                if(editable) {
                  return (
                    <NavLink className={`btn btn-outline-secondary btn-lg mr-2 ${isDeletingPurchaseOrder ? `disabled` : ``}`} to={`${routes.PURCHASEORDERS}/${purchaseOrder.nid[0].value}/edit`}>
                      <i className="fas fa-pen mr-3"></i>Edit
                    </NavLink>
                  )
                }
              })()}
              {(() => {
                let deletable = false;

                if(isAdmin() || isOwner(purchaseOrder.uid[0].target_id)) {
                  deletable = true;
                }
                if(purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.APPROVED || purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.CANCELLED) {
                  deletable = false;
                }

                if(deletable) {
                  return (
                    <button className="btn btn-outline-danger btn-lg" disabled={isDeletingPurchaseOrder} onClick={() => { this.confirmDeletePurchaseOrder(); }}><i className="fas fa-trash mr-3"></i>
                      {isDeletingPurchaseOrder ? 'Deleting' : 'Delete'}
                    </button>
                  )
                }
              })()}
            </div>
          </div>

          <div className="card mb-4">
            <div id="purchaseorder" className="card-body">

              {pages.map((page, i) => (
                <div className="position-absolute d-none d-print-block" style={{ left: `${page.left}rem`, top: `${page.top}rem`, zIndex: 1000 }} key={`pagenumber-${i}`}>
                  {page.text}
                </div>
              ))}

              <div className="container-fluid mb-4">

                <div className="row mb-3">
                  <div className="col"><img src={logo} width="400" /></div>
                  <div className="col">
                    <p className="text-right font-italic font-weight-bold">"Your sports solutions partner"</p>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <p>
                      105 New Society St., Rosario Subd.-4<br/>
                      Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                      Philippines<br/><br/>
                      Tel # +63 2 75007559<br/>
                      Fax # +63 2 77486641<br/><br/>
                      Email: info@linksportsinc.com<br/>
                    </p>
                  </div>
                  <div className="col">
                    <table className="table table-bordered table-sm mb-0">
                      <thead>
                        <tr><th colSpan="2"><h3 className="mb-0 text-uppercase font-weight-bold">Purchase Order</h3></th></tr>
                      </thead>
                      <tbody>
                        <tr><th>PO Number</th><td>{purchaseOrder.title[0].value}</td></tr>
                        <tr><th>Date</th><td>{purchaseOrder.field_purchase_order_date ? moment(purchaseOrder.field_purchase_order_date[0].value).format(`MMMM DD, YYYY`) : ``}</td></tr>
                        <tr><th>Terms</th><td>{purchaseOrder.field_purchase_order_terms[0].value}</td></tr>
                        <tr><th>Shipment</th><td>{purchaseOrder.field_purchase_order_shipping[0].value}</td></tr>
                        <tr><th>Delivery</th><td  width="50%">{purchaseOrder.field_purchase_order_delivery[0].value}</td></tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    {/* Supplier */}
                    {purchaseOrder.field_purchase_order_supplier &&
                      <>
                      {purchaseOrder.field_purchase_order_supplier.length > 0 &&
                        <>
                          {purchaseOrder.field_purchase_order_supplier.map((supplierItem) => {
                            const supplier = this.props.getSupplierByID(supplierItem.target_id);
                            return (
                              <React.Fragment key={supplierItem.target_id}>
                                {supplier &&
                                  <div className="card" style={{minHeight: '180px'}}>
                                    <div className="card-body">
                                      <p className="mb-0">Vendor ID: {supplier.field_supplier_code[0].value}</p>
                                      <h5 className="text-uppercase">{supplier.title[0].value}</h5>
                                      <p>
                                        {supplier.field_supplier_address_street[0]?.value},&nbsp;
                                        {supplier.field_supplier_address_city[0]?.value},&nbsp;
                                        {supplier.field_supplier_address_state[0]?.value},&nbsp;
                                        {supplier.field_supplier_address_zip[0]?.value},&nbsp;
                                        {supplier.field_supplier_address_country[0]?.value}
                                      </p>

                                      <p>
                                        {supplier.supplier_contact_name?.[0]?.value}<br/>
                                        {supplier.field_supplier_telephone[0]?.value}<br/>
                                        {supplier.field_supplier_email_address[0]?.value}
                                      </p>

                                      {/* {supplier.field_supplier_contact.map((contactItem, id) => {
                                        const contact = this.props.getContactByID(contactItem.target_id);
                                        return (
                                          <React.Fragment key={contactItem.target_id}>
                                            <p className="mb-0">
                                              {contact.title[0].value},&nbsp;
                                              {contact.field_contact_designation[0].value}
                                            </p>
                                            <p className="mb-0">
                                              Tel: {contact.field_contact_telephone[0].value},&nbsp;
                                              Fax: {contact.field_contact_fax[0].value}
                                            </p>
                                          </React.Fragment>
                                        )
                                      })} */}
                                    </div>
                                  </div>
                                }
                              </React.Fragment>
                            )
                          })}
                        </>
                      }
                      </>
                    }
                  </div>

                  <div className="col">
                    {/* Location
                    {purchaseOrder.field_purchase_order_ship_to &&
                      <>
                      {purchaseOrder.field_purchase_order_ship_to.length > 0 &&
                        <>
                          {purchaseOrder.field_purchase_order_ship_to.map((locationItem) => {
                            const location = this.props.getLocationByID(locationItem.target_id);
                            return (
                              <React.Fragment key={locationItem.target_id}>
                                {location &&
                                  <div className="card" style={{minHeight: '180px'}}>
                                    <div className="card-body">
                                      <p  className="mb-0">Ship To:</p>
                                      <h5>{location.title[0].value}</h5>
                                      <p dangerouslySetInnerHTML={{__html: location.body[0].processed}}></p>
                                      <p>Prepared By: {userName} {isOwner(purchaseOrder.uid[0].target_id) ? `(You)` : ``}</p>
                                    </div>
                                  </div>
                                }
                              </React.Fragment>
                            )
                          })}
                        </>
                      }
                      </>
                    } */}
                    <div className="card" style={{minHeight: '180px'}}>
                      <div className="card-body">
                        <p className="mb-0 font-weight-bold">Ship To:</p>
                        <h5 className="text-uppercase"><strong>Linksports, Inc.</strong></h5>
                        <p>
                          105 New Society St., Rosario Subd.-4<br/>
                          Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                          Philippines<br/><br/>

                          Contact Name:	<strong>Jojo Eugenio</strong><br/>
                          +63917 5207606<br/>
                          jojo@linksportsinc.com
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                {/* ITEMS */}
                <div className="row">
                  <div className="col">
                    <table className="table table-smd table-striped table-hover border-1">
                      <thead className="thead-dark">
                        <tr>
                          <th className="text-center text-uppercase font-weight-bold">Item</th>
                          <th className="text-uppercase font-weight-bold">Model</th>
                          <th className="text-uppercase font-weight-bold">Qty</th>
                          <th width="40%" className="text-uppercase font-weight-bold">Description</th>
                          <th className="text-right text-uppercase font-weight-bold">Unit Price</th>
                          <th className="text-right text-uppercase font-weight-bold">Amount</th>
                        </tr>
                      </thead>
                      {purchaseOrder.field_purchase_order_items &&
                        <>
                        {purchaseOrder.field_purchase_order_items.length > 0 &&
                          <tbody>
                            {purchaseOrder.field_purchase_order_items.map((itemItem, id) => {
                              const item = this.props.getItemByID(itemItem.target_id);
                              const supplier = this.props.getSupplierByID(item?.field_item_supplier?.[0].target_id);
                              if(item){

                                const itemTotal = parseFloat(parseFloat(item.field_unit_price[0].value) * parseFloat(itemItem.quantity)).toFixed(2);
                                total += parseFloat(itemTotal);

                                return (
                                  <React.Fragment key={itemItem.target_id}>
                                    <tr>
                                      <td className="text-center">{id + 1}</td>
                                      <td></td>
                                      <td>{itemItem.quantity}</td>
                                      <td>{item.title[0].value}</td>
                                      <td className="text-right">{numeral(item.field_unit_price[0].value).format('0,0.00')}</td>
                                      <td className="text-right" width="150">{numeral(itemTotal).format('0,0.00')}</td>
                                    </tr>
                                  </React.Fragment>
                                )
                              }
                            })}
                            <tr className="border-bottom">
                              <td colSpan={6} className="text-center">
                                <strong>---------- NOTHING FOLLOWS ----------</strong>
                              </td>
                            </tr>
                          </tbody>
                        }
                        </>
                      }
                    </table>
                  </div>
                </div>

                {/* <div style={{ pageBreakAfter: 'always' }} /> */}

                <div className="d-none d-print-block" style={{ height: `${verticalSpace}rem` }}>
                </div>

                <div className="row">
                  <div className="col">
                    <table className="table table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="border-top-0">
                            <p className="">
                              <strong>Instructions:</strong><br/>
                              <p dangerouslySetInnerHTML={generateInstruction(purchaseOrder.field_purchase_order_instruction[0]?.value)}></p>
                            </p>
                            <p className="font-weight-bold">
                              <br/>
                              _________________________________<br/>
                              Supplier Name / Signature
                            </p>
                            {purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.APPROVED &&
                              <p>
                                <br/><br/>
                                <strong>Remarks</strong><br/><br/><br/>
                                <em>Electronically approved, signature not required</em><br/><br/>
                                <strong>Approved By:</strong><br/>
                                JLE
                              </p>
                            }

                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div className="col">
                    <table className="table table-sm table-striped table-hover border-1">
                      <tfoot>
                        <tr>
                          <td className="text-right bg-dark text-white">Ex-Works Value-Sub-Total</td>
                          <td className="text-right"  width="50%">{numeral(parseFloat(total)).format('0,0.00')}</td>
                        </tr>
                        {purchaseOrder.field_purchase_order_currency[0].value === CURRENCY.PHP &&
                          (
                            <>
                              <tr>
                                <td className="text-right bg-dark text-white">VAT</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_order_vat[0].value)).format('0,0.00')}</td>
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">EWT</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_order_ewt[0].value)).format('0,0.00')}</td>
                              </tr>
                            </>
                          )
                        }
                        {purchaseOrder.field_purchase_order_currency[0].value != CURRENCY.PHP &&
                          (
                            <>
                              <tr>
                                <td className="text-right bg-dark text-white">Shipping</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_order_shipping_co[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">Insurance</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_order_insurance[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              {/* <tr>
                                <td className="text-right bg-dark text-white">Ex-Works Value - Sub-Total</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_ewv[0].value)).format('0,0.00')}</td>
                              </tr> */}
                              <tr>
                                <td className="text-right bg-dark text-white">Certificate of Conformity</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_coc_p[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">Handling Pack SOLAS</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_hps_p[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">Photo/HT Certificate</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.photo_ht_certificate_p[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">Freight</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_freight_p[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">Bank and or Courier Cost</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.bank_and_or_courier_cost_p[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">Documentation Fee</td>
                                <td className="text-right">{numeral(parseFloat(purchaseOrder.field_purchase_doc_p[0].value)).format('0,0.00')}</td>
                                {}
                              </tr>
                            </>
                          )
                        }
                        {(() => {
                          if(purchaseOrder.field_purchase_order_currency[0].value === CURRENCY.PHP) {
                            total = total + parseFloat(purchaseOrder.field_purchase_order_vat[0].value);
                            total = total - parseFloat(purchaseOrder.field_purchase_order_ewt[0].value);
                          }

                          if(purchaseOrder.field_purchase_order_currency[0].value != CURRENCY.PHP) {
                            total = total + parseFloat(purchaseOrder.field_purchase_order_shipping_co[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_order_insurance[0].value);

                            // total = total + parseFloat(purchaseOrder.field_purchase_ewv[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_coc_p[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_hps_p[0].value);
                            total = total + parseFloat(purchaseOrder.photo_ht_certificate_p[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_freight_p[0].value);
                            total = total + parseFloat(purchaseOrder.bank_and_or_courier_cost_p[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_doc_p[0].value);
                          }
                        })()}
                        <tr>
                          <td className="text-right bg-dark text-white pt-3 pb-3">
                            <h5 className="font-weight-bold">{purchaseOrder.field_purchase_order_currency[0].value === CURRENCY.PHP ? 'Grand Total' : 'TOTAL CIF-Manila'}</h5>
                          </td>
                          <td className="text-right pt-3 pb-3"><h5>{getCurrencyDisplay(purchaseOrder.field_purchase_order_currency[0].value)} {numeral(parseFloat(total)).format('0,0.00')}</h5></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>

                <div className="row mt-4">
                  <div className="col text-right">
                    {/* <span className="d-inline-block mr-3 text-success">www.linksportsinc.com</span> | <span className="d-inline-block ml-3">jeugenio@linksportsinc.com</span> */}
                  </div>
                </div>

              </div>

            </div>

            <Modal isOpen={printModal} size={'lg'} toggle={() => { this.setState({printModal: !printModal})}}>
              <ModalHeader toggle={() => { this.setState({printModal: !printModal})}}>Print Settings</ModalHeader>
              <ModalBody>
                <h5 className="mb-3">
                  These settings are only viewed on printing PDF
                </h5>
                <div className="form-group">
                  <label htmlFor="verticalSpace">Vertical Space</label>
                  <input id="verticalSpace" type="number" className="form-control form-control-lg" value={verticalSpace} placeholder="Vertical Space" min="0" onChange={(e) => { this.setState({verticalSpace: e.target.value}) }}></input>
                </div>
                <div className="form-group">
                  <label>Manual Pages</label>
                  <div className="row">
                    <div className="col col-4 font-weight-bold">Text</div>
                    <div className="col col-3 font-weight-bold">Left</div>
                    <div className="col col-3 font-weight-bold">Top</div>
                    <div className="col col-2 font-weight-bold">Remove</div>
                  </div>
                  {pages.map((page, i) => (
                    <div className="row mb-1" key={`page-${i}`}>
                      <div className="col col-4">{page.text}</div>
                      <div className="col col-3">{page.left}</div>
                      <div className="col col-3">{page.top}</div>
                      <div className="col col-2"><button type="button" className="btn btn-sm btn-danger text-center" style={{ width: '30px' }} onClick={(e) => { this.removePage(i) }}><i className="fa fa-times" /></button></div>
                    </div>
                  ))}
                  <hr/>
                  <form onSubmit={(e) => { this.addPage(e); }}>
                    <div className="row">
                      <div className="col col-4">
                        <input id="text" type="text" className="form-control form-control-sm" placeholder="Please enter paging text" defaultValue="Page 1 of 1"></input>
                      </div>
                      <div className="col col-3">
                        <input id="left" type="number" min="0" className="form-control form-control-sm" placeholder="Please enter left position" defaultValue="0"></input>
                      </div>
                      <div className="col col-3">
                        <input id="top" type="number" min="0" className="form-control form-control-sm" placeholder="Please enter right position" defaultValue="0"></input>
                      </div>
                      <div className="col col-2">
                        <button type="submit" className="btn btn-sm btn-outline-success text-center" style={{ width: '30px' }}><i className="fa fa-plus" /></button>
                      </div>
                    </div>
                  </form>
                </div>
              </ModalBody>
              <ModalFooter>
                <Button color="success" size={'lg'} onClick={() => { this.setState({printModal: !printModal})}}>Okay</Button>
              </ModalFooter>
            </Modal>

          </div>

        </>
      );
    }

    else {
      return (
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.PURCHASEORDERS}>Purchase Orders</NavLink></li>
            <li className="breadcrumb-item font-weight-bold text-uppercase active">{purchaseOrder.title ? purchaseOrder.title[0].value : ``}</li>
          </ol>
        </nav>
      )
    }

  }
}

const mapStateToProps = (state) => {
  return {
    isDeletingPurchaseOrder: state.purchaseorders.isDeletingPurchaseOrder
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getPurchaseOrders,
    getPurchaseOrder,
    getSupplierByID,
    getContactByID,
    getLocationByID,
    getItemByID,
    getUserByID,
    deletePurchaseOrder
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(PurchaseOrder));
