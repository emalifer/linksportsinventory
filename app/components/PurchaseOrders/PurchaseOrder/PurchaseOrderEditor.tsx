import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import { Typeahead } from 'react-bootstrap-typeahead';
import DatePicker from 'react-datepicker';
import { ToastContainer, toast } from 'react-toastify';

import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import terms from '../../../constants/terms.json';
import shipments from '../../../constants/shipment.json';
import deliveries from '../../../constants/delivery.json';
import { getPurchaseOrders, getPurchaseOrder, createOrUpdatePurchaseOrder, getNextPurchaseOrderNumber, getPurchaseOrderBadgeStatus, deletePurchaseOrder, STATUS as PURCHASEORDERSTATUS, CURRENCY, getCurrencyDisplay } from '../../../actions/purchaseorders';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getSupplierByID } from '../../../actions/suppliers';
import { getContactByID } from '../../../actions/contacts';
import { getLocationByID } from '../../../actions/locations';
import { getItemByID } from '../../../actions/items';
import { getUserByID } from '../../../actions/users';
import { estore } from '../../../reducers/index';

const MODE = {
  CREATE: 'create',
  UPDATE: 'update'
}

const instruction = `
1. Indicate Purchase Number on all copies of your invoice and delivery receipts. <br/>
2. Instruct your delivery man to leave the original and duplicate copies of your invoice with our receiving personnel to facilitate payment of your account; otherwise, sent the original to our Accounting Office. Payment will be paid by the Accounting Office upon receipt of original invoice. <br/>
3. The purchasers reserves the right to reject any of all materials delivered under this order which do not conform to the description shown above or specification furnished
`;

class PurchaseOrderEditor extends Component {

  constructor (props) {
    super(props);
    this.titleInput = React.createRef();
    this.supplierInput = React.createRef();
    this.locationInput = React.createRef();
    this.itemsTypeahead = React.createRef();
  }

  state = {
    mode: '',
    purchaseOrder: {
      type: [{target_id: "purchase_order"}],
      title: [{value: '', error: false}],
      field_purchase_order_date: [{value: new Date(), error: false}],
      field_purchase_order_terms: [{value: terms["30DAYS"]}],
      field_purchase_order_shipping: [{value: shipments["30DAYS"]}],
      field_purchase_order_delivery: [{value: deliveries.INLAND}],
      field_purchase_order_supplier: [{target_id: 0, error: false}],
      field_purchase_order_ship_to: [{target_id: 0, error: false}],
      field_purchase_order_items: [],
      field_purchase_order_vat: [{value: 0}],
      field_purchase_order_ewt: [{value: 0}],
      field_purchase_order_shipping_co: [{value: 0}],
      field_purchase_order_insurance: [{value: 0}],
      field_purchase_order_status: [{value: PURCHASEORDERSTATUS.PENDING}],
      field_purchase_ewv: [{value: 0}],
      field_purchase_coc_p: [{value: 0}],
      field_purchase_hps_p: [{value: 0}],
      photo_ht_certificate_p: [{value: 0}],
      field_purchase_freight_p: [{value: 0}],
      bank_and_or_courier_cost_p: [{value: 0}],
      field_purchase_doc_p: [{value: 0}],
      field_purchase_order_currency: [{value: CURRENCY.USD}],
      field_purchase_order_instruction: [{value: instruction}],
    },
    tempTarget: [],
    tempQuantity: 0,
    tempItemError: false,
    suppliers: [],
    locations: [],
    items: []
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getPurchaseOrder(this.props.match.params.id, (purchaseOrder) => {
        console.log(purchaseOrder);
        this.setState(
          produce(this.state, draft => {
            draft.purchaseOrder = purchaseOrder;
            if(purchaseOrder.field_purchase_order_terms.length === 0) {
              draft.purchaseOrder.field_purchase_order_terms = [{value: ''}]
            }
            if(purchaseOrder.field_purchase_order_shipping.length === 0) {
              draft.purchaseOrder.field_purchase_order_shipping = [{value: ''}]
            }
            if(purchaseOrder.field_purchase_order_delivery.length === 0) {
              draft.purchaseOrder.field_purchase_order_delivery = [{value: ''}]
            }
            draft.purchaseOrder.field_purchase_order_date[0].value = moment(purchaseOrder.field_purchase_order_date[0].value).toDate()
            draft.mode = MODE.UPDATE;
          })
        );
      })
    } else {
      this.props.getNextPurchaseOrderNumber((nextPurchaseOrderNumber) => {
        this.setState(produce(this.state, draft => {
          draft.purchaseOrder.title[0].value = nextPurchaseOrderNumber;
          draft.mode = MODE.CREATE;
        }))
      });
    }

    this.mapSuppliers();
    this.mapLocations();
    this.mapItems();

  }

  componentDidUpdate = (prevProps) => {
    if(prevProps.suppliers !== this.props.suppliers) {
      this.mapSuppliers()
    }
    if(prevProps.locations !== this.props.locations) {
      this.mapLocations()
    }
    if(prevProps.items !== this.props.items) {
      this.mapItems()
    }
  }

  mapSuppliers = () => {
    const suppliers = this.props.suppliers.map((supplier) => {
      return {
        id: supplier.nid[0].value,
        label: supplier.title[0].value
      }
    });
    this.setState({suppliers});
  }

  mapLocations = () => {
    const locations = this.props.locations.map((location) => {
      return {
        id: location.nid[0].value,
        label: location.title[0].value
      }
    });
    this.setState({locations});
  }

  mapItems = () => {
    const items = this.props.items.map((item) => {
      return {
        id: item.nid[0].value,
        label: item.title[0].value,
        field_unit_price: item.field_unit_price[0].value
      }
    });
    this.setState({items});
  }

  changeTitle = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.title[0].value = e.target.value;
      draft.purchaseOrder.title[0].error = false;
    }));
  }

  changeDate = (date) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_date[0].value = date,
      draft.purchaseOrder.field_purchase_order_date[0].error = false;
    }));
  }

  changeTerms = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_terms[0].value = e.target.value
    }));
  }

  changeShipping = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_shipping[0].value = e.target.value
    }));
  }

  changeDelivery = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_delivery[0].value = e.target.value
    }));
  }

  changeSupplier = (suppliers) => {
    if(suppliers.length > 0){
      this.setState(produce(this.state, draft => {
        draft.purchaseOrder.field_purchase_order_supplier[0] = {};
        draft.purchaseOrder.field_purchase_order_supplier[0].target_id = suppliers[0].id;
        draft.purchaseOrder.field_purchase_order_supplier[0].error = false;
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.purchaseOrder.field_purchase_order_supplier[0] = {};
        draft.purchaseOrder.field_purchase_order_supplier[0].target_id = 0
      }));
    }
  }

  changeLocation = (locations) => {
    if(locations.length > 0){
      this.setState(produce(this.state, draft => {
        draft.purchaseOrder.field_purchase_order_ship_to[0] = {};
        draft.purchaseOrder.field_purchase_order_ship_to[0].target_id = locations[0].id;
        draft.purchaseOrder.field_purchase_order_ship_to[0].error = false;
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.purchaseOrder.field_purchase_order_ship_to[0] = {};
        draft.purchaseOrder.field_purchase_order_ship_to[0].target_id = 0
      }));
    }
  }

  changeTempQuantity = (e) => {
    this.setState(produce(this.state, draft => {
      draft.tempQuantity = e.target.value
    }));
  }

  changeTemp = (items) => {
    if(items.length > 0){
      this.setState(produce(this.state, draft => {
        draft.tempTarget = items
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.tempTarget = []
      }));
    }
  }

  addItem = () => {
    if(this.state.tempQuantity > 0 && this.state.tempTarget.length > 0) {
      this.setState(produce(this.state, draft => {
        draft.purchaseOrder.field_purchase_order_items.push(
          {
            target_id: draft.tempTarget[0].id,
            quantity: numeral(draft.tempQuantity).format('0')
          }
        );
        draft.tempQuantity = 0;
        draft.tempTarget = [];
        draft.tempItemError = false;
      }));
      this.itemsTypeahead.getInstance().clear();
    }
  }

  removeItem = (id) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_items.splice(id, 1);
    }));
  }

  changeVAT = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_vat[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeEWT = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_ewt[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeShippingCost = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_shipping_co[0].value = e.target.value ? e.target.value : 0
    }));
  }

  changeInsurance = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_insurance[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeEW = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_ewv[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeCOC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_coc_p[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeHPS = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_hps_p[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changePHC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.photo_ht_certificate_p[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeFreight = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_freight_p[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeBCC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.bank_and_or_courier_cost_p[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeDOC = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_doc_p[0].value =  e.target.value ? e.target.value : 0
    }));
  }

  changeStatus = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_status[0].value = e.target.value;
    }));
  }

  changeCurrency = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_currency[0].value = e.target.value;
    }));
  }

  changeInstruction = (e) => {
    this.setState(produce(this.state, draft => {
      draft.purchaseOrder.field_purchase_order_instruction[0].value = e.target.value;
    }));
  }

  validatePurchaseOrder = () => {
    let flag = true;
    const { purchaseOrder } = this.state;

    console.log(purchaseOrder.field_purchase_order_date[0].value);

    this.setState(produce(this.state, draft => {
      if(purchaseOrder.title[0].value.length === 0) {
        flag = false;
        draft.purchaseOrder.title[0].error = true;
        toast.error("Please enter a PO Number");
      }
      if(!purchaseOrder.field_purchase_order_date[0].value) {
        flag = false;
        draft.purchaseOrder.field_purchase_order_date[0].error = true;
        toast.error("Please select a PO date");
      }
      if(purchaseOrder.field_purchase_order_supplier[0].target_id === 0) {
        flag = false;
        draft.purchaseOrder.field_purchase_order_supplier[0].error = true;
        toast.error("Please select a supplier/vendor");
      }
      // if(purchaseOrder.field_purchase_order_ship_to[0].target_id === 0) {
      //   flag = false;
      //   draft.purchaseOrder.field_purchase_order_ship_to[0].error = true;
      //   toast.error("Please select a location/ship-to");
      // }
      if(purchaseOrder.field_purchase_order_items.length === 0) {
        flag = false;
        draft.tempItemError = true;
        toast.error("Please add at least one item");
      }
    }))

    return flag;
  }

  savePurchaseOrder = (e) => {

    e.preventDefault();

    if(this.validatePurchaseOrder()) {
      let method = 'POST';

      switch(this.state.mode) {
        case MODE.CREATE:
          method = 'POST';
          break;
        case MODE.UPDATE:
          method = 'PATCH';
          break;
        default:
          break;
      }

      this.props.createOrUpdatePurchaseOrder(method, this.state.purchaseOrder, (resp) => {
        this.props.getPurchaseOrders(() => {
          this.props.history.push(`${routes.PURCHASEORDERS}/${resp.nid[0].value}`)
        });
      })
    }

  }

  render() {

    const { isSavingPurchaseOrder } = this.props;
    const { mode, purchaseOrder, suppliers, locations, items, tempQuantity, tempTarget, tempItemError }  = this.state;

    let total = 0;
    let userName = '';
    if(purchaseOrder.uid) {
      let user = this.props.getUserByID(purchaseOrder.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    } else {
      userName = estore.get('name');
    }

    return (
      <>
        <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop={false} closeOnClick rtl={false} pauseOnVisibilityChange draggable pauseOnHover />

        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.PURCHASEORDERS}>Purchase Orders</NavLink></li>
            {mode === MODE.CREATE &&
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{purchaseOrder.title[0].value ? purchaseOrder.title[0].value : `Create`}</li>
            }
            {mode === MODE.UPDATE &&
              <>
                <li className="breadcrumb-item font-weight-bold text-uppercase">
                  <NavLink className="text-success" to={`${routes.PURCHASEORDERS}/${purchaseOrder.nid[0].value ? purchaseOrder.nid[0].value : ``}`}>{purchaseOrder.title[0].value ? purchaseOrder.title[0].value : ``}</NavLink>
                </li>
                <li className="breadcrumb-item font-weight-bold text-uppercase active">Edit</li>
              </>
            }
          </ol>
        </nav>

        <form onSubmit={(e) => { this.savePurchaseOrder(e); }}>
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 className="h2 text-success font-weight-bold text-uppercase">
              <i className="fas fa-inbox mr-2"></i>
              {purchaseOrder.title[0].value ? purchaseOrder.title[0].value : `New Purchase Order`}
              <span className={`badge ${getPurchaseOrderBadgeStatus(purchaseOrder.field_purchase_order_status[0].value)} ml-2`}>{purchaseOrder.field_purchase_order_status[0].value}</span>
            </h1>
            {/* <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingPurchaseOrder}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingPurchaseOrder ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingPurchaseOrder ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div> */}
          </div>

          <div className="card mb-4">
            <div className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-3 mt-4">
                  <div className="col"><img src={logo} width="400" /></div>
                  <div className="col">
                    <p className="text-right font-italic font-weight-bold">"Your sports solutions partner"</p>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <p>
                      105 New Society St., Rosario Subd.-4<br/>
                      Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                      Philippines<br/><br/>
                      Tel # +63 2 75007559<br/>
                      Fax # +63 2 77486641<br/><br/>
                      Email: info@linksportsinc.com<br/>
                    </p>
                  </div>
                  <div className="col">
                    <table className="table table-bordered table-sm mb-0">
                      <thead>
                        <tr><th colSpan="2"><h3 className="mb-0 text-uppercase font-weight-bold">Purchase Order</h3></th></tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th>PO Number</th>
                          <td width="50%"><input type="number" className={`form-control form-control-sm ${purchaseOrder.title[0].error ? `is-invalid` : ``}`} disabled={isSavingPurchaseOrder} placeholder="PO Number" value={purchaseOrder.title[0].value} onChange={(e) => { this.changeTitle(e) }} /></td>
                        </tr>
                        <tr>
                          <th>Date</th>
                          <td>
                            <DatePicker className={`form-control form-control-sm ${purchaseOrder.field_purchase_order_date[0].error ? `is-invalid` : ``}`} disabled={isSavingPurchaseOrder} placeholderText="PO Date" selected={purchaseOrder.field_purchase_order_date[0].value} onChange={(date) => {this.changeDate(date)}} dateFormat="MMMM dd, yyyy" isClearable />
                            {/* <input type="date" className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="PO Date" value={moment(purchaseOrder.field_purchase_order_date[0].value).format('YYYY-MM-DD')} onChange={(e) => { this.changeDate(e) }}/> */}
                          </td>
                        </tr>
                        <tr>
                          <th>Terms</th>
                          <td>
                            {/* <input className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Terms" value={purchaseOrder.field_purchase_order_terms[0].value} onChange={(e) => { this.changeTerms(e) }}/> */}
                            <select className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Terms" value={purchaseOrder.field_purchase_order_terms[0].value} onChange={(e) => { this.changeTerms(e) }}>
                              {Object.keys(terms).map((term, index) => {
                                return (
                                  <option value={term} key={`terms-${index}`}>
                                    {terms[term]}
                                  </option>
                                )
                              })}
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Shipment</th>
                          <td>
                            {/* <input className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Shipping" value={purchaseOrder.field_purchase_order_shipping[0].value} onChange={(e) => { this.changeShipping(e) }} /> */}
                            <select className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Shipment" value={purchaseOrder.field_purchase_order_shipping[0].value} onChange={(e) => { this.changeShipping(e) }}>
                              {Object.keys(shipments).map((shipment, index) => {
                                return (
                                  <option value={shipment} key={`shipment-${index}`}>
                                    {shipments[shipment]}
                                  </option>
                                )
                              })}
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <th>Delivery</th>
                          <td>
                            {/* <input className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Delivery" value={purchaseOrder.field_purchase_order_delivery[0].value} onChange={(e) => { this.changeDelivery(e) }} /> */}
                            <select className="form-control form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Delivery" value={purchaseOrder.field_purchase_order_delivery[0].value} onChange={(e) => { this.changeDelivery(e) }}>
                              {Object.keys(deliveries).map((delivery, index) => {
                                return (
                                  <option value={delivery} key={`delivery-${index}`}>
                                    {deliveries[delivery]}
                                  </option>
                                )
                              })}
                            </select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    <div className="card" style={{minHeight: '230px'}}>
                      <div className="card-body">
                        <div className="mb-3">
                          <Typeahead id="supplier-typeahead" isInvalid={purchaseOrder.field_purchase_order_supplier[0].error} placeholder="Search suppliers" onChange={(selected) => { this.changeSupplier(selected); }} options={suppliers} clearButton={true} disabled={isSavingPurchaseOrder} />
                        </div>
                        {/* Supplier */}
                        {purchaseOrder.field_purchase_order_supplier &&
                          <>
                          {purchaseOrder.field_purchase_order_supplier.length > 0 &&
                            <>
                              {purchaseOrder.field_purchase_order_supplier.map((supplierItem) => {
                                const supplier = this.props.getSupplierByID(supplierItem.target_id);
                                return (
                                  <React.Fragment key={supplierItem.target_id}>
                                    {supplier &&
                                      <>
                                        <p className="mb-0">Vendor ID: {supplier.field_supplier_code[0].value}</p>
                                        <h5 className="text-uppercase">{supplier.title[0].value}</h5>
                                        <p>
                                          {supplier.field_supplier_address_street[0]?.value},&nbsp;
                                          {supplier.field_supplier_address_city[0]?.value},&nbsp;
                                          {supplier.field_supplier_address_state[0]?.value},&nbsp;
                                        {supplier.field_supplier_address_zip[0]?.value},&nbsp;
                                          {supplier.field_supplier_address_country[0]?.value}
                                        </p>

                                        <p>
                                          {supplier.supplier_contact_name?.[0]?.value}<br/>
                                          {supplier.field_supplier_telephone[0]?.value}<br/>
                                          {supplier.field_supplier_email_address[0]?.value}
                                        </p>

                                        {/* {supplier.field_supplier_contact.map((contactItem, id) => {
                                          const contact = this.props.getContactByID(contactItem.target_id);
                                          return (
                                            <React.Fragment key={contactItem.target_id}>
                                              <p className="mb-0">
                                                {contact.title[0].value},&nbsp;
                                                {contact.field_contact_designation[0].value}
                                              </p>
                                              <p className="mb-0">
                                                Tel: {contact.field_contact_telephone[0].value},&nbsp;
                                                Fax: {contact.field_contact_fax[0].value}
                                              </p>
                                            </React.Fragment>
                                          )
                                        })} */}
                                      </>
                                    }
                                  </React.Fragment>
                                )
                              })}
                            </>
                          }
                          </>
                        }
                      </div>
                    </div>
                  </div>

                  <div className="col">
                    <div className="card" style={{minHeight: '230px'}}>
                      <div className="card-body">
                        {/* <div className="mb-3">
                          <Typeahead id="locations-typeahead" isInvalid={purchaseOrder.field_purchase_order_ship_to[0].error} placeholder="Search location" onChange={(selected) => { this.changeLocation(selected); }} options={locations} clearButton={true} disabled={isSavingPurchaseOrder} />
                        </div> */}
                        {/* Location */}
                        {/* {purchaseOrder.field_purchase_order_ship_to &&
                          <>
                          {purchaseOrder.field_purchase_order_ship_to.length > 0 &&
                            <>
                              {purchaseOrder.field_purchase_order_ship_to.map((locationItem) => {
                                const location = this.props.getLocationByID(locationItem.target_id);
                                return (
                                  <React.Fragment key={locationItem.target_id}>
                                    {location &&
                                      <>
                                        <p  className="mb-0">Ship To:</p>
                                        <h5>{location.title[0].value}</h5>
                                        <p dangerouslySetInnerHTML={{__html: location.body[0].processed}}></p>
                                      </>
                                    }
                                  </React.Fragment>
                                )
                              })}
                            </>
                          }
                          </>
                        } */}
                        <p className="mb-0 font-weight-bold">Ship To:</p>
                        <h5 className="text-uppercase"><strong>Linksports, Inc.</strong></h5>
                        <p>
                          105 New Society St., Rosario Subd.-4<br/>
                          Brgy. Sta. Lucia, Ortigas Ext., Pasig City. 1608<br/>
                          Philippines<br/><br/>

                          Contact Name:	<strong>Jojo Eugenio</strong><br/>
                          +63917 5207606<br/>
                          jojo@linksportsinc.com
                        </p>
                        {/* <p>Prepared By: {userName}</p> */}
                      </div>
                    </div>
                  </div>
                </div>

                {/* ITEMS */}
                <div className="row">
                  <div className="col">
                    <table className="table table-smd table-striped table-hover">
                      <thead className="thead-dark">
                        <tr>
                          <th className="text-center"></th>
                          <th className="text-center text-uppercase font-weight-bold">Item</th>
                          <th className="text-uppercase font-weight-bold">Model</th>
                          <th width="12%" className="text-uppercase font-weight-bold">Qty</th>
                          <th width="25%" className="text-uppercase font-weight-bold">Description</th>
                          <th width="15%" className="text-right text-uppercase font-weight-bold">Unit Price</th>
                          <th width="25%" className="text-right text-uppercase font-weight-bold">Amount</th>
                        </tr>
                      </thead>
                      {purchaseOrder.field_purchase_order_items &&
                        <>
                        {purchaseOrder.field_purchase_order_items.length > 0 &&
                          <tbody>
                            {purchaseOrder.field_purchase_order_items.map((itemItem, id) => {
                              const item = this.props.getItemByID(itemItem.target_id);
                              const supplier = this.props.getSupplierByID(item?.field_item_supplier?.[0].target_id);
                              if(item){

                                const itemTotal = parseFloat(parseFloat(item.field_unit_price[0].value) * parseFloat(itemItem.quantity)).toFixed(2);
                                total += parseFloat(itemTotal);

                                return (
                                  <React.Fragment key={`${id}-${itemItem.target_id}`}>
                                    <tr>
                                      <td className="text-center">
                                        <button className="btn btn-danger btn-sm rounded-circle text-center" onClick={() => { this.removeItem(id); }} style={{width: '30px', height: '30px'}}><i className="fas fa-times"></i></button>
                                      </td>
                                      <td className="text-center">{id + 1}</td>
                                      <td></td>
                                      <td>{numeral(itemItem.quantity).format('0,0')}</td>
                                      <td>{item.title[0].value}</td>
                                      <td className="text-right">
                                        {id === 0 &&
                                          <select className="form-control d-inline mr-2" style={{width: '80px'}} value={purchaseOrder.field_purchase_order_currency[0].value} onChange={(e) => { this.changeCurrency(e) }}>
                                            {Object.keys(CURRENCY).map((key, id) => {
                                              if(key === 'APPROVED') {
                                                if(isAdmin()) {
                                                  return <option key={id} value={CURRENCY[key]}>{key}</option>;
                                                }
                                              } else {
                                                return <option key={id} value={CURRENCY[key]}>{key}</option>;
                                              }
                                            })}
                                          </select>
                                        }
                                        {numeral(item.field_unit_price[0].value).format('0,0.00')}
                                      </td>
                                      <td className="text-right">{numeral(itemTotal).format('0,0.00')}</td>
                                    </tr>
                                  </React.Fragment>
                                )
                              }
                            })}
                          </tbody>
                        }
                        </>
                      }
                      <tbody>
                        <tr className={`${tempItemError ? `table-danger` :``}`}>
                          <td className="text-center">
                            <button className="btn btn-success btn-sm rounded-circle text-center" disabled={!tempQuantity || tempTarget.length === 0} onClick={() => { this.addItem(); }} style={{width: '30px', height: '30px'}}><i className="fas fa-plus"></i></button>
                          </td>
                          <td></td>
                          <td></td>
                          <td><input className="form-control" type="number" disabled={isSavingPurchaseOrder} placeholder="Qty" value={tempQuantity} min="0" onChange={(e) => { this.changeTempQuantity(e) }} /></td>
                          <td>
                            <Typeahead id="items-typeahead" className="" placeholder="Search Item" onChange={(selected) => { this.changeTemp(selected); }} options={items} clearButton={true} ref={(typeahead) => this.itemsTypeahead = typeahead} disabled={isSavingPurchaseOrder} />
                          </td>
                          <td className="text-right">
                            {tempTarget.length > 0 &&
                              <>{numeral(tempTarget[0].field_unit_price).format('0,0.00')}</>
                            }
                            {(() => {
                              if(tempQuantity && tempTarget.length > 0) {
                                total = total + parseFloat(tempTarget[0].field_unit_price) * parseFloat(tempQuantity);
                              }
                            })()}
                          </td>
                          <td className="text-right">
                            {(() => {
                              if(tempQuantity && tempTarget.length > 0) {
                                return numeral(parseFloat(total)).format('0,0.00');
                              }
                            })()}
                          </td>
                        </tr>
                        <tr className="border-bottom">
                          <td colSpan={7} className="text-center">
                            <strong>---------- NOTHING FOLLOWS ----------</strong>
                          </td>
                        </tr>
                      </tbody>

                    </table>
                  </div>
                </div>

                {/* <div style={{ pageBreakAfter: 'always' }} /> */}

                <div className="row">
                  <div className="col">
                    <table className="table table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="border-top-0">
                            <p className="">
                              <strong>Instructions:</strong><br/>
                              <textarea className="form-control" rows={10} value={purchaseOrder.field_purchase_order_instruction[0].value} onChange={(e) => { this.changeInstruction(e) }}>
                              </textarea>
                              <span className="font-italic">Add {`<br/>`} for newline</span>
                              <br/>
                            </p>
                            <p className="font-weight-bold">
                              <br/>
                              _________________________________<br/>
                              Supplier Name / Signature
                            </p>
                            {(() => {
                              if(purchaseOrder.nid){
                                if(isAdmin() || isOwner(purchaseOrder.uid[0].target_id)) {
                                  return (
                                    <div className="form-group">
                                      <label>Status</label>
                                      <select className="form-control col-5" value={purchaseOrder.field_purchase_order_status[0].value} onChange={(e) => { this.changeStatus(e) }}>
                                        {Object.keys(PURCHASEORDERSTATUS).map((key, id) => {
                                          if(key === 'APPROVED') {
                                            if(isAdmin()) {
                                              return <option key={id} value={PURCHASEORDERSTATUS[key]}>{key}</option>;
                                            }
                                          } else {
                                            return <option key={id} value={PURCHASEORDERSTATUS[key]}>{key}</option>;
                                          }
                                        })}
                                      </select>
                                    </div>
                                  );
                                }
                              }
                            })()}
                            {purchaseOrder.field_purchase_order_status[0].value === PURCHASEORDERSTATUS.APPROVED &&
                              <p>
                                <strong>Remarks</strong><br/><br/><br/>
                                <em>Electronically approved, signature not required</em><br/><br/>
                                <strong>Approved By:</strong><br/>
                                JLE
                              </p>
                            }

                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div className="col">
                    <table className="table table-sm table-striped table-hover">
                      <tfoot>
                        <tr>
                          <td className="text-right bg-dark text-white">Ex-Works Value-Sub-Total</td>
                          <td className="text-right"  width="50%">{numeral(parseFloat(total)).format('0,0.00')}</td>
                        </tr>
                        {purchaseOrder.field_purchase_order_currency[0].value === CURRENCY.PHP &&
                          (
                            <>
                              <tr>
                                <td className="text-right bg-dark text-white">VAT</td>
                                <td className="text-right">
                                  <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="VAT" min="0" value={purchaseOrder.field_purchase_order_vat[0].value}  onChange={(e) => { this.changeVAT(e) }} />
                                </td>
                              </tr>
                              <tr>
                                <td className="text-right bg-dark text-white">EWT</td>
                                <td className="text-right">
                                  <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="VAT" min="0" value={purchaseOrder.field_purchase_order_ewt[0].value}  onChange={(e) => { this.changeEWT(e) }} />
                                </td>
                              </tr>
                            </>
                          )
                        }
                        {purchaseOrder.field_purchase_order_currency[0].value != CURRENCY.PHP &&
                          (
                            <>
                            <tr>
                              <td className="text-right bg-dark text-white">Shipping</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Shipping" min="0" value={purchaseOrder.field_purchase_order_shipping_co[0].value}  onChange={(e) => { this.changeShippingCost(e) }} />
                              </td>
                            </tr>
                            <tr>
                              <td className="text-right bg-dark text-white">Insurance</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Insurance" min="0" value={purchaseOrder.field_purchase_order_insurance[0].value}  onChange={(e) => { this.changeInsurance(e) }} />
                              </td>
                            </tr>
                            {/* <tr>
                              <td className="text-right bg-dark text-white">Ex-Works Value - Sub-Total</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Ex-Works Value - Sub-Total" min="0" value={purchaseOrder.field_purchase_ewv[0].value}  onChange={(e) => { this.changeEW(e) }} />
                              </td>
                            </tr> */}
                            <tr>
                              <td className="text-right bg-dark text-white">Certificate of Conformity</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Certificate of Conformity" min="0" value={purchaseOrder.field_purchase_coc_p[0].value}  onChange={(e) => { this.changeCOC(e) }} />
                              </td>
                            </tr>
                            <tr>
                              <td className="text-right bg-dark text-white">Handling Pack SOLAS</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Handling Pack SOLAS" min="0" value={purchaseOrder.field_purchase_hps_p[0].value}  onChange={(e) => { this.changeHPS(e) }} />
                              </td>
                            </tr>
                            <tr>
                              <td className="text-right bg-dark text-white">Photo/HT Certificate</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Photo/HT Certificate" min="0" value={purchaseOrder.photo_ht_certificate_p[0].value}  onChange={(e) => { this.changePHC(e) }} />
                              </td>
                            </tr>
                            <tr>
                              <td className="text-right bg-dark text-white">Freight</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Freight" min="0" value={purchaseOrder.field_purchase_freight_p[0].value}  onChange={(e) => { this.changeFreight(e) }} />
                              </td>
                            </tr>
                            <tr>
                              <td className="text-right bg-dark text-white">Bank and or Courier Cost</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Bank and or Courier Cost" min="0" value={purchaseOrder.bank_and_or_courier_cost_p[0].value}  onChange={(e) => { this.changeBCC(e) }} />
                              </td>
                            </tr>
                            <tr>
                              <td className="text-right bg-dark text-white">Documentation Fee</td>
                              <td className="text-right">
                                <input type="number" step="0.01" className="form-control text-right form-control-sm" disabled={isSavingPurchaseOrder} placeholder="Documentation Fee" min="0" value={purchaseOrder.field_purchase_doc_p[0].value}  onChange={(e) => { this.changeDOC(e) }} />
                              </td>
                            </tr>
                          </>
                          )
                        }
                        {(() => {

                          if(purchaseOrder.field_purchase_order_currency[0].value === CURRENCY.PHP) {
                            total = total + parseFloat(purchaseOrder.field_purchase_order_vat[0].value);
                            total = total - parseFloat(purchaseOrder.field_purchase_order_ewt[0].value);
                          }

                          if(purchaseOrder.field_purchase_order_currency[0].value != CURRENCY.PHP) {
                            total = total + parseFloat(purchaseOrder.field_purchase_order_shipping_co[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_order_insurance[0].value);

                            // total = total + parseFloat(purchaseOrder.field_purchase_ewv[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_coc_p[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_hps_p[0].value);
                            total = total + parseFloat(purchaseOrder.photo_ht_certificate_p[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_freight_p[0].value);
                            total = total + parseFloat(purchaseOrder.bank_and_or_courier_cost_p[0].value);
                            total = total + parseFloat(purchaseOrder.field_purchase_doc_p[0].value);
                          }

                        })()}
                        <tr>
                          <td className="text-right bg-dark text-white pt-3 pb-3">
                            <h5 className="font-weight-bold">{purchaseOrder.field_purchase_order_currency[0].value === CURRENCY.PHP ? 'Grand Total' : 'TOTAL CIF-Manila'}</h5>
                          </td>
                          <td className="text-right pt-3 pb-3">
                            <h5 className="mb-0">{getCurrencyDisplay(purchaseOrder.field_purchase_order_currency[0].value)} {numeral(parseFloat(total)).format('0,0.00')}</h5>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>

                <div className="row mt-4">
                  <div className="col text-right">
                    {/* <span className="d-inline-block mr-3 text-success">www.linksportsinc.com</span> | <span className="d-inline-block ml-3">jeugenio@linksportsinc.com</span> */}
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="mb-3">
            <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingPurchaseOrder}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingPurchaseOrder ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingPurchaseOrder ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div>
            <div className="clearfix"></div>
          </div>

        </form>

      </>
    );

  }
}

const mapStateToProps = (state) => {
  return {
    suppliers: state.suppliers.suppliers,
    locations: state.locations.locations,
    items: state.items.items,
    isSavingPurchaseOrder: state.purchaseorders.isSavingPurchaseOrder
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getUserByID,
    getPurchaseOrders,
    getPurchaseOrder,
    createOrUpdatePurchaseOrder,
    getSupplierByID,
    getContactByID,
    getLocationByID,
    getItemByID,
    getNextPurchaseOrderNumber,
    deletePurchaseOrder
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(PurchaseOrderEditor));
