// @flow
import React, { Component } from 'react';

import Navigation from '../Navigation/Navigation';

import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router';
import routes from '../../constants/routes.json';

import PurchaseOrdersList from './PurchaseOrdersList';
import PurchaseOrder from './PurchaseOrder/PurchaseOrder';
import PurchaseOrderEditor from './PurchaseOrder/PurchaseOrderEditor';

class PurchaseOrders extends Component {
  render() {

    const { location, match } = this.props;

    return (
      <Switch location={location}>
        <Route path={routes.PURCHASEORDERS} component={PurchaseOrdersList} exact />
        <Route path={`${routes.PURCHASEORDERS}/create`} component={PurchaseOrderEditor} exact />
        <Route path={`${routes.PURCHASEORDERS}/:id`} component={PurchaseOrder} exact />
        <Route path={`${routes.PURCHASEORDERS}/:id/edit`} component={PurchaseOrderEditor} exact />
      </Switch>
    );
  }
}

export default withRouter(PurchaseOrders);
