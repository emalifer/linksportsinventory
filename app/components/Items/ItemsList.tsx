import React, { Component } from 'react';
import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import produce from 'immer';

import routes from '../../constants/routes.json';
import { getItems, deleteItem } from '../../actions/items';
import { isAdmin, isOwner } from '../../actions/auth';
import { getUserByID } from '../../actions/users';
import Inventory from './Item/Inventory';
import sample from './Item/sample.png';

const { dialog } = require('electron').remote;

class ItemsList extends Component {

  constructor(props) {
    super(props);
  }

  state = {
    items: [],
    showItemInventory: false,
    filter: {
      enabled: false,
      title: [{value: ''}]
    }
  }

  componentDidMount = () => {

    this.setState({items: this.props.items}, () => {
      if(this.state.filter.enabled) {
        this.filterItems();
      }
    })
  }

  componentDidUpdate = (prevProps, prevState) => {

  }

  toggleShowItemInventory = () => {
    this.setState({showItemInventory: !this.state.showItemInventory});
  }

  showHideFilter = () => {
    this.setState(produce(this.state, draft => {
      draft.filter.enabled = !this.state.filter.enabled;
    }));
  }

  changeTitleFilter = (e) => {
    this.setState(produce(this.state, draft => {
      draft.filter.title[0].value = e.target.value;
    }));
  }

  clearFilter = () => {

    this.setState(produce(this.state, draft => {
      draft.filter.title = [{value: ''}];
    }), () => { this.filterItems(); });
  }

  filterItems = (e) => {
    if(e) {
      e.preventDefault();
    }

    const { filter } = this.state;
    let items = this.props.items;

    if(filter.title[0].value.length > 0) {
      items = items.filter((item) => {
        console.log(item.title[0].value, filter.title[0].value, item.title[0].value.match(filter.title[0].value) )
        return item.title[0].value.match(filter.title[0].value)
      });
    }

    this.setState({items});
  }

  confirmDeleteItem = (item) => {
    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete Item ${item.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteItem(item.nid[0].value, () => {
        this.props.getItems(() => {
        })
      })
    }

    // dialog.showMessageBox(
    //   {
    //     message: `Do you want to delete Item ${item.title[0].value}?`,
    //     buttons: ['Yes','No']
    //   },
    //   (response) => {
    //     if(response === 0) {
    //       this.props.deleteItem(item.nid[0].value, () => {
    //         this.props.getItems(() => {
    //         })
    //       })
    //     }
    //   }
    // );
  }

  render() {

    const { isGettingItems, deletingItemID } = this.props;
    const { items, filter, showItemInventory } = this.state;

    return (
      <>
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase active">Items</li>
          </ol>
        </nav>

        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
          <h1 className="h2 text-success font-weight-bold text-uppercase"><i className="fab fa-buffer mr-2"></i>Items</h1>
          <div className="float-right">
            <NavLink className="btn btn-outline-success btn-lg" to={`${routes.ITEMS}/create`}><i className="fas fa-pen mr-3"></i>Create</NavLink>
          </div>
        </div>

        <h4 className="float-left">Items List</h4>
        <ul className="nav nav-tabs float-right" style={{borderBottom: '0px'}}>
          <li className="nav-item">
            <a className={`nav-link cursor-pointer ${filter.enabled ? `active` : ``}`} onClick={() => { this.showHideFilter(); }}>
              <i className="fas fa-filter mr-2"></i>
              Filter
            </a>
          </li>
        </ul>
        <div className="clearfix"></div>

        <div className={`filter-box border-top border-left border-right p-3 ${filter.enabled ? `` : `d-none`}`}>
          <form onSubmit={(e) => { this.filterItems(e); }}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-8 pl-0">
                  <div className="form-group">
                    <label>Filter by Item Name</label>
                    <input className="form-control" placeholder="Item Names" value={filter.title[0].value} onChange={(e) => { this.changeTitleFilter(e) }} />
                  </div>
                </div>
                <div className="col-4 pr-0">
                  <div className="float-right">
                    <button onClick={() => { this.clearFilter(); }} className="btn btn-link mr-2 text-dark">Clear Filter</button>
                    <button type="submit" className="btn btn-outline-success"><i className="fas fa-filter mr-3"></i>Filter</button>
                  </div>
                </div>
              </div>

              <hr />

              <div className="row">
                <div className="col-8 pl-0">
                  <button className={`btn ${showItemInventory ? `btn-success` : `btn-outline-success`}`} onClick={() => { this.toggleShowItemInventory(); }}>Toggle Item Inventory</button>
                </div>
              </div>

            </div>
          </form>
        </div>

        <div className="table-responsive">
          <table className="table table-striped table-sm">
            <thead>
              <tr>
                <th colSpan="2">Item</th>
                <th width="115">Actions</th>
              </tr>
            </thead>
            <tbody>
              {isGettingItems &&
                <tr>
                  <td colSpan="3">
                    <em>Loading Items</em>
                  </td>
                </tr>
              }
              {!isGettingItems &&
                <>
                {items.length > 0 &&
                  <>
                    {items.map((item, idx) => {
                      return (
                        <React.Fragment key={item.nid[0].value}>
                        <tr>
                          <td width="80">
                            {(() => {
                              if(item.field_item_image.length > 0) {
                                return <img className="img-thumbnail ls-image-thumbnail border-0" src={item.field_item_image[0].url} />
                              } else {
                                return <img className="img-thumbnail ls-image-thumbnail border-0" src={sample} />
                              }
                            })()}
                          </td>
                          <td>
                            <p style={{fontSize: '1.2rem'}} className="mb-0">{item.title[0].value}</p>
                            {(() => {
                              if(item.uid) {
                                let user = this.props.getUserByID(item.uid[0].target_id);
                                if(user) {
                                  let userName = user.name[0].value;
                                  return `Prepared by ${userName} ${isOwner(item.uid[0].target_id) ? `(You)` : ``}`;
                                }
                              }
                            })()}
                          </td>
                          <td>
                            <NavLink className={`btn btn-outline-success btn-sm mr-1 ${deletingItemID === item.nid[0].value ? `disabled` : ``}`} to={`${routes.ITEMS}/${item.nid[0].value}`}>
                              <i className="fas fa-search"></i>
                            </NavLink>
                            {(() => {
                              let editable = false;

                              if(isAdmin() || isOwner(item.uid[0].target_id)) {
                                editable = true;
                              }

                              if(editable) {
                                return (
                                  <NavLink className={`btn btn-outline-dark btn-sm mr-1 ${deletingItemID === item.nid[0].value ? `disabled` : ``}`} to={`${routes.ITEMS}/${item.nid[0].value}/edit`}>
                                    <i className="fas fa-pen"></i>
                                  </NavLink>
                                )
                              }
                            })()}
                            {(() => {
                              let deletable = false;

                              if(isAdmin() || isOwner(item.uid[0].target_id)) {
                                deletable = true;
                              }

                              if(deletable) {
                                return (
                                  <button className={`btn btn-outline-danger btn-sm ${deletingItemID === item.nid[0].value ? `disabled` : ``}`} onClick={() => { this.confirmDeleteItem(item) }}>
                                    <i className="fas fa-trash"></i>
                                  </button>
                                )
                              }
                            })()}
                          </td>
                        </tr>
                        {showItemInventory &&
                          <tr>
                            <td colSpan="3">
                              <Inventory id={item.nid[0].value} tableClassNames={`table-borderless`} />
                            </td>
                          </tr>
                        }
                        </React.Fragment>
                      )
                    })}
                  </>
                }
                {items.length === 0 &&
                  <tr>
                    <td colSpan="3">
                      <em>No Items</em>
                    </td>
                  </tr>
                }
                </>
              }
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                  {items.length} Item{items.length !== 1 ? 's' : ''}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isGettingItems: state.items.isGettingItems,
    isGettingItems: state.items.isGettingItems,
    items: state.items.items,
    deletingItemID: state.items.deletingItemID,
    isDeletingItem: state.items.isDeletingItem
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getItems,
    deleteItem,
    getUserByID,
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemsList);
