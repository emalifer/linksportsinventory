// @flow
import React, { Component } from 'react';

import Navigation from '../Navigation/Navigation';

import { withRouter } from "react-router";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router';
import routes from '../../constants/routes.json';

import ItemsList from './ItemsList';
import Item from './Item/Item';
import ItemEditor from './Item/ItemEditor';

class Items extends Component {
  render() {

    const { location, match } = this.props;

    return (
      <Switch location={location}>
        <Route path={routes.ITEMS} component={ItemsList} exact />
        <Route path={`${routes.ITEMS}/create`} component={ItemEditor} exact />
        <Route path={`${routes.ITEMS}/:id`} component={Item} exact />
        <Route path={`${routes.ITEMS}/:id/edit`} component={ItemEditor} exact />
      </Switch>
    );
  }
}

export default withRouter(Items);
