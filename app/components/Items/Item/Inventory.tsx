import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import routes from '../../../constants/routes.json';

import {
  getItemByID
} from '../../../actions/items';

import {
  getPurchaseOrdersWithItemID
} from '../../../actions/purchaseorders';

import {
  getDeliveryReceiptsWithItemID
} from '../../../actions/deliveryreceipts';
import moment from 'moment';

class Inventory extends Component {

  state = {
    initialQuantity: null,
    inventoryItemRows: []
  }

  componentDidMount = () => {

    if(this.props.id) {

      const item = this.props.getItemByID(this.props.id);
      if(item) {

        let initialQuantity = {details: 'Initial Quantity', in: 0};

        if(item.field_item_initial_quantity.length > 0 ) {
          initialQuantity = {
            details: `Initial Quantity`,
            in: item.field_item_initial_quantity[0].value
          };
        }

        this.setState({initialQuantity})
      }

      let inventoryItemRows = [];

      const purchaseOrdersWithItemID = this.props.getPurchaseOrdersWithItemID(this.props.id);
      if(purchaseOrdersWithItemID.length > 0) {
        inventoryItemRows = inventoryItemRows.concat(purchaseOrdersWithItemID);
      }

      const deliveryReceiptsWithItemID = this.props.getDeliveryReceiptsWithItemID(this.props.id);
      if(deliveryReceiptsWithItemID.length > 0) {
        inventoryItemRows = inventoryItemRows.concat(deliveryReceiptsWithItemID);
      }

      if(inventoryItemRows.length > 0) {
        inventoryItemRows.sort((inventoryItemA, inventoryItemB) =>{
          return new Date(inventoryItemA.field_purchase_order_date[0].value) - new Date(inventoryItemB.field_purchase_order_date[0].value);
        })
      }

      this.setState({inventoryItemRows});
    }
  }

  render() {

    const { initialQuantity, inventoryItemRows } = this.state;
    const { id, tableClassNames } = this.props;
    let balance = 0;
    let initialQuantityRender = null;
    let inventoryItemRowsRender = null;
    let totalBalanceRender = null;

    if(initialQuantity) {
      balance += initialQuantity.in;
      initialQuantityRender =
        <tr>
          <th width="20%">-</th>
          <th width="40%">{initialQuantity.details}</th>
          <th width="10%" className="text-right">{initialQuantity.in}</th>
          <th width="10%" className="text-right"></th>
          <th width="10%" className="text-right">{balance}</th>
        </tr>;
    }

    if(inventoryItemRows.length > 0) {
      inventoryItemRowsRender = inventoryItemRows.map((inventoryItem) => {
        let quantity = 0;
        let type = inventoryItem.type[0].target_id;

        inventoryItem.field_purchase_order_items.forEach((purchaseOrderItem) => {
          if(purchaseOrderItem.target_id === id) {
            quantity = purchaseOrderItem.quantity;
          }
        })

        switch(type) {
          case 'purchase_order':
            balance += quantity;
            break;
          case 'delivery_receipt':
            balance -= quantity;
            break;
          default:
            break;
        }

        return (
          <tr key={inventoryItem.title[0].value}>
            <td>{moment(inventoryItem.field_purchase_order_date[0].value).format('MMM DD - YYYY')}</td>
            <td>
              {type === 'purchase_order' &&
                <NavLink className="text-success" to={`${routes.PURCHASEORDERS}/${inventoryItem.nid[0].value}`}>PO# {inventoryItem.title[0].value}</NavLink>
              }
              {type === 'delivery_receipt' &&
                <NavLink className="text-success" to={`${routes.DELIVERYRECEIPTS}/${inventoryItem.nid[0].value}`}>DR# {inventoryItem.title[0].value}</NavLink>
              }
            </td>
            <td className="text-right">{type === 'purchase_order' ? quantity : ''}</td>
            <td className="text-right">{type === 'delivery_receipt' ? quantity : ''}</td>
            <td className="text-right">{balance}</td>
          </tr>
        )
      })
    }

    totalBalanceRender = <tfoot>
      <tr>
        <th colSpan="4" className="text-right"><h5>Remaining</h5></th>
        <th className="text-right"><h5>{balance}</h5></th>
      </tr>
    </tfoot>

    return (
      <div className="container-fluid">
        <div className="row">
          <table className={`table ${tableClassNames ? tableClassNames: ``}`}>
            <thead>
              <tr>
                <th>Date</th>
                <th>Details</th>
                <th className="text-right">In</th>
                <th className="text-right">Out</th>
                <th className="text-right">Balance</th>
              </tr>
            </thead>
            <tbody>
              {initialQuantityRender}
              {inventoryItemRowsRender}
            </tbody>
            {totalBalanceRender}
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getItemByID,
    getPurchaseOrdersWithItemID,
    getDeliveryReceiptsWithItemID
  }, dispatch);
}

export default connect(
  mapStateToProps, mapDispatchToProps
)(Inventory);
