import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import moment from 'moment';
import numeral from 'numeraljs';
import Barcode from 'react-barcode';
import { ipcRenderer } from 'electron';

import config from '../../../../configs/config';
import logo from '../../../images/logo_white.png'
import routes from '../../../constants/routes.json';
import { getItems, getItem, deleteItem, getItemBadgeStatus } from '../../../actions/items';
import { getSupplierByID } from '../../../actions/suppliers';
import { isAdmin, isOwner } from '../../../actions/auth';
import { getItemByID } from '../../../actions/items';
import { getUserByID } from '../../../actions/users';
import Inventory from './Inventory';
import sample from './sample.png';

const { dialog } = require('electron').remote;

class Item extends Component {

  state = {
    item: {},
    suppliers: []
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getItem(this.props.match.params.id, (item) => {
        console.log(item);
        this.setState(
          produce(this.state, draft => {
            draft.item = item;
          })
        );
      })
    }
    this.mapSuppliers();
  }

  componentDidUpdate = (prevProps) => {
    if(prevProps.suppliers !== this.props.suppliers) {
      this.mapSuppliers()
    }
  }

  mapSuppliers = () => {
    const suppliers = this.props.suppliers.map((supplier) => {
      return {
        id: supplier.nid[0].value,
        label: supplier.title[0].value
      }
    });
    this.setState({suppliers});
  }

  generateDescription = (html) => {
    return {__html: html};
  }

  sendToPrinter = () => {
    ipcRenderer.send("printPDF", this.state.item.title[0].value, document.getElementById('item').innerHTML);
  }

  confirmDeleteItem = () => {
    const { item } = this.state;

    const resp = dialog.showMessageBoxSync(
      {
        message: `Do you want to delete Item ${item.title[0].value}?`,
        buttons: ['Yes','No']
      }
    );

    if(resp === 0) {
      this.props.deleteItem(item.nid[0].value, () => {
        this.props.getItems(() => {
          this.props.history.push(`${routes.ITEMS}`);
        })
      })
    }

    // dialog.showMessageBox(
    //   {
    //     message: `Do you want to delete Item ${item.title[0].value}?`,
    //     buttons: ['Yes','No']
    //   },
    //   (response) => {
    //     if(response === 0) {
    //       this.props.deleteItem(item.nid[0].value, () => {
    //         this.props.getItems(() => {
    //           this.props.history.push(`${routes.ITEMS}`);
    //         })
    //       })
    //     }
    //   }
    // );
  }

  render() {

    const { item, suppliers }  = this.state;
    const { isDeletingItem }  = this.props;
    let total = 0;
    let userName = '';
    if(item.uid) {
      let user = this.props.getUserByID(item.uid[0].target_id);
      if(user) {
        userName = user.name[0].value;
      }
    }

    if(item.nid){
      return (
        <>
          <nav className="mt-4">
            <ol className="breadcrumb">
              <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.ITEMS}>Items</NavLink></li>
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{item.title ? item.title[0].value : ``}</li>
            </ol>
          </nav>

          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="col-8 pl-0">
              <h1 className="h2 text-success font-weight-bold text-uppercase">
                <i className="fab fa-buffer mr-2"></i>
                {item.title ? item.title[0].value : ``}
              </h1>
            </div>
            <div className="text-right col-4 pr-0">
              {(() => {
                let editable = false;

                if(isAdmin() || isOwner(item.uid[0].target_id)) {
                  editable = true;
                }

                if(editable) {
                  return (
                    <NavLink className={`btn btn-outline-secondary btn-lg mr-2 ${isDeletingItem ? `disabled` : ``}`} to={`${routes.ITEMS}/${item.nid[0].value}/edit`}>
                      <i className="fas fa-pen mr-3"></i>Edit
                    </NavLink>
                  )
                }
              })()}
              {(() => {
                let deletable = false;

                if(isAdmin() || isOwner(item.uid[0].target_id)) {
                  deletable = true;
                }

                if(deletable) {
                  return (
                    <button className="btn btn-outline-danger btn-lg" disabled={isDeletingItem} onClick={() => { this.confirmDeleteItem(); }}><i className="fas fa-trash mr-3"></i>
                      {isDeletingItem ? 'Deleting' : 'Delete'}
                    </button>
                  )
                }
              })()}
            </div>
          </div>

          <div className="card p-4 mb-4">
            <div id="item" className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(item.field_unit_price.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Unit Price</h5>
                            <p>{numeral(item.field_unit_price[0].value).format('0,0.00')} </p>
                          </>
                        )
                      }
                    })()}
                    {(() => {
                      if(item.field_selling_price.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Selling Price</h5>
                            <p>{numeral(item.field_selling_price[0].value).format('0,0.00')} </p>
                          </>
                        )
                      }
                    })()}
                    {(() => {
                      if(item.barcode.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Barcode</h5>
                            <Barcode value={`${item.barcode[0].value}`} />
                            <div className="mb-4" />
                          </>
                        )
                      }
                    })()}
                  </div>
                  <div className="col text-right">
                    {(() => {
                      if(item.field_item_image.length > 0) {
                        return <><img className="img-fluid" src={item.field_item_image[0].url} /><br/></>
                      } else {
                        return (<>
                          <img className="img-fluid" src={sample} /><br/>
                        </>)
                      }
                    })()}
                    <span className="d-inline-block font-italic text-muted mt-3" style={{fontSize: '.75rem'}}>
                      Item image can be uploaded through the CMS<br/>
                      {config.INVENTORY_URL}
                    </span>
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    <h5 className="card-title"><label htmlFor="itemName">Item Supplier</label></h5>
                    {item.field_item_supplier &&
                      <>
                      {item.field_item_supplier.length > 0 &&
                        <>
                          {item.field_item_supplier.map((supplierItem) => {
                            const supplier = this.props.getSupplierByID(supplierItem.target_id);
                            return (
                              <React.Fragment key={supplierItem.target_id}>
                                {supplier &&
                                  <>
                                    <p className="mb-0">Vendor ID: {supplier.field_supplier_code[0].value}</p>
                                    <h5>{supplier.title[0].value}</h5>
                                  </>
                                }
                              </React.Fragment>
                            )
                          })}
                        </>
                      }
                      </>
                    }
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    {(() => {
                        if(item.field_code.length > 0) {
                          return (
                            <>
                              <h5 className="card-title">Item Code</h5>
                              <p>
                                {item.field_item_supplier &&
                                  <>
                                  {item.field_item_supplier.length > 0 &&
                                    <>
                                      {item.field_item_supplier.map((supplierItem) => {
                                        const supplier = this.props.getSupplierByID(supplierItem.target_id);
                                        return (
                                          <React.Fragment key={supplierItem.target_id}>
                                            {supplier &&
                                              <>
                                                {supplier.field_supplier_code[0].value}-
                                              </>
                                            }
                                          </React.Fragment>
                                        )
                                      })}
                                    </>
                                  }
                                  </>
                                }
                                {item.field_code[0].value}
                              </p>
                            </>
                          )
                        }
                      })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(item.body.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Item Description</h5>
                            <div dangerouslySetInnerHTML={this.generateDescription(item.body[0].value)} />
                          </>
                        )
                      }
                    })()}
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    {(() => {
                      if(item.field_item_initial_quantity.length > 0) {
                        return (
                          <>
                            <h5 className="card-title">Item Initial Quantity</h5>
                            <p>{item.field_item_initial_quantity[0].value}</p>
                          </>
                        )
                      }
                    })()}
                  </div>
                </div>

              </div>

            </div>
          </div>

          <div className="card p-4 mb-4">
            <div className="card-body">
              <h5 className="card-title">Item Inventory</h5>
              <Inventory id={item.nid[0].value} />
            </div>
          </div>

        </>
      );
    }

    else {
      return (
        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.ITEMS}>Items</NavLink></li>
            <li className="breadcrumb-item font-weight-bold text-uppercase active">{item.title ? item.title[0].value : ``}</li>
          </ol>
        </nav>
      )
    }

  }
}

const mapStateToProps = (state) => {
  return {
    isDeletingItem: state.items.isDeletingItem,
    suppliers: state.suppliers.suppliers
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getItems,
    getItem,
    getItemByID,
    getUserByID,
    deleteItem,
    getSupplierByID
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Item));
