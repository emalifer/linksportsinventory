import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import produce from 'immer';
import { ToastContainer, toast } from 'react-toastify';
import Barcode from 'react-barcode';
import CKEditor from '@ckeditor/ckeditor5-react';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Typeahead } from 'react-bootstrap-typeahead';

import config from '../../../../configs/config';
import routes from '../../../constants/routes.json';
import {
  getItems,
  getItem,
  createOrUpdateItem
} from '../../../actions/items';
import { getSupplierByID } from '../../../actions/suppliers';
import sample from './sample.png';

const MODE = {
  CREATE: 'create',
  UPDATE: 'update'
}

class ItemEditor extends Component {

  constructor(props) {
    super(props);
    this.titleInput = React.createRef();
    this.supplierInput = React.createRef();
  }

  state = {
    mode: '',
    item: {
      type: [{target_id: "item"}],
      title: [{value: '', error: false}],
      barcode: [{value: '', error: false}],
      body: [{value: ''}],
      field_code: [{value: '', error: false}],
      field_i: [{value: false}],
      field_item: [],
      field_selling_price: [{value: '0.00', error: false}],
      field_unit_price: [{value: '0.00', error: false}],
      field_item_initial_quantity: [{value: 0, error: false}],
      field_item_supplier: [{target_id: 0, error: false}]
    },
    suppliers: []
  }

  componentDidMount = () => {
    if(this.props.match.params.id){
      this.props.getItem(this.props.match.params.id, (item) => {
        console.log(item);
        this.setState(
          produce(this.state, draft => {
            draft.item = item;

            if(item.body.length === 0) {
              draft.item.body = [{value: ''}];
            }

            draft.mode = MODE.UPDATE;
          })
        );
      })
    } else {
      this.setState(
        produce(this.state, draft => {
          draft.mode = MODE.CREATE;
        })
      );
    }

    this.mapSuppliers();
  }

  componentDidUpdate = (prevProps) => {
    if(prevProps.suppliers !== this.props.suppliers) {
      this.mapSuppliers()
    }
  }

  mapSuppliers = () => {
    const suppliers = this.props.suppliers.map((supplier) => {
      return {
        id: supplier.nid[0].value,
        label: supplier.title[0].value
      }
    });
    this.setState({suppliers});
  }

  changeTitle = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.title[0].value = e.target.value;
      draft.item.title[0].error = false;
    }));
  }

  changeBarcode = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.barcode[0].value = e.target.value;
      draft.item.barcode[0].error = false;
    }));
  }

  changeCode = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.field_code[0].value = e.target.value;
      draft.item.field_code[0].error = false;
    }));
  }

  changeIsAssemble = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.field_i[0].value = e.target.value;
    }));
  }

  changeSellingPrice = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.field_selling_price[0].value = e.target.value;
      draft.item.field_selling_price[0].error = false;
    }));
  }

  changeUnitPrice = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.field_unit_price[0].value = e.target.value;
      draft.item.field_unit_price[0].error = false;
    }));
  }

  changeBody = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.body[0].value = e.target.value;
      draft.item.body[0].error = false;
    }));
  }

  changeInitialQuantity = (e) => {
    this.setState(produce(this.state, draft => {
      draft.item.field_item_initial_quantity[0].value = e.target.value;
      draft.item.field_item_initial_quantity[0].error = false;
    }));
  }

  changeSupplier = (suppliers) => {
    if(suppliers.length > 0){
      this.setState(produce(this.state, draft => {
        draft.item.field_item_supplier[0] = {};
        draft.item.field_item_supplier[0].target_id = suppliers[0].id;
        draft.item.field_item_supplier[0].error = false;
      }));
    } else {
      this.setState(produce(this.state, draft => {
        draft.item.field_item_supplier[0] = {};
        draft.item.field_item_supplier[0].target_id = 0
      }));
    }
  }

  validateItem = () => {
    let flag = true;
    const { item } = this.state;

    this.setState(produce(this.state, draft => {
      if(item.title[0].value.length === 0) {
        flag = false;
        draft.item.title[0].error = true;
        toast.error("Please enter an Item Name");
      }
    }));

    this.setState(produce(this.state, draft => {
      if(item.barcode[0].value.length === 0) {
        flag = false;
        draft.item.barcode[0].error = true;
        toast.error("Please enter a Barcode");
      }
    }));

    this.setState(produce(this.state, draft => {
      if(item.field_code[0].value.length === 0) {
        flag = false;
        draft.item.field_code[0].error = true;
        toast.error("Please enter an Item Code");
      }
    }));

    this.setState(produce(this.state, draft => {
      if(item.field_selling_price[0].value.length === 0) {
        flag = false;
        draft.item.field_selling_price[0].error = true;
        toast.error("Please enter a Selling Price");
      }
    }));

    this.setState(produce(this.state, draft => {
      if(item.field_unit_price[0].value.length === 0) {
        flag = false;
        draft.item.field_unit_price[0].error = true;
        toast.error("Please enter a Unit Price");
      }
    }));

    this.setState(produce(this.state, draft => {
      if(item.field_item_initial_quantity[0].value.length === 0) {
        flag = false;
        draft.item.field_item_initial_quantity[0].error = true;
        toast.error("Please enter an Initial Quantity");
      }
    }));

    this.setState(produce(this.state, draft => {
      if(item.field_item_supplier[0].target_id === 0) {
        flag = false;
        draft.item.field_item_supplier[0].error = true;
        toast.error("Please select a supplier/vendor");
      }
    }));

    return flag;
  }

  saveItem = (e) => {

    e.preventDefault();

    if(this.validateItem()) {
      let method = 'POST';

      switch(this.state.mode) {
        case MODE.CREATE:
          method = 'POST';
          break;
        case MODE.UPDATE:
          method = 'PATCH';
          break;
        default:
          break;
      }

      this.props.createOrUpdateItem(method, this.state.item, (resp) => {
        this.props.getItems(() => {
          this.props.history.push(`${routes.ITEMS}/${resp.nid[0].value}`)
        });
      })
    }

  }

  render() {

    const { isSavingItem } = this.props;
    const { mode, item, suppliers }  = this.state;

    return (
      <>
        <ToastContainer position="top-right" autoClose={5000} hideProgressBar newestOnTop={false} closeOnClick rtl={false} pauseOnVisibilityChange draggable pauseOnHover />

        <nav className="mt-4">
          <ol className="breadcrumb">
            <li className="breadcrumb-item font-weight-bold text-uppercase"><NavLink className="text-success" to={routes.ITEMS}>Items</NavLink></li>
            {mode === MODE.CREATE &&
              <li className="breadcrumb-item font-weight-bold text-uppercase active">{item.title[0].value ? item.title[0].value : `Create`}</li>
            }
            {mode === MODE.UPDATE &&
              <>
                <li className="breadcrumb-item font-weight-bold text-uppercase">
                  <NavLink className="text-success" to={`${routes.ITEMS}/${item.nid[0].value ? item.nid[0].value : ``}`}>{item.title[0].value ? item.title[0].value : ``}</NavLink>
                </li>
                <li className="breadcrumb-item font-weight-bold text-uppercase active">Edit</li>
              </>
            }
          </ol>
        </nav>

        <form onSubmit={(e) => { this.saveItem(e); }}>
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="col-8 pl-0">
              <h1 className="h2 text-success font-weight-bold text-uppercase">
                <i className="fas fa-truck-loading mr-2"></i>
                {item.title[0].value ? item.title[0].value : `New Item`}
              </h1>
            </div>
            {/* <div className="text-right col-4 pr-0">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingItem}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingItem ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingItem ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div> */}
          </div>

          <div className="card p-4 mb-4">
            <div className="card-body">
              <div className="container-fluid mb-4">

                <div className="row mb-4">
                  <div className="col">

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="itemName">Item Name</label></h5>
                      <input type="text" id="itemName" className={`form-control ${item.title[0].error ? `is-invalid` : ``}`} disabled={isSavingItem} placeholder="Item Name" value={item.title[0].value} onChange={(e) => { this.changeTitle(e) }} />
                    </div>

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="itemUnitPrice">Unit Price</label></h5>
                      <input type="text" id="itemUnitPrice" className={`form-control ${item.field_unit_price[0].error ? `is-invalid` : ``}`} disabled={isSavingItem} placeholder="Unit Price" value={item.field_unit_price[0].value} onChange={(e) => { this.changeUnitPrice(e) }} />
                    </div>

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="itemSellingPrice">Selling Price</label></h5>
                      <input type="text" id="itemSellingPrice" className={`form-control ${item.field_selling_price[0].error ? `is-invalid` : ``}`} disabled={isSavingItem} placeholder="Selling Price" value={item.field_selling_price[0].value} onChange={(e) => { this.changeSellingPrice(e) }} />
                    </div>

                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="itemBarcode">Barcode</label></h5>
                      <input type="text" id="itemBarcode" className={`form-control mb-2 ${item.barcode[0].error ? `is-invalid` : ``}`} disabled={isSavingItem} placeholder="Barcode" value={item.barcode[0].value} onChange={(e) => { this.changeBarcode(e) }} />
                      <Barcode value={`${item.barcode[0].value}`} />
                      <div className="mb-4" />
                    </div>

                  </div>
                  <div className="col text-right">
                    <img className="img-fluid" src={sample} /><br/>
                    <span className="d-inline-block font-italic text-muted mt-3" style={{fontSize: '.75rem'}}>
                      Item image can be uploaded through the CMS<br/>
                      {config.INVENTORY_URL}
                    </span>
                  </div>

                </div>

                <div className="row mb-3">
                  <div className="col">
                    <h5 className="card-title"><label htmlFor="itemName">Item Supplier</label></h5>
                    <div className="mb-3">
                      <Typeahead id="supplier-typeahead" isInvalid={item.field_item_supplier[0].error} placeholder="Search suppliers" onChange={(selected) => { this.changeSupplier(selected); }} options={suppliers} clearButton={true} disabled={isSavingItem} />
                    </div>
                    {/* Supplier */}
                    {item.field_item_supplier &&
                      <>
                      {item.field_item_supplier.length > 0 &&
                        <>
                          {item.field_item_supplier.map((supplierItem) => {
                            const supplier = this.props.getSupplierByID(supplierItem.target_id);
                            return (
                              <React.Fragment key={supplierItem.target_id}>
                                {supplier &&
                                  <>
                                    <p className="mb-0">Vendor ID: {supplier.field_supplier_code[0].value}</p>
                                    <h5>{supplier.title[0].value}</h5>
                                  </>
                                }
                              </React.Fragment>
                            )
                          })}
                        </>
                      }
                      </>
                    }
                  </div>

                </div>

                <div className="row mb-4">
                  <div className="col">
                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="itemCode">Item Code</label></h5>
                      <div className="input-group">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="inputGroupPrepend">
                          {item.field_item_supplier &&
                            <>
                            {item.field_item_supplier.length > 0 &&
                              <>
                                {item.field_item_supplier.map((supplierItem) => {
                                  const supplier = this.props.getSupplierByID(supplierItem.target_id);
                                  return (
                                    <React.Fragment key={supplierItem.target_id}>
                                      {supplier &&
                                        <>
                                          {supplier.field_supplier_code[0].value}-
                                        </>
                                      }
                                    </React.Fragment>
                                  )
                                })}
                              </>
                            }
                            </>
                          }
                          </span>
                        </div>
                        <input type="text" id="itemCode" className={`form-control ${item.field_code[0].error ? `is-invalid` : ``}`} disabled={isSavingItem} placeholder="Item Code" value={item.field_code[0].value} onChange={(e) => { this.changeCode(e) }} />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row mb-4">
                  <div className="col">
                    <h5 className="card-title">Item Description</h5>
                    <textarea className="form-control" rows="3" value={item.body[0].value} onChange={this.changeBody} />
                    {/* <CKEditor editor={ ClassicEditor } data={item.body[0].value} onChange={ ( event, editor ) => { const data = editor.getData(); this.changeBody(data); }} /> */}
                    {/* <CKEditor data={item.body[0].value} onChange={ ( event, editor ) => { const data = editor.getData(); this.changeBody(data); }} /> */}
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <div className="form-group">
                      <h5 className="card-title"><label htmlFor="itemInitialQuantity">Item Initial Quantity</label></h5>
                      <input type="number" id="itemInitialQuantity" className={`form-control ${item.field_item_initial_quantity[0].error ? `is-invalid` : ``}`} disabled={isSavingItem} placeholder="Item Initial Quantity" value={item.field_item_initial_quantity[0].value} onChange={(e) => { this.changeInitialQuantity(e) }} />
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className="mb-3">
            <div className="float-right">
              <button type="submit" className="btn btn-outline-success btn-lg" disabled={isSavingItem}><i className="fas fa-pen mr-3"></i>
                {mode === MODE.CREATE ?
                  <>
                    {isSavingItem ? 'Saving' : 'Save'}
                  </>
                : ''}
                {mode === MODE.UPDATE ?
                  <>
                    {isSavingItem ? 'Updating' : 'Update'}
                  </>
                : ''}
              </button>
            </div>
            <div className="clearfix"></div>
          </div>

        </form>

      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSavingItem: state.items.isSavingItem,
    suppliers: state.suppliers.suppliers
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getItems,
    getItem,
    createOrUpdateItem,
    getSupplierByID
  }, dispatch);
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ItemEditor));
